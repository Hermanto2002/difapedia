<?php

function uploadImage($file, $path, $name)
{
    $extension = $file->getClientOriginalExtension();
    $fileName = $name . '.' . $extension;
    $file->move($path, $fileName);
    return $fileName;
}

function deleteImage($path)
{
    if(File::exists($path)){
        File::delete($path);
    }
}

function idGenerator()
{
    $id=rand(1000,9999);
    return $id;
}

function hitung_umur($tanggal_lahir){
	$birthDate = new DateTime($tanggal_lahir);
	$today = new DateTime("today");
	if ($birthDate > $today) {
	    exit("0 tahun");
	}
	$y = $today->diff($birthDate)->y;
	return $y." tahun ";
}

function countCart()
{
    $jml=0;
    if(Session::has('cart')){
        $cart=Session::get('cart');
        foreach($cart as $item){
            $jml++;
        }
    }
    return $jml;
}


?>
