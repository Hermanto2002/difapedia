<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function authenticated(Request $request, $user)
    {
        if ($user->hasRole('admin')) {
            if ($user->status == '0') {
                return redirect()->route('admin.profile.index');
            }
            return redirect()->route('admin.index');
        }else if($user->hasRole('difabel')){
            if ($user->status == '0') {
                return redirect()->route('difabel.profile.index');
            }
            return redirect()->route('difabel.index');
        }else if($user->hasRole('hrd')){
            if ($user->status == '0') {
                return redirect()->route('hrd.profile.index');
            }
            return redirect()->route('loker.index');
        }else if($user->hasRole('donatur')){
            if ($user->status == '0') {
                return redirect()->route('donatur.profile.index');
            }
            return redirect()->route('donatur.index');
        }else{
            return redirect()->route('index');
        }
    }
}
