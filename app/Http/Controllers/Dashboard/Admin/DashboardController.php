<?php

namespace App\Http\Controllers\Dashboard\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\User;
use App\models\Donasi;
use App\models\Transaksi;
use App\models\DetailTransaksi;
use App\models\Loker;

class DashboardController extends Controller
{
    public function index()
    {
        $donasi=donasi::all();
        $transaksi=transaksi::all();

        $jmlDifabel=User::role('difabel')->count();
        $jmlDonatur=User::role('donatur')->count();
        $jmlHrd=User::role('hrd')->count();

        $totalDonasi=Donasi::where('status','=','1')->sum('jumlah');
        $transaksi=Transaksi::all();
        $transaksiDetail=DetailTransaksi::all();
        $totalTransaksi=0;
        foreach ($transaksiDetail as $item) {
            $totalTransaksi += $item->jumlah*$item->getProduk->harga;
        }

        $totalPemasukan=$totalDonasi+$totalTransaksi;

        $data=[
            'donasi'=>$donasi,
            'transaksi'=>$transaksi,
            'jmlDifabel'=>$jmlDifabel,
            'jmlDonatur'=>$jmlDonatur,
            'jmlHrd'=>$jmlHrd,
            'totalDonasi'=>$totalDonasi,
            'totalTransaksi'=>$totalTransaksi,
            'totalPemasukan'=>$totalPemasukan,
            'loker' => Loker::all(),
        ];

        return view('dashboard.admin.index', $data);
    }
}
