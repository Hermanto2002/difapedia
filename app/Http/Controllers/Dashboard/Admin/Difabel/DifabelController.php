<?php

namespace App\Http\Controllers\Dashboard\Admin\Difabel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use DB;

class DifabelController extends Controller
{
    public function index()
    {
        $difabel=User::role('difabel')->get();
        return view('dashboard.admin.difabel.list', compact('difabel'));
    }

    public function detail($id)
    {
        $difabel=User::find($id);
        return view('dashboard.admin.difabel.detail', compact('difabel'));
    }

    public function create()
    {
        return view('dashboard.admin.difabel.add');
    }

    public function store(Request $request)
    {
        DB::beginTransaction();

        try {
            $data=[
                'name'=>$request->name,
                'email'=>$request->email,
                'password'=>bcrypt($request->password),
                'role_id'=>2,
                'kategori_disabilitas'=>$request->kategori_disabilitas,
                'no_telp'=>$request->no_telp,
                'alamat'=>$request->alamat,
                'kota_asal'=>$request->kota_asal,
                'tanggal_lahir'=>$request->tanggal_lahir,
                'jenis_kelamin'=>$request->jenis_kelamin,
                'nama_bank'=>$request->nama_bank,
                'nomor_rekening'=>$request->nomor_rekening,
                'tentang_difabel'=>$request->tentang_difabel,
                'foto_profil'=>$request->name.'.'.$request->foto_profil->getClientOriginalExtension(),
            ];

            if ($request->hasFile('foto_profil')) {
                //upload gambar
                $file = $request->file('foto_profil');
                $patch = 'difabel';
                $name = $request->name;
                uploadImage($file, $patch, $name);
            }

            $user=User::create($data);
            $user->assignRole('difabel');


            DB::commit();
            return redirect()->route('admin.difabel.index')->with('success', 'Data berhasil ditambahkan');
            // all good
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('failed', 'Data gagal ditambahkan');
        }

    }

    public function update(Request $request)
    {
        DB::beginTransaction();

        try {
            $data=[
                'name'=>$request->name,
                'email'=>$request->email,
                'password'=>bcrypt($request->password),
                'role_id'=>2,
                'kategori_disabilitas'=>$request->kategori_disabilitas,
                'no_telp'=>$request->no_telp,
                'alamat'=>$request->alamat,
                'kota_asal'=>$request->kota_asal,
                'tanggal_lahir'=>$request->tanggal_lahir,
                'jenis_kelamin'=>$request->jenis_kelamin,
                'nama_bank'=>$request->nama_bank,
                'nomor_rekening'=>$request->nomor_rekening,
                'tentang_difabel'=>$request->tentang_difabel,
                'foto_profil'=>$request->foto_profil,
            ];

            $id=$request->id;

            $user=User::find($id)->update($data);


            DB::commit();
            return redirect()->route('admin.difabel.index')->with('success', 'Data berhasil ditambahkan');
            // all good
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with('failed', 'Data gagal ditambahkan');
        }
    }
}
