<?php

namespace App\Http\Controllers\Dashboard\Admin\Difabel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Produk;

class DifabelProdukController extends Controller
{
    public function index()
    {
        $produk=Produk::all();
        return view('dashboard.admin.difabel.produk', compact('produk'));
    }
}
