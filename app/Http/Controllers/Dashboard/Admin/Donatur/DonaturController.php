<?php

namespace App\Http\Controllers\Dashboard\Admin\Donatur;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class DonaturController extends Controller
{
    public function index()
    {
        $donatur=User::role('donatur')->get();
        return view('dashboard.admin.donatur.list',compact('donatur'));
    }
}
