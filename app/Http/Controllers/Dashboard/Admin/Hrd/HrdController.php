<?php

namespace App\Http\Controllers\Dashboard\Admin\Hrd;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class HrdController extends Controller
{
    public function index()
    {
        $hrd=User::role('hrd')->get();
        return view('dashboard.admin.hrd.list', compact('hrd'));
    }
}
