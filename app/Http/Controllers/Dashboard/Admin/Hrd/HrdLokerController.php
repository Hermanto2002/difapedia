<?php

namespace App\Http\Controllers\Dashboard\Admin\Hrd;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Loker;

class HrdLokerController extends Controller
{
    public function index()
    {
        $loker=Loker::all();
        return view('dashboard.admin.hrd.listLoker', compact('loker'));
    }
}
