<?php

namespace App\Http\Controllers\Dashboard\Admin\Pembayaran;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Donasi;

class DonasiController extends Controller
{
    public function index()
    {
        $donasi=Donasi::all();
        return view('dashboard.admin.pembayaran.donasi', compact('donasi'));
    }

    public function update(Request $request)
    {
        $donasi=Donasi::find($request->id);
        $data=[
            'status'=>$request->status,
        ];
        $donasi->update($data);
        return redirect()->back();
    }
};
