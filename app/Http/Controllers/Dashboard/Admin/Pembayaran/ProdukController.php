<?php

namespace App\Http\Controllers\Dashboard\Admin\Pembayaran;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\Transaksi;

class ProdukController extends Controller
{
    public function index()
    {
        $transaksi=Transaksi::all();
        return view('dashboard.admin.pembayaran.produk',compact('transaksi'));
    }

    public function update(Request $request)
    {
        $transaksi=Transaksi::find($request->id);
        $data=[
            'status'=>$request->status,
        ];
        $transaksi->update($data);
        return redirect()->back();
    }
}
