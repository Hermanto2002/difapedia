<?php

namespace App\Http\Controllers\Dashboard\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class ProfileController extends Controller
{
    function index()
    {
        $user = User::find(auth()->user()->id);
        return view('dashboard.admin.profile.index', compact('user'));
    }
}
