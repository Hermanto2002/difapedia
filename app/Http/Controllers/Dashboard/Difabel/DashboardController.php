<?php

namespace App\Http\Controllers\Dashboard\Difabel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Loker;
use App\Models\Donasi;
use App\Models\DetailTransaksi;

class DashboardController extends Controller
{
    public function index()
    {
        $id_user=auth()->user()->id;
        $donasi=Donasi::where('id_difabel',$id_user)->where('status', '1')->get();
        $penjualan = DetailTransaksi::whereHas('getProduk', function($q) use ($id_user) {
            $q->where('id_difabel', $id_user);
        })->get();

        $loker =Loker::all();

        $jmlDonasi=0;
        $jmlPenjualan=0;
        foreach ($donasi as $key => $value) {
            $jmlDonasi += $value->jumlah;
        }
        foreach ($penjualan as $key => $value) {
            $jmlPenjualan += $value->jumlah*$value->getProduk->harga;
        }

        $data = [
            'donasi' => $donasi,
            'penjualan' => $penjualan,
            'jmlDonasi' => $jmlDonasi,
            'jmlPenjualan' => $jmlPenjualan,
            'loker' => $loker,
        ];

        return view('dashboard.difabel.index', $data);
    }
}
