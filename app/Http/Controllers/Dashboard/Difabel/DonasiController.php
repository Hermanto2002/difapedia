<?php

namespace App\Http\Controllers\Dashboard\Difabel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Donasi;

class DonasiController extends Controller
{
    public function index()
    {
        $donasi=Donasi::where('id_difabel', Auth()->user()->id)->where('status', "1")->get();
        return view('dashboard.difabel.donasi.list',compact('donasi'));
    }
}
