<?php

namespace App\Http\Controllers\Dashboard\Difabel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Loker;

class LokerController extends Controller
{
    public function index()
    {
        $loker=Loker::all();
        return view('dashboard.difabel.loker.list', compact('loker'));
    }
}
