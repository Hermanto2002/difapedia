<?php

namespace App\Http\Controllers\Dashboard\Difabel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\DetailTransaksi;

class PenjualanController extends Controller
{
    public function index()
    {
        $detailtransaksi=DetailTransaksi::whereHas('getProduk', function($q) {
            $q->where('id_difabel', Auth()->user()->id);
        })->get();

        return view('dashboard.difabel.penjualan.list', compact('detailtransaksi'));
    }
}
