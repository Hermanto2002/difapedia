<?php

namespace App\Http\Controllers\Dashboard\Difabel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Produk;

class ProdukController extends Controller
{
    public function index()
    {
        $produk=Produk::where('id_difabel', Auth()->user()->id)->get();
        return view('dashboard.difabel.produk.list', compact('produk'));
    }

    public function store(Request $request)
    {
        $data=[
            'nama_produk'=>$request->nama_produk,
            'id_difabel'=>Auth()->user()->id,
            'deskripsi_produk'=>$request->deskripsi_produk,
            'harga'=>$request->harga,
            'jumlah'=>$request->jumlah,
            'status'=>$request->status,
            'gambar_produk' => $request->nama_produk.'.'.$request->gambar_produk->getClientOriginalExtension(),
        ];

        if ($request->hasFile('gambar_produk')) {
            //upload gambar
            $file = $request->file('gambar_produk');
            $patch = 'produk';
            $name = $request->nama_produk;
            uploadImage($file, $patch, $name);
        }

        Produk::create($data);

        return redirect()->back();
    }
}
