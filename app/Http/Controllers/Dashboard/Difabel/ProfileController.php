<?php

namespace App\Http\Controllers\Dashboard\Difabel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class ProfileController extends Controller
{
    public function index()
    {
        $user = User::find(auth()->user()->id);
        return view('dashboard.difabel.profile.index', compact('user'));
    }
}
