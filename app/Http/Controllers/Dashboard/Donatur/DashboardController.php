<?php

namespace App\Http\Controllers\Dashboard\Donatur;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Donasi;
use App\Models\DetailTransaksi;

class DashboardController extends Controller
{
    public function index()
    {
        $donasi=Donasi::where('id_donatur', Auth()->user()->id)->get();
        $pembelian=DetailTransaksi::whereHas('getTransaksi', function($q) {
            $q->where('id_user', Auth()->user()->id);
        })->get();

        $jmlDonasi=0;
        $jmlPembelian=0;
        foreach ($donasi as $key => $value) {
            $jmlDonasi += $value->jumlah;
        }
        foreach ($pembelian as $key => $value) {
            $jmlPembelian += $value->jumlah*$value->getProduk->harga;
        }

        $data = [
            'donasi' => $donasi,
            'pembelian' => $pembelian,
            'jmlDonasi' => $jmlDonasi,
            'jmlPembelian' => $jmlPembelian,
        ];

        return view('dashboard.donatur.index', $data);
    }
}
