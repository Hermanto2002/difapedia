<?php

namespace App\Http\Controllers\Dashboard\Donatur;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Donasi;

class DonasiController extends Controller
{
    public function index()
    {
        $donasi=Donasi::where('id_donatur', auth()->user()->id)->get();
        return view('dashboard.donatur.pembayaran.donasi', compact('donasi'));
    }

    public function update(Request $request)
    {
        $donasi=Donasi::find($request->id);
        $file=$request->file('foto');
        $path="foto_donasi/";
        $name=time()."_".$file->getClientOriginalName();
        uploadImage($file, $path, $name);

        $donasi->update([
            'bukti_pembayaran' => $name,
        ]);

        return redirect()->back();
    }
}
