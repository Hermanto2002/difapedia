<?php

namespace App\Http\Controllers\Dashboard\Donatur;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Transaksi;
use App\Models\DetailTransaksi;

class ProdukController extends Controller
{
    public function index()
    {
        $pembelian=DetailTransaksi::whereHas('getTransaksi', function($q) {
            $q->where('id_user', Auth()->user()->id);
        })->get();

        $jmlPembelian=0;
        foreach ($pembelian as $key => $value) {
            $jmlPembelian += $value->jumlah*$value->getProduk->harga;
        }

        $data = [
            'pembelian' => $pembelian,
            'jmlPembelian' => $jmlPembelian,
        ];

        return view('dashboard.donatur.pembayaran.produk', $data);
    }

    public function update(Request $request)
    {
        $donasi=Transaksi::find($request->id);
        $file=$request->file('foto');
        $path="foto_pembayaran_produk/";
        $name=time()."_".$file->getClientOriginalName();
        uploadImage($file, $path, $name);

        $donasi->update([
            'bukti_pembayaran' => $name,
        ]);

        return redirect()->back();
    }

}
