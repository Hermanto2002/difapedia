<?php

namespace App\Http\Controllers\Dashboard\Donatur;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class ProfileController extends Controller
{
    public function index()
    {
        $user = User::find(auth()->user()->id);
        return view('dashboard.donatur.profile.index', compact('user'));
    }
}
