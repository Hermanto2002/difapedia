<?php

namespace App\Http\Controllers\Dashboard\Hrd;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Loker;
use App\Helpers\Main;

class LokerController extends Controller
{
    public function index()
    {
        $loker=Loker::where('id_hrd', auth()->user()->id)->get();
        return view('dashboard.hrd.loker.index', compact('loker'));
    }

    public function store(Request $request)
    {
        Loker::Create([
            'id_hrd' => auth()->user()->id,
            'nama_loker' => $request->nama_loker,
            'deskripsi' => $request->deskripsi,
            'deadline' => $request->deadline,
            'jumlah_pelamar' => $request->jumlah_pelamar,
            'gambar' => $request->nama_loker.'.'.$request->gambar->getClientOriginalExtension(),
            'status' => $request->status,
        ]);

        if ($request->hasFile('gambar')) {
            //upload gambar
            $file = $request->file('gambar');
            $patch = 'loker';
            $name = $request->nama_loker;
            uploadImage($file, $patch, $name);
        }

        return redirect()->back();
    }

    public function update(Request $request)
    {
        $loker = Loker::find($request->id);
        $loker->nama_loker = $request->nama_loker;
        $loker->deskripsi = $request->deskripsi;
        $loker->deadline = $request->deadline;
        $loker->jumlah_pelamar = $request->jumlah_pelamar;
        $loker->status = $request->status;
        $loker->save();

        if ($request->hasFile('gambar')) {
            $path = 'loker/'.$loker->gambar;
            deleteImage($path);

            //upload gambar
            $file = $request->file('gambar');
            $patch = 'loker';
            $name = $request->nama_loker;
            uploadImage($file, $patch, $name);
        }

        return redirect()->back();
    }
}
