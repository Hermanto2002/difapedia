<?php

namespace App\Http\Controllers\Dashboard\Hrd;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class ProfileController extends Controller
{
    public function index()
    {
        $user = User::find(auth()->user()->id);
        return view('dashboard.hrd.profile.index', compact('user'));
    }
}
