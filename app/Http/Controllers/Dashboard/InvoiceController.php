<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Transaksi;
use App\Models\DetailTransaksi;
use App\Models\Donasi;

class InvoiceController extends Controller
{
    public function transaksi($id)
    {
        $transaksi=Transaksi::find($id);
        $detail=DetailTransaksi::where('id_transaksi', $id)->get();

        return view('dashboard.invoice.transaksi', compact('transaksi','detail'));
    }

    public function donasi($id)
    {
        $donasi=Donasi::find($id);

        return view('dashboard.invoice.donasi', compact('donasi'));
    }
}
