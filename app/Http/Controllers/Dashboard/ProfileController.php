<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function update(Request $request)
    {
        $user = User::find(auth()->user()->id);
        $data =[
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'no_telp' => $request->no_telp,
            'tanggal_lahir' => $request->tanggal_lahir,
            'alamat' => $request->alamat,
            'kota_asal' => $request->kota_asal,
            'jenis_kelamin' => $request->jenis_kelamin,
            'nama_bank' => $request->nama_bank,
            'nomor_rekening' => $request->nomor_rekening,
            'kategori_disabilitas' => $request->kategori_disabilitas,
            'pekerjaan' => $request->pekerjaan,
            'nama_perusahaan' => $request->nama_perusahaan,
            'status' => '1',
        ];

        if ($request->hasFile('foto_profil')) {
            $path = 'foto_profil/'.$data['foto_profil'];
            deleteImage($path);

            //upload gambar
            $file = $request->file('foto_profil');
            $patch = 'foto_profil';
            $name = $request->name;
            uploadImage($file, $patch, $name);

            $data['foto_profil'] = $request->foto_profil.'.'.$request->foto_profil->getClientOriginalExtension();
        }

        $user->update($data);

        return redirect()->back()->with('success', 'Data berhasil diubah');
    }
}
