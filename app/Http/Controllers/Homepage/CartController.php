<?php

namespace App\Http\Controllers\Homepage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use App\Models\Produk;
use App\Models\Transaksi;
use App\Models\DetailTransaksi;

class CartController extends Controller
{
    public function index()
    {
        $cart=Session::get('cart');
        $total=0;
        foreach ($cart as $key => $value) {
            $total+=$value['qty']*$value['harga'];
        }
        return view('homepage.cart.index',compact('cart', 'total'));
    }

    public function add(Request $request)
    {
        $product = Produk::find($request->id);
        $cart = Session::get('cart');

        $total=$product->harga*$request->qty;
        $cart[$product->id] = array(
            "id" => $product->id,
            "nama_produk" => $product->nama_produk,
            "harga" => $product->harga,
            "gambar" => $product->gambar_produk,
            "qty" => $request->qty,
            "total" => $total,
        );

        Session::put('cart', $cart);
        Session::flash('success','barang berhasil ditambah ke keranjang!');

        return redirect()->back();
    }

    public function checkout()
    {
        $cart=Session::get('cart');
        $total=0;
        foreach ($cart as $key => $value) {
            $total+=$value['qty']*$value['harga'];
        }
        return view('homepage.cart.checkout',compact('cart', 'total'));
    }

    public function checkout_add(Request $request)
    {
        $cart=Session::get('cart');

        $total=0;
        foreach ($cart as $key => $value) {
            $total+=$value['qty']*$value['harga'];
        }

        $transaksi=[
            'id_user' => $request->id_user,
            'tanggal_pembelian' => date('Y-m-d'),
            'status' => "0",
        ];

        $mTransaksi=Transaksi::create($transaksi);

        foreach ($cart as $key => $value) {
            $detail=[
                'id_transaksi' => $mTransaksi->id,
                'id_produk' => $value['id'],
                'jumlah' => $value['qty'],
            ];

            DetailTransaksi::create($detail);
        }

        return view('homepage.cart.payment', compact('total'));
    }

    public function delete($id)
    {
        $cart = Session::get('cart');
        unset($cart[$id]);
        Session::put('cart', $cart);
        return redirect()->route('cart.index');
    }
}
