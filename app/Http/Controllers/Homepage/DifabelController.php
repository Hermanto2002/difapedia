<?php

namespace App\Http\Controllers\Homepage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Produk;
use App\Models\Donasi;

class DifabelController extends Controller
{
    public function index()
    {
        $difabel = User::role('difabel')->get();
        return view('homepage.difabel.list', compact('difabel'));
    }

    public function profil($id)
    {
        $difabel = User::role('difabel')->findOrFail($id);
        $produk = Produk::where('id_difabel', $id)->get();
        return view('homepage.difabel.profil', compact('difabel', 'produk'));
    }

    public function donasi(Request $request)
    {
        $data=[
            'id_difabel' => $request->id_difabel,
            'id_donatur' => auth()->user()->id,
            'jumlah' => $request->jumlah,
            'pesan_semangat' => $request->pesan_semangat,
            'status' => '0',
        ];

        $total=$request->jumlah;
        Donasi::create($data);
        return view('homepage.cart.payment', compact('total'));
    }
}
