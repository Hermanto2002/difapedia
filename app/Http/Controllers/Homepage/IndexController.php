<?php

namespace App\Http\Controllers\Homepage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Produk;
use App\Models\Loker;

class IndexController extends Controller
{
    public function index()
    {
        $difabel=User::role('difabel')->get();
        $produk=Produk::all();
        $loker = Loker::all();

        $data=[
            'difabel'=>$difabel,
            'produk'=>$produk,
            'loker'=>$loker,
        ];

        return view('homepage.index', $data);
    }
}
