<?php

namespace App\Http\Controllers\Homepage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Loker;
use App\Models\Pelamar;

class LokerController extends Controller
{
    public function index()
    {
        $loker=Loker::all();
        return view('homepage.loker.list', compact('loker'));
    }

    public function detail($id)
    {
        $loker=Loker::find($id);
        return view('homepage.loker.detail', compact('loker'));
    }

    public function add(Request $request)
    {
        $data=[
            'id_loker'=>$request->id_loker,
            'id_difabel'=>auth()->user()->id,
        ];

        Pelamar::create($data);
        return redirect()->route('loker.index');
    }
}
