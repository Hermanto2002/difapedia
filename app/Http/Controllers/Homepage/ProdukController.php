<?php

namespace App\Http\Controllers\Homepage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Produk;

class ProdukController extends Controller
{
    public function index()
    {
        $produk = Produk::all();
        return view('homepage.produk.index', compact('produk'));
    }

    public function detail($id)
    {
        $produk = Produk::all();
        $produkDetail = Produk::find($id);
        return view('homepage.produk.detail', compact('produk','produkDetail'));
    }
}
