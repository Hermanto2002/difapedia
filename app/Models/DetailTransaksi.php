<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailTransaksi extends Model
{
    use HasFactory;
    protected $table = 'detail_transaksi';
    protected $guarded = [];

    function getProduk()
    {
        return $this->belongsTo('App\Models\Produk', 'id_produk');
    }

    function getTransaksi(){
        return $this->belongsTo('App\Models\Transaksi', 'id_transaksi');
    }
}
