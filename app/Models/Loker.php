<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Loker extends Model
{
    use HasFactory;
    protected $table="loker";
    protected $guarded=[];

    function getHrd(){
        return $this->belongsTo('App\Models\User','id_hrd');
    }

    function getPelamar(){
        return $this->hasMany('App\Models\Pelamar','id_loker');
    }

    function countPelamar(){
        return $this->hasMany('App\Models\Pelamar','id_loker')->count();
    }
}
