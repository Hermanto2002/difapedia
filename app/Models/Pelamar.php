<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pelamar extends Model
{
    use HasFactory;
    protected $table = 'pelamar';
    protected $guarded = [];

    function getLoker()
    {
        return $this->belongsTo('App\Models\Loker', 'id_loker');
    }

    function getDifabel()
    {
        return $this->belongsTo('App\Models\User', 'id_difabel');
    }

}
