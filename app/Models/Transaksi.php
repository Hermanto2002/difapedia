<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    use HasFactory;
    protected $table = 'transaksi';
    protected $guarded = [];

    function getUser()
    {
        return $this->belongsTo('App\Models\User', 'id_user');
    }

    function getDetail()
    {
        return $this->hasMany('App\Models\DetailTransaksi', 'id_transaksi');
    }

    function getTotal()
    {
        $total = 0;
        foreach ($this->getDetail as $detail) {
            $total += $detail->jumlah*$detail->getProduk->harga;
        }
        return $total;
    }
}
