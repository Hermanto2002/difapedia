<!DOCTYPE html>
<html>

<head>
    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Login | Difa - Difabel Indonesia</title>
    <meta name="keywords" content="" />
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('homepage/img/favicon.png')}}" type="image/x-icon" />
    <link rel="apple-touch-icon" href="{{ asset('homepage/img/apple-touch-icon.png')}}">
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('homepage/img/favicon.ico')}}" type="image/x-icon" />
    <link rel="apple-touch-icon" href="{{ asset('homepage/img/apple-touch-icon.png')}}">
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">
    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light"
        rel="stylesheet" type="text/css">
    <!-- Vendor CSS -->
    <link rel="stylesheet" href="{{ asset('homepage/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/vendor/fontawesome-free/css/all.min.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/vendor/animate/animate.min.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/vendor/simple-line-icons/css/simple-line-icons.min.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/vendor/owl.carousel/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/vendor/owl.carousel/assets/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/vendor/magnific-popup/magnific-popup.min.css')}}">
    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{ asset('homepage/css/theme.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/css/theme-elements.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/css/theme-blog.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/css/theme-shop.css')}}">
    <!-- Demo CSS -->
    <!-- Skin CSS -->
    <link rel="stylesheet" href="{{ asset('homepage/css/skins/default.css')}}">
    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="{{ asset('homepage/css/custom.css')}}">
    <!-- Head Libs -->
    <script src="{{ asset('homepage/vendor/modernizr/modernizr.min.js')}}"></script>
</head>

<body>
    <div class="body">
        <header id="header" class="header-effect-shrink"
            data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyChangeLogo': true, 'stickyStartAt': 30, 'stickyHeaderContainerHeight': 70}">
            <div class="header-body border-top-0 header-body-bottom-border">
                <div class="header-top header-top-borders">
                    <div class="container h-100">
                        <div class="header-row h-100">
                            <div class="header-column justify-content-start">
                                <div class="header-row">
                                    <nav class="header-nav-top">
                                        <ul class="nav nav-pills">
                                            <li class="nav-item nav-item-borders py-2">
                                                <span class="pl-0">
                                                    <i class="far fa-dot-circle text-4 text-color-primary"
                                                        style="top: 1px;"></i>
                                                    Live Donasi <i class="fas fa-angle-right"></i> <strong> Ahmad Abdul
                                                        : Rp.
                                                        50.000</strong>
                                                </span>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header-container container">
                    <div class="header-row">
                        <div class="header-column">
                            <div class="header-row">
                                <div class="header-logo">
                                    <a href="index.html">
                                        <img alt="" width="160" height="52" data-sticky-width="125"
                                            data-sticky-height="40" src="{{ asset('homepage/img/logo-new.png')}}">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="header-column justify-content-end">
                            <div class="header-row">
                                <div class="header-nav header-nav-links order-2 order-lg-1">
                                    <div
                                        class="header-nav-main header-nav-main-square header-nav-main-dropdown-no-borders header-nav-main-effect-2 header-nav-main-sub-effect-1">
                                        <nav class="collapse">
                                            <ul class="nav nav-pills" id="mainNav">
                                                <li class="dropdown">
                                                    <a class="dropdown-item dropdown-toggle active" href="index.html">
                                                        Beranda
                                                    </a>
                                                </li>
                                                <li class="dropdown">
                                                    <a class="dropdown-item dropdown-toggle" href="#">
                                                        Donasi
                                                    </a>
                                                    <ul class="dropdown-menu">
                                                        <li class="dropdown-submenu">
                                                            <a class="dropdown-item" href="#">Sub 1</a>
                                                        </li>
                                                        <li class="dropdown-submenu">
                                                            <a class="dropdown-item" href="#">Sub 2</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="dropdown">
                                                    <a class="dropdown-item dropdown-toggle" href="#">
                                                        Tentang Kami
                                                    </a>
                                                </li>
                                                <li class="dropdown">
                                                    <a class="dropdown-item dropdown-toggle" href="#">
                                                        Kontak Kami
                                                    </a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                    <button class="btn header-btn-collapse-nav" data-toggle="collapse"
                                        data-target=".header-nav-main nav">
                                        <i class="fas fa-bars"></i>
                                    </button>
                                </div>
                                <div
                                    class="header-nav-features header-nav-features-no-border header-nav-features-lg-show-border order-1 order-lg-2">
                                    <div class="header-nav-feature header-nav-features-search d-inline-flex">
                                        <a href="#" class="header-nav-features-toggle" data-focus="headerSearch"><i
                                                class="fas fa-search header-nav-top-icon"></i></a>
                                        <div class="header-nav-features-dropdown header-nav-features-dropdown-mobile-fixed"
                                            id="headerTopSearchDropdown">
                                            <form role="search" action="page-search-results.html" method="get">
                                                <div class="simple-search input-group">
                                                    <input class="form-control text-1" id="headerSearch" name="q"
                                                        type="search" value="" placeholder="Search...">
                                                    <span class="input-group-append">
                                                        <button class="btn" type="submit">
                                                            <i class="fa fa-search header-nav-top-icon"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div role="main" class="main">
            <section class="page-header page-header-classic">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <ul class="breadcrumb">
                                <li><a href="#">Beranda</a></li>
                                <li class="active">Masuk / Daftar</li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col p-static">
                            <h1 data-title-border>Masuk</h1>
                        </div>
                    </div>
                </div>
            </section>
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="featured-boxes">
                            <div class="row">
                                <div class="col-md-6 offset-md-3">
                                    <div class="featured-box featured-box-primary text-left mt-5">
                                        <div class="box-content">
                                            <h4 class="color-primary font-weight-semibold text-4 text-uppercase mb-3">
                                                Belum Punya Akun? <a class="btn btn-primary" href="{{ route('register')}}">Daftar disini</a></h4>
                                            <form method="POST" action="{{ route('login') }}" id="frmSignIn" class="needs-validation">
                                                @csrf
                                                <div class="form-row">
                                                    <div class="form-group col">
                                                        <label class="font-weight-bold text-dark text-2">Username /
                                                            E-mail</label>
                                                        <input type="text" name="email" value="{{ old('email') }}"
                                                            class="form-control form-control-lg @error('email') is-invalid @enderror"
                                                            required>
                                                        @error('email')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col">
                                                        <label
                                                            class="font-weight-bold text-dark text-2">Password</label>
                                                        <input type="password" name="password"
                                                            class="form-control form-control-lg @error('password') is-invalid @enderror" required>
                                                        @error('password')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-lg-6">
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input"
                                                                id="rememberme">
                                                            <label class="custom-control-label text-2"
                                                                for="rememberme">Ingat
                                                                Saya</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <input type="submit" value="Masuk"
                                                            class="btn btn-primary btn-modern float-right"
                                                            data-loading-text="Loading...">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer id="footer" class="mt-0">
        <div class="footer-copyright">
            <div class="container py-2">
                <div class="row py-4">
                    <div class="col d-flex align-items-center justify-content-center">
                        <p class="text-center"><strong>DIFAPEDIA.ID - DIFABEL INDONESIA</strong> <br> ©Copyright 2021.
                            All
                            Rights Reserved. </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    </div>
    <!-- Vendor -->
    <script src="{{ asset('homepage/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/jquery.appear/jquery.appear.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/jquery.easing/jquery.easing.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/jquery.cookie/jquery.cookie.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/popper/umd/popper.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/common/common.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/jquery.validation/jquery.validate.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/jquery.gmap/jquery.gmap.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/jquery.lazyload/jquery.lazyload.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/isotope/jquery.isotope.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/owl.carousel/owl.carousel.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/vide/jquery.vide.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/vivus/vivus.min.js')}}"></script>
    <!-- Theme Base, Components and Settings -->
    <script src="{{ asset('homepage/js/theme.js')}}"></script>
    <!-- Theme Custom -->
    <script src="{{ asset('homepage/js/custom.js')}}"></script>
    <!-- Theme Initialization Files -->
    <script src="{{ asset('homepage/js/theme.init.js')}}"></script>
</body>

</html>
