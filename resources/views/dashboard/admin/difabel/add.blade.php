@extends('layouts.dashboard')

@section('content')
<form action="{{ route('admin.difabel.store')}}" method="POST" enctype="multipart/form-data">
    @csrf
<div class="row">
    <h2>Informasi Lengkap</h2>
    <br>
        <div class="col-sm-12 mt-4">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label class="control-label">Nama Lengkap</label>
                    <input type="text" name="name" class="form-control">
                </div>
                <div class="form-group col-md-6">
                    <label class="control-label">Kategori Disabilitas</label>
                    <select class="form-control" name="kategori_disabilitas">
                        <option selected disabled>-</option>
                        <option value="Disabilitas fisik">Disabilitas fisik</option>
                        <option value="Disabilitas sensorik">Disabilitas sensorik</option>
                        <option value="Disabilitas mental">Disabilitas mental</option>
                        <option value="Disabilitas intelektual">Disabilitas intelektual</option>
                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label class="control-label">Email</label>
                    <input type="email" name="email" class="form-control">
                </div>
                <div class="form-group col-md-4">
                    <label class="control-label">Password</label>
                    <input type="password" name="password" class="form-control">
                </div>
                <div class="form-group col-md-4">
                    <label class="control-label">No. Telp</label>
                    <input type="number" name="no_telp" class="form-control">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-5">
                    <label class="control-label">Alamat</label>
                    <input type="text" name="alamat" class="form-control">
                </div>
                <div class="form-group col-md-4">
                    <label class="control-label">Asal Kota</label>
                    <input type="text" name="kota_asal" class="form-control">
                </div>
                <div class="form-group col-md-3">
                    <label class="control-label">Tanggal Lahir</label>
                    <input type="date" name="tanggal_lahir" class="form-control">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-3">
                    <label class="control-label">Jenis Kelamin</label>
                    <select name="jenis_kelamin" class="form-control">
                        <option>-</option>
                        <option value="L">Laki - Laki</option>
                        <option value="P">Perempuan</option>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label class="control-label">Nama Bank</label>
                    <select name="nama_bank" class="form-control">
                        <option>-</option>
                        <option value="BCA">BCA</option>
                        <option value="BNI">BNI</option>
                        <option value="BRI">BRI</option>
                        <option value="Mandiri">Mandiri</option>
                        <option value="BTN">BTN</option>
                        <option value="Permata">Permata</option>
                    </select>
                </div>
                <div class="form-group col-md-5">
                    <label class="control-label">No. Rekening</label>
                    <input type="number" name="nomor_rekening" class="form-control">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-7">
                    <label class="control-label">Tentang Difabel</label>
                    <textarea name="tentang_difabel" class="form-control" rows="6"></textarea>
                </div>
                <div class="form-group col-md-5">
                    <label class="control-label">Upload foto profil</label>
                    <input type="file" name="foto_profil" class="form-control">
                </div>
            </div>

            <div class="form-row">
                <div class="col-sm-6">
                    <div class="form-group">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="text-sm-right">
                        <button class="btn btn-gradient-success">TAMBAH DATA</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection
