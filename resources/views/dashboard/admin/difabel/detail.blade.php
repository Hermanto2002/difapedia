@extends('layouts.dashboard')

@section('content')
<form action="{{ route('admin.difabel.update')}}" method="POST" enctype="multipart/form-data">
@csrf
<input type="hidden" name="id" value="{{ $difabel['id'] }}">
<div class="row">
    <h2>Informasi Lengkap</h2>
    <br>
    <div class="col-sm-12 mt-4">
        <div class="form-row">
            <div class="form-group col-md-6">
                <label class="control-label">Nama Lengkap</label>
                <input type="text" name="name" value="{{$difabel['name']}}" class="form-control">
            </div>
            <div class="form-group col-md-3">
                <label class="control-label">Email</label>
                <input type="email" name="email" value="{{$difabel['email']}}" class="form-control">
            </div>
            <div class="form-group col-md-3">
                <label class="control-label">No. Telp</label>
                <input type="number" name="no_telp" value="{{$difabel['no_telp']}}" class="form-control">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-5">
                <label class="control-label">Alamat</label>
                <input type="text" name="alamat" value="{{$difabel['alamat']}}" class="form-control">
            </div>
            <div class="form-group col-md-4">
                <label class="control-label">Asal Kota</label>
                <input type="text" name="kota_asal" value="{{$difabel['kota_asal']}}" class="form-control">
            </div>
            <div class="form-group col-md-3">
                <label class="control-label">Tanggal Lahir</label>
                <input type="date" name="tanggal_lahir" value="{{$difabel['tanggal_lahir']}}" class="form-control">
            </div>
        </div>
        <div class="form-group">

        </div>
        <div class="form-group">
            <label class="control-label">Tentang Difabel</label>
            <textarea class="form-control" name="tentang_difabel" rows="6">{{$difabel['tentang_difabel']}}</textarea>
        </div>
        <div class="form-row">
            <div class="form-group col-md-5">
                <label class="control-label">Total Donasi</label>
                <input type="number" class="form-control">
            </div>
            <div class="form-group col-md-4">
                <label class="control-label">Total Produk</label>
                <input type="number" class="form-control">
            </div>
            <div class="form-group col-md-3">
                <label class="control-label">Status</label>
                <?php
                $aktif="";
                    $nonaktif="";
                    if ($difabel['status']=="0") {
                        $nonaktif="selected";
                    }
                    else{
                        $aktif="selected";
                    }
                ?>
                <select class="form-control">
                    <option {{$aktif}}>Aktif</option>
                    <option {{$nonaktif}}>Non-Aktif</option>
                </select>
            </div>
        </div>
        <div class="form-row">
            <div class="col-sm-6">
                <div class="form-group">

                </div>
            </div>
            <div class="col-sm-6">
                <div class="text-sm-right">
                    <button class="btn btn-gradient-success">SIMPAN PERUBAHAN</button>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
@endsection
