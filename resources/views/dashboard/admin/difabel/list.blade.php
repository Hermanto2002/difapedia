@extends('layouts.dashboard')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">List Difabel</h4>
            </div>
            <div class="table-overflow container">
                <table class="table table-striped table-bordered datatable">
                    <thead>
                        <tr>
                            <td class="text-dark text-semibold">Nama</td>
                            <td class="text-dark text-semibold">Tanggal Lahir</td>
                            <td class="text-dark text-semibold">Kota Asal</td>
                            <td class="text-dark text-semibold">Email</td>
                            <td class="text-dark text-semibold">No. Telp</td>
                            <td class="text-dark text-semibold">Total Donasi</td>
                            <td class="text-dark text-semibold">Status</td>
                            <td class="text-dark text-semibold">Aksi</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($difabel as $item)
                        <tr>
                            <td>
                                <div class="list-item">
                                    <div class="info">
                                        <span class="title p-t-10 text-semibold">{{ $item->name }}</span>
                                    </div>
                                </div>
                            </td>
                            <td> {{ $item->tanggal_lahir }} </td>
                            <td> {{ $item->kota_asal }}</td>
                            <td> {{ $item->email }}</td>
                            <td> {{ $item->no_telp }}</td>
                            <td><span class="text-primary"><strong>Rp. {{$item->getDonasi->where('status','1')->sum('jumlah')}}</strong></span>
                            </td>
                            <td><span class="badge badge-pill badge-primary">Aktif</span></td>
                            <td>
                                <a href="{{ route('admin.difabel.detail', ['id' => $item->id])}}">
                                    <button
                                        class="btn btn-icon btn-gradient-success mt-3 btn-sm">
                                        <i class="mdi mdi-eye"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
