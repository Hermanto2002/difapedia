@extends('layouts.dashboard')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Semua Produk Difabel</h4>
            </div>
            <div class="table-overflow container">
                <table class="table table-striped table-bordered datatable">
                    <thead>
                        <tr>
                            <td class="text-dark text-semibold">ID</td>
                            <td class="text-dark text-semibold">Nama Produk</td>
                            <td class="text-dark text-semibold">Oleh</td>
                            <td class="text-dark text-semibold">Deskripsi</td>
                            <td class="text-dark text-semibold">Jumlah</td>
                            <td class="text-dark text-semibold">Harga</td>
                            <td class="text-dark text-semibold">Status</td>
                            <td class="text-dark text-semibold">Aksi</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($produk as $item)
                            <tr>
                            <td>{{ $item->id }}</td>
                            <td>
                                <div class="list-item">
                                    <div class="info">
                                        <span class="title p-t-10 text-semibold">{{ $item->nama_produk }}</span>
                                    </div>
                                </div>
                            </td>
                            <td> {{ $item->getDifabel->name }} </td>
                            <td> {{ $item->deskripsi_produk }} </td>
                            <td> 11</td>
                            <td><span class="text-primary"><strong>{{ $item->harga }}</strong></span></td>
                            <td><span class="badge badge-pill badge-primary">Aktif</span></td>
                            <td>
                                <a href="javascript:void(0)" data-toggle="modal" data-target="#modalCekProduk">
                                    <button class="btn btn-icon btn-gradient-success mt-3 btn-sm">
                                        <i class="mdi mdi-eye"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
