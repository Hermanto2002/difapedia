@extends('layouts.dashboard')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">List Donatur</h4>
            </div>
            <div class="table-overflow container">
                <table class="table table-striped table-bordered datatable">
                    <thead>
                        <tr>
                            <td class="text-dark text-semibold">Nama</td>
                            <td class="text-dark text-semibold">Email</td>
                            <td class="text-dark text-semibold">No. Telp</td>
                            <td class="text-dark text-semibold">Alamat</td>
                            <td class="text-dark text-semibold">Pekerjaan</td>
                            <td class="text-dark text-semibold">Status</td>
                            <td class="text-dark text-semibold">Aksi</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($donatur as $item)
                        <tr>
                            <td>
                                <div class="list-item">
                                    <div class="info">
                                        <span class="title p-t-10 text-semibold">{{$item->name}}</span>
                                    </div>
                                </div>
                            </td>
                            <td>{{ $item->email }}</td>
                            <td>{{ $item->no_telp }}</td>
                            <td>{{ $item->alamat }}</td>
                            <td>{{ $item->pekerjaan }}</td>
                            <td><span class="badge badge-pill badge-primary">Aktif</span></td>
                            <td>
                                <a href="javascript:void(0)" data-toggle="modal" data-target="#modalCekDonatur{{$item->id}}">
                                    <button class="btn btn-icon btn-gradient-success mt-3 btn-sm">
                                        <i class="mdi mdi-eye"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>

                        <div class="modal fade" id="modalCekDonatur{{$item->id}}">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4>Detail Profil Donatur</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-8 offset-sm-2">
                                                    <div class="form-row">
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Nama Donatur</label>
                                                            <input type="text" class="form-control"
                                                                value="{{$item->name}}">
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Email</label>
                                                            <input type="email" class="form-control"
                                                                value="{{$item->email}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">No. Telp</label>
                                                            <input type="number" class="form-control"
                                                                value="{{$item->no_telp}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Pekerjaan</label>
                                                            <input type="text" class="form-control" value="{{$item->pekerjaan}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="form-group col-md-12">
                                                            <label class="control-label">Alamat</label>
                                                            <textarea class="form-control" rows="3"
                                                                value="">{{$item->alamat}}</textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="col-sm-12">
                                                            <div class="text-center">
                                                                <button class="btn btn-danger btn-full">Hapus
                                                                    Donatur</button>
                                                                <button class="btn btn-primary btn-full">Simpan
                                                                    Perubahan</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
