@extends('layouts.dashboard')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Transaksi Produk Terbaru</h4>
            </div>
            <div class="table-overflow container">
                <table class="table table-striped table-bordered datatable">
                    <thead>
                        <tr>
                            <td class="text-dark text-semibold">Nama User</td>
                            <td class="text-dark text-semibold">Order ID</td>
                            <td class="text-dark text-semibold">Tgl Masuk</td>
                            <td class="text-dark text-semibold">Jumlah</td>
                            <td class="text-dark text-semibold">Status</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($transaksi as $item)
                        <tr>
                            <td>
                                <div class="list-item">
                                    <div class="info">
                                        <span class="title p-t-10 text-semibold">{{$item->getUser->name}}</span>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <a href="javascript:void(0)" data-toggle="modal"
                                    data-target="#modalCekPembayaranProduk{{$item->id}}">
                                    #{{$item->id}}
                                </a>
                            </td>
                            <td>{{$item->tanggal_pembelian}}</td>
                            <td>{{$item->getTotal()}}</td>
                            <td><span class="badge badge-pill">
                                @if ($item->status == 0)
                                Pending
                                @else
                                Diterima
                                @endif
                            </span></td>
                        </tr>
                        <div class="modal fade" id="modalCekPembayaranProduk{{$item->id}}">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4>Konfirmasi Pembayaran Produk</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-8 offset-sm-2">
                                                    <div class="form-row">
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Nama User</label>
                                                            <input type="text" class="form-control" value="{{$item->getUser->name}}">
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Order ID</label>
                                                            <input type="number" class="form-control" value="#{{$item->id}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="form-group col-md-4">
                                                            <label class="control-label">Total</label>
                                                            <input type="number" class="form-control" value="{{$item->getTotal()}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="form-group col-md-12">
                                                            <label class="control-label">Bukti Pembayaran</label>
                                                            <br>
                                                            <a href="{{ asset('foto_pembayaran_produk/'.$item->bukti_pembayaran)}}" target="_blank"> Lihat
                                                                Bukti Pembayaran</a>

                                                        </div>
                                                    </div>
                                                    <form action="{{ route('admin.produk.update')}}" method="POST">
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{$item->id}}">
                                                    <div class="form-row">
                                                        <div class="form-group col-md-12">
                                                            <label class="control-label">Status</label>
                                                            <select class="form-control" name="status">
                                                                <option value="1">Diterima</option>
                                                                <option value="0">Dipending</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="col-sm-12">
                                                            <div class="text-center">
                                                                <button type="submit" class="btn btn-gradient-success btn-full">Konfirmasi
                                                                    Pembayaran</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
