@extends('layouts.dashboard')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="my-4">
            <h2>Selamat Datang, <strong><span class="text-info">Admin</span></strong>!</h2>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header border bottom">
                <h4 class="card-title">Informasi Akun</h4>
            </div>
            <div class="card-body">
                <form action="{{ route('profile.update') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-sm-8 offset-sm-2">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label class="control-label">Email</label>
                                    <input type="text" class="form-control" name="email" value="{{ $user['email']}}">
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label">Password</label>
                                    <input type="password" class="form-control" name="password" name="{{ $user['password']}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Nama Lengkap</label>
                                <input type="text" class="form-control" name="name" value="{{ $user['name']}}">
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label class="control-label">No. Telp</label>
                                    <input type="number" class="form-control" name="no_telp" value="{{ $user['no_telp']}}">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">Foto Profil</label>
                                        <input type="file" class="form-control" name="foto_profil">
                                    </div>
                                </div>
                            </div>
                            <br><br>
                            <div class="form-row">
                                <div class="col-sm-12">
                                    <div class="text-sm-right">
                                        <a href="#">
                                            <button type="submit" class="btn btn-info btn-full"
                                                style="width: 100%;">UPDATE
                                                PROFIL</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
@endsection
