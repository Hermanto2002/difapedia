@extends('layouts.dashboard')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="my-4">
            <h2>Selamat Datang, <strong><span class="text-info">Rahmad Abdullah</span></strong>!</h2>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">
                    <span>Total Donasi Diterima</span>
                    <span class="d-block text-gray font-size-13 m-t-10">Per-tanggal {{ date("F j, Y,") }}</span>
                </h4>
            </div>
            <div class="card-body">
                <h2 class="font-weight-light font-size-28" style="margin-bottom:-4px;margin-top:-20px;">
                    Rp. {{$jmlDonasi}}</h2>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Total Penjualan Diterima</h4>
                <span class="d-block text-gray font-size-13 m-t-10">Per-tanggal {{ date("F j, Y,") }}</span>
                </h4>
            </div>
            <div class="card-body">
                <h2 class="font-weight-light font-size-28" style="margin-bottom:-4px;margin-top:-20px;">
                    Rp. {{$jmlPenjualan}}</h2>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Donasi Terbaru</h4>
            </div>
            <div class="table-overflow">
                <table class="table table-lg">
                    <thead>
                        <tr>
                            <td class="text-dark text-semibold">Nama User</td>
                            <td class="text-dark text-semibold">Order ID</td>
                            <td class="text-dark text-semibold">Tgl Masuk</td>
                            <td class="text-dark text-semibold">Jumlah</td>
                            <td class="text-dark text-semibold">Status</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($donasi as $item)
                        <tr>
                            <td>
                                <div class="list-item">
                                    <div class="info">
                                        <span class="title p-t-10 text-semibold">Agus
                                            Sumail</span>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <a href="javascript:void(0)" data-toggle="modal"
                                    data-target="#modalCekPembayaran{{ $item->id }}">
                                    #{{ $item->id }}
                                </a>

                            </td>
                            <td>11 November 2021</td>
                            <td> Rp. 150.000</td>
                            <td>
                                <span class="badge badge-pill badge-primary">
                                    @if ($item->status == 0)
                                    Pending
                                    @else
                                    Diterima
                                    @endif
                                </span>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Transaksi Produk Terbaru</h4>
            </div>
            <div class="table-overflow">
                <table class="table table-lg">
                    <thead>
                        <tr>
                            <td class="text-dark text-semibold">Nama User</td>
                            <td class="text-dark text-semibold">Order ID</td>
                            <td class="text-dark text-semibold">Tgl Masuk</td>
                            <td class="text-dark text-semibold">Jumlah</td>
                            <td class="text-dark text-semibold">Status</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($penjualan as $item)
                        <tr>
                            <td>
                                <div class="list-item">
                                    <div class="info">
                                        <span class="title p-t-10 text-semibold">{{$item->getTransaksi->getUser->name}}</span>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <a href="javascript:void(0)" data-toggle="modal"
                                    data-target="#modalCekPembayaranProduk">
                                    #{{$item->id}}
                                </a>
                            </td>
                            <td>{{$item->getTransaksi->tanggal_pembelian}}</td>
                            <td> Rp. {{$item->getTransaksi->getTotal()}}</td>
                            <td><span class="badge badge-pill badge-warning">
                                    @if ($item->getTransaksi->status == 0)
                                    Pending
                                    @else
                                    Diterima
                                    @endif
                                </span></td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">List Lowongan Kerja</h4>
            </div>
            <div class="table-overflow">
                <table class="table table-lg">
                    <thead>
                        <tr>
                            <td class="text-dark text-semibold">Nama Loker</td>
                            <td class="text-dark text-semibold">Deskripsi</td>
                            <td class="text-dark text-semibold">Perusahaan</td>
                            <td class="text-dark text-semibold">Deadline</td>
                            <td class="text-dark text-semibold">Aksi</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($loker as $item)
                        <tr>
                            <td>
                                <div class="list-item">
                                    <div class="info">
                                        <span class="title p-t-10 text-semibold">{{$item->nama_loker}}</span>
                                    </div>
                                </div>
                            </td>
                            <td width="40%">{{ $item->deskripsi }}</td>
                            <td> {{ $item->getHrd->nama_perusahaan }}</td>
                            <td> {{$item->deadline}}</td>
                            <td>
                                <a href="{{ route('loker.detail', ['id' => $item->id])}}">
                                    <button class="btn-icon btn btn-info"> Lihat Loker</button>
                                </a>
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
