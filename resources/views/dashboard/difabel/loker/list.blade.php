@extends('layouts.dashboard')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">List Lowongan Kerja</h4>
            </div>
            <div class="table-overflow container">
                <table class="table table-striped table-bordered datatable">
                    <thead>
                        <tr>
                            <td class="text-dark text-semibold">Nama Loker</td>
                            <td class="text-dark text-semibold">Deskripsi</td>
                            <td class="text-dark text-semibold">Perusahaan</td>
                            <td class="text-dark text-semibold">Deadline</td>
                            <td class="text-dark text-semibold">Jumlah Pelamar</td>
                            <td class="text-dark text-semibold">Status</td>
                            <td class="text-dark text-semibold">Aksi</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($loker as $item)
                        <tr>
                            <td>
                                <div class="list-item">
                                    <div class="info">
                                        <span class="title p-t-10 text-semibold">{{$item->nama_loker}}</span>
                                    </div>
                                </div>
                            </td>
                            <td width="40%">
                                {{ $item->deskripsi }}
                            </td>
                            <td>{{$item->getHrd->nama_perusahaan}}</td>
                            <td>{{$item->deadline}}</td>
                            <td>{{$item->countPelamar()}}</td>
                            <td><span class="badge badge-pill badge-primary">Aktif</span></td>
                            <td>
                                <a href="{{ route('loker.detail', ['id' => $item->id])}}">
                                    <button class="btn-icon btn btn-info"> Lihat Loker</button>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
