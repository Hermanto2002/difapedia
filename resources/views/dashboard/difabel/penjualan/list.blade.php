@extends('layouts.dashboard')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="my-4">
            <h2>Halaman <strong><span class="text-primary">List Penjualan</span></strong></h2>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Transaksi Produk Terbaru</h4>
            </div>
            <div class="table-overflow container">
                <table class="table table-striped table-bordered datatable">
                    <thead>
                        <tr>
                            <td class="text-dark text-semibold">Nama Barang</td>
                            <td class="text-dark text-semibold">Tgl Masuk</td>
                            <td class="text-dark text-semibold">Jumlah</td>
                            <td class="text-dark text-semibold">Total Harga</td>
                            <td class="text-dark text-semibold">Status</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($detailtransaksi as $item)
                        <tr>
                            <td>
                                <div class="list-item">
                                    <div class="info">
                                        <span class="title p-t-10 text-semibold">{{$item->getProduk->nama_produk}}</span>
                                    </div>
                                </div>
                            </td>
                            <td>{{$item->getTransaksi->tanggal_pembayaran}}</td>
                            <td>{{$item->jumlah}}</td>
                            <td>{{$item->getProduk->harga}}</td>
                            <td><span class="badge badge-pill badge-primary">
                            @if($item->getTransaksi->status == 0)
                                <span class="text-semibold">Belum Dibayar</span>
                            @else
                                <span class="text-semibold">Sudah Dibayar</span>
                            @endif
                            </span></td>
                        </tr>
                        @endforeach


                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>




@endsection
