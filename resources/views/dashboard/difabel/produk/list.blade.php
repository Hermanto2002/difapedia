@extends('layouts.dashboard')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="my-4">
            <h2>Halaman <strong><span class="text-primary">List Produk</span></strong></h2>
            <br>
            <a href="javascript:void(0)" data-toggle="modal" data-target="#modalAddProduk">
                <button class="btn btn-primary">Tambah Produk</button>
            </a>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <br>
            <div class="table-overflow container">
                <table class="table table-striped table-bordered datatable">
                    <thead>
                        <tr>
                            <th>Nama Produk</th>
                            <th>Jumlah</th>
                            <th>Harga</th>
                            <th>Deskripsi</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($produk as $item)
                        <tr>
                            <td>
                                <div class="list-media">
                                    <div class="list-item">
                                        <div class="media-img">
                                            <img src="{{ asset('produk/'.$item->gambar_produk)}}" alt="">
                                        </div>
                                        <div class="info">
                                            <span class="title">{{$item->nama_produk}}</span>
                                            <span class="sub-title">ID {{$item->id}}</span>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td>{{$item->jumlah}}</td>
                            <td>Rp. {{$item->harga}}</td>
                            <td width="30%">{{$item->deskripsi_produk}}</td>
                            <td><span class="badge badge-pill badge-primary">
                                @if($item->status == 0){
                                    <span class="text-semibold">Non Aktif</span>
                                @else
                                    <span class="text-semibold">Aktif</span>
                                @endif
                            </span></td>
                            <td class="text-center font-size-18">
                                <a href="javascript:void(0)" data-toggle="modal" data-target="#modalCekProduk{{$item->id}}"
                                    class="text-gray m-r-15"><i class="ti-pencil"></i></a>
                                <a href="" class="text-gray"><i class="ti-trash"></i></a>
                            </td>
                        </tr>

                        <!-- Modal Cek Produk-->
                        <div class="modal fade" id="modalCekProduk{{$item->id}}">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4>Detail Produk</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-row">
                                                        <div class="form-group col-md-9">
                                                            <label class="control-label">Nama Produk</label>
                                                            <input type="text" name="nama_produk" class="form-control"
                                                                value="{{$item->nama_produk}}">
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="form-group col-md-12">
                                                            <label class="control-label">Deskripsi Produk</label>
                                                            <textarea class="form-control" name="deskripsi_produk"
                                                                rows="3">{{$item->deskripsi_produk}}</textarea>
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Jumlah</label>
                                                            <input type="number" value="{{ $item->jumlah }}" name="jumlah" class="form-control" value="11">
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Harga</label>
                                                            <input type="number" class="form-control" value="{{ $item->harga }}" name="harga">
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="form-group col-md-6">
                                                            <label class="control-label">Gambar</label>
                                                            <input type="file" name="gambar_produk" class="form-control">
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <img src="{{ asset('produk/'.$item->gambar_produk)}}" width="40%">
                                                        </div>
                                                    </div>

                                                    <div class="form-row">
                                                        <div class="form-group col-md-12">
                                                            <label class="control-label">Status</label>
                                                            <select class="form-control" name="status">
                                                                <option value="1">Aktif</option>
                                                                <option value="0">Non-Aktif</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="col-sm-12">
                                                            <div class="text-center">
                                                                <a href="{{ route('difabel.produk.delete', [$item->id])}}" class="btn btn-gradient-danger btn-full">Hapus
                                                                    Produk</a>
                                                                <button type="submit"
                                                                    class="btn btn-gradient-success btn-full">Konfirmasi
                                                                    Perubahan</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<!-- Modal Add Produk-->
<div class="modal fade" id="modalAddProduk">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Tambah Produk</h4>
            </div>
            <div class="modal-body">
                <div class="card-body">
                    <form action="{{ route('difabel.produk.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label class="control-label">Nama Produk</label>
                                        <input type="text" name="nama_produk" class="form-control" value="">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label class="control-label">Deskripsi Produk</label>
                                        <textarea class="form-control" name="deskripsi_produk" rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label class="control-label">Jumlah</label>
                                        <input type="number" name="jumlah" class="form-control" value="">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label">Harga</label>
                                        <input type="number" name="harga" class="form-control" value="">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label class="control-label">Gambar</label>
                                        <input type="file" name="gambar_produk" class="form-control">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <img src="" width="40%">
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label class="control-label">Status</label>
                                        <select class="form-control" name="status">
                                            <option value="1">Aktif</option>
                                            <option value="0">Non-Aktif</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-sm-12">
                                        <div class="text-center">
                                            <button
                                                class="btn btn-danger btn-full">Batalkan</button>
                                            <button type="submit" class="btn btn-primary btn-full">Simpan
                                                Produk</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
