@extends('layouts.dashboard')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="my-4">
            <h2>Selamat Datang, <strong><span class="text-info">Rahmad Abdullah</span></strong>!</h2>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card">
            <div class="card-header border bottom">
                <h4 class="card-title">Informasi Akun</h4>
            </div>
            <div class="card-body">
                <form action="{{ route('profile.update')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-sm-12 mt-4">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label class="control-label">Nama Lengkap</label>
                                    <input type="text" class="form-control" name="name" value="{{ $user['name'] }}">
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label">Kategori Disabilitas</label>
                                    <select class="form-control" name="kategori_disabilitas">
                                        <option selected disabled>{{ $user['kategori_disabilitas'] }}</option>
                                        <option value="Disabilitas fisik">Disabilitas fisik</option>
                                        <option value="Disabilitas sensorik">Disabilitas sensorik</option>
                                        <option value="Disabilitas mental">Disabilitas mental</option>
                                        <option value="Disabilitas intelektual">Disabilitas intelektual</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label class="control-label">Email</label>
                                    <input type="email" name="email" class="form-control" value="{{ $user['email'] }}">
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label">Password</label>
                                    <input type="password" name="password" class="form-control" value="">
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label">No. Telp</label>
                                    <input type="number" name="no_telp" class="form-control"
                                        value="{{ $user['no_telp'] }}">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-5">
                                    <label class="control-label">Alamat</label>
                                    <input type="text" name="alamat" class="form-control" name="{{ $user['alamat'] }}">
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label">Asal Kota</label>
                                    <input type="text" name="asal_kota" class="form-control" {{ $user['asal_kota'] }}>
                                </div>
                                <div class="form-group col-md-3">
                                    <label class="control-label">Tanggal Lahir</label>
                                    <input type="date" name="tanggal_lahir" class="form-control"
                                        {{ $user['tanggal_lahir'] }}>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label class="control-label">Jenis Kelamin</label>
                                    <select class="form-control" name="jumlah_kelamin">
                                        <option disabled selected>{{ $user['jenis_kelamin'] }}</option>
                                        <option value="L">Laki - Laki</option>
                                        <option value="P">Perempuan</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label">Nama Bank</label>
                                    <select class="form-control" name="nama_bank">
                                        <option disabled selected>{{ $user['nama_bank'] }}</option>
                                        <option value="BCA">BCA</option>
                                        <option value="BNI">BNI</option>
                                        <option value="BRI">BRI</option>
                                        <option value="Mandiri">Mandiri</option>
                                        <option value="BTN">BTN</option>
                                        <option value="Permata">Permata</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-5">
                                    <label class="control-label">No. Rekening</label>
                                    <input type="number" class="form-control" name="nomor_rekening"
                                        value="{{ $user['nomor_rekening'] }}">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-7">
                                    <label class="control-label">Tentang Difabel</label>
                                    <textarea class="form-control" name="tentang_difabel"
                                        rows="6">{{ $user['tentang_difabel'] }}</textarea>
                                </div>
                                <div class="form-group col-md-5">
                                    <label class="control-label">Upload foto profil</label>
                                    <input type="file" class="form-control" name="foto_profil">
                                </div>
                            </div>
                            <br><br>
                            <div class="form-row">

                                <div class="col-sm-12">
                                    <div class="text-sm-right">
                                        <button type="submit" class="btn btn-info" style="width: 100%;">UPDATE
                                            PROFIL</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
@endsection
