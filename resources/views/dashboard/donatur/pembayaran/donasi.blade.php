@extends('layouts.dashboard')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="my-4">
            <h2>Halaman <strong><span class="text-info">History Donasi</span></strong></h2>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">History Donasi Anda</h4>
            </div>
            <div class="table-overflow container">
                <table class="table table-striped table-bordered datatable">
                    <thead>
                        <tr>
                            <td class="text-dark text-semibold">Kepada</td>
                            <td class="text-dark text-semibold">Tgl Donasi</td>
                            <td class="text-dark text-semibold">Jumlah</td>
                            <td class="text-dark text-semibold">Status</td>
                            <td class="text-dark text-semibold" width="25%">Aksi</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($donasi as $item)
                        <tr>
                            <td>
                                <div class="list-item">
                                    <div class="info">
                                        <span class="title p-t-10 text-semibold">{{$item->getDifabel->name}}</span>
                                    </div>
                                </div>
                            </td>
                            <td>{{$item->tanggal_bayar}}</td>
                            <td> Rp. {{$item->jumlah}}</td>
                            <td><span class="badge badge-pill badge-info">
                                    @if ($item->status == 0)
                                    Pending
                                    @else
                                    Diterima
                                    @endif
                                </span></td>
                            <td>
                                @if ($item->status == 0)
                                <a href="javascript:void(0)" data-toggle="modal"
                                data-target="#modalUploadBuktiPembayaranProduk{{$item->id}}">
                                <button class="btn-icon btn btn-warning"> Upload Bukti Pembayaran</button>
                            </a>
                            <div class="modal fade" id="modalUploadBuktiPembayaranProduk{{$item->id}}">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4>Upload Bukti Pembayaran Transaksi Produk</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="card-body">
                                                <form action="{{ route('donatur.donasi.update')}}" method="POST" enctype="multipart/form-data">
                                                    @csrf
                                                    <input type="hidden" name="id" value="{{$item->id}}">
                                                    <div class="form-row">
                                                        <div class="form-group col-md-12">
                                                            <label class="control-label">Bukti Tranfser</label><br>
                                                            <input type="file" name="foto" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="form-group col-md-12">
                                                            <button type="submit" class="btn btn-info btn-full"
                                                                style="width:100%">KIRIM BUKTI PEMBAYARAN</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                @else
                                <a href="{{ route('invoice.donasi', ['id'=>$item->id])}}" class="btn-icon btn btn-info">
                                    Cetak Invoice
                                </a>
                                @endif

                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
