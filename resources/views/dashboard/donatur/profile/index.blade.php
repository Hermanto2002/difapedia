@extends('layouts.dashboard')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="my-4">
            <h2>Selamat Datang, <strong><span class="text-info">Agus Saputra</span></strong>!</h2>
        </div>
    </div>

    <div class="col-md-12">
        <div class="card">
            <div class="card-header border bottom">
                <h4 class="card-title">Informasi Akun</h4>
            </div>
            <div class="card-body">
                <form action="{{ route('profile.update')}}" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label class="control-label">Email</label>
                                    <input type="email" class="form-control" name="email" value="{{ $user['email']}}">
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label">Password</label>
                                    <input type="password" name="password" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Nama Lengkap</label>
                                <input type="text" class="form-control" name="name" value="{{ $user['name']}}">
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label class="control-label">No. Telp</label>
                                    <input type="number" class="form-control" name="no_telp" value="{{ $user['no_telp']}}">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label class="control-label">Pekerjaan</label>
                                    <input type="text" class="form-control" name="pekerjaan" value="{{ $user['pekerjaan']}}">
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label">Jenis Kelamin</label>
                                    <select class="form-control" name="jenis_kelamin">
                                        <option disabled selected>{{ $user['jenis_kelamin'] }}</option>
                                        <option value="L">Laki - Laki</option>
                                        <option value="P">Perempuan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label class="control-label">Alamat</label>
                                    <textarea class="form-control" rows="6">{{ $user['alamat']}}</textarea>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">Foto Profil</label>
                                        <input type="file" class="form-control" name="foto_profil">
                                    </div>
                                </div>
                            </div>
                            <br><br>
                            <div class="form-row">
                                <div class="col-sm-12">
                                    <div class="text-sm-right">
                                        <a href="#">
                                            <button class="btn btn-info btn-full" style="width: 100%;">UPDATE
                                                PROFIL</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>

</div>
@endsection
