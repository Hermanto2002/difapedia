@extends('layouts.dashboard')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="my-4">
            <h2>Selamat Datang, <strong><span class="text-info"> {{ auth()->user()->name }}</span></strong>!</h2>
            <br>
            <a href="javascript:void(0)" data-toggle="modal" data-target="#modalAddLoker">
                <button class="btn btn-info">Tambah Loker</button>
            </a>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">List Lowongan Kerja</h4>
            </div>
            <div class="table-overflow container">
                <table class="table table-striped table-bordered datatable">
                    <thead>
                        <tr>
                            <td class="text-dark text-semibold">Nama Loker</td>
                            <td class="text-dark text-semibold">Deskripsi</td>
                            <td class="text-dark text-semibold">Perusahaan</td>
                            <td class="text-dark text-semibold">Deadline</td>
                            <td class="text-dark text-semibold">Jumlah Pelamar</td>
                            <td class="text-dark text-semibold">Status</td>
                            <td class="text-dark text-semibold">Aksi</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($loker as $item)
                        <tr>
                            <td>
                                <div class="list-item">
                                    <div class="info">
                                        <span class="title p-t-10 text-semibold">{{$item->nama_loker}}</span>
                                    </div>
                                </div>
                            </td>
                            <td width="40%">
                                {{ $item->deskripsi }}
                            </td>
                            <td>{{$item->getHrd->nama_perusahaan}}</td>
                            <td>{{$item->deadline}}</td>
                            <td>{{$item->countPelamar()}}</td>
                            <td><span class="badge badge-pill badge-primary">Aktif</span></td>
                            <td>
                                <a href="javascript:void(0)" data-toggle="modal"
                                    data-target="#modalCekLoker{{$item->id}}">
                                    <button class="btn btn-icon btn-gradient-success mt-3 btn-sm">
                                        <i class="mdi mdi-eye"></i>
                                    </button>
                                </a>
                                <div class="modal fade" id="modalCekLoker{{$item->id}}">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4>Detail Lowongan Kerja</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="card-body">
                                                    <form action="{{ route('hrd.loker.update')}}" method="POST">
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{$item->id}}">
                                                        <div class="row">
                                                            <div class="col-sm-8 offset-sm-2">
                                                                <div class="form-row">
                                                                    <div class="form-group col-md-12">
                                                                        <label class="control-label">Nama Loker</label>
                                                                        <input type="text" name="nama_loker"
                                                                            class="form-control"
                                                                            value="{{$item->nama_loker}}">
                                                                    </div>
                                                                    <div class="form-group col-md-12">
                                                                        <label class="control-label">Deskripsi</label>
                                                                        <textarea name="deskripsi" class="form-control"
                                                                            rows="3"
                                                                            value="">{{$item->deskripsi}}</textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="form-row">
                                                                    <div class="form-group col-md-6">
                                                                        <label class="control-label">Perusahaan</label>
                                                                        <input type="text" class="form-control"
                                                                            value="{{$item->getHrd->nama_perusahaan}}">
                                                                    </div>
                                                                    <div class="form-group col-md-6">
                                                                        <label class="control-label">No. Telp</label>
                                                                        <input type="number" class="form-control"
                                                                            value="{{$item->getHrd->no_telp}}">
                                                                    </div>
                                                                </div>
                                                                <div class="form-row">
                                                                    <div class="form-group col-md-6">
                                                                        <label class="control-label">Jumlah
                                                                            Pelamar</label>
                                                                        <input type="number" name="jumlah_pelamar"
                                                                            class="form-control"
                                                                            value="{{$item->jumlah_pelamar}}">
                                                                    </div>
                                                                    <div class="form-group col-md-6">
                                                                        <label class="control-label">Deadline</label>
                                                                        <input type="date" name="deadline" name="date"
                                                                            class="form-control"
                                                                            value="{{$item->deadline}}">
                                                                    </div>
                                                                    <div class="form-group col-md-6">
                                                                        <label class="control-label">Gambar</label>
                                                                        <input type="file" name="gambar"
                                                                            class="form-control">
                                                                    </div>
                                                                    <div class="form-group col-md-6">
                                                                        <label class="control-label">Status</label>
                                                                        <select name="status" class="form-control">
                                                                            <option value="1">1</option>
                                                                            <option value="0">0</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-row">
                                                                    <div class="col-sm-12">
                                                                        <div class="text-center">
                                                                            <button
                                                                                class="btn btn-danger btn-full">Hapus
                                                                                Data</button>
                                                                            <button
                                                                                class="btn btn-primary btn-full">Simpan
                                                                                Perubahan</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>



<!-- Modal Add Lowongan Kerja-->
<div class="modal fade" id="modalAddLoker">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Tambah Lowongan Kerja</h4>
            </div>
            <div class="modal-body">
                <div class="card-body">
                    <form action="{{ route('hrd.loker.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-sm-8 offset-sm-2">
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label class="control-label">Nama Loker</label>
                                        <input type="text" name="nama_loker" class="form-control" value="">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label class="control-label">Deskripsi</label>
                                        <textarea class="form-control" name="deskripsi" rows="3" value=""></textarea>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label class="control-label">Perusahaan</label>
                                        <input type="text" class="form-control" readonly
                                            value="{{ auth()->user()->nama_perusahaan }}">
                                    </div>

                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label class="control-label">No. Telp</label>
                                        <input type="number" class="form-control" readonly
                                            value="{{ auth()->user()->no_telp }}">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label">Jumlah Pelamar</label>
                                        <input type="number" class="form-control" name="jumlah_pelamar" value="">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label">Deadline</label>
                                        <input type="date" class="form-control" name="deadline" value="">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label">Gambar</label>
                                        <input type="file" name="gambar" class="form-control">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <img src="" width="80%">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label class="control-label" name="status">Status</label>
                                        <select class="form-control">
                                            <option value="1">Aktif</option>
                                            <option value="0">Ditutup</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col-sm-12">
                                        <div class="text-center">
                                            <button class="btn btn-danger btn-full">Hapus Data</button>
                                            <button class="btn btn-primary btn-full" type="submit">Simpan
                                                Perubahan</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
