@extends('layouts.dashboard')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="my-4">
            <h2>Selamat Datang, <strong><span class="text-info"> Kawi Dirgayusa</span></strong>!</h2>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header border bottom">
                <h4 class="card-title">Informasi Akun</h4>
            </div>
            <div class="card-body">
                <form action="{{route('profile.update')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label class="control-label">Email</label>
                                    <input type="email" class="form-control" name="email" value="{{ $user['email']}}">
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label">Password</label>
                                    <input type="password" class="form-control" name="password">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Nama Lengkap</label>
                                <input type="text" class="form-control" value="{{ $user['name']}}" name="name">
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label class="control-label">No. Telp</label>
                                    <input type="number" class="form-control" name="no_telp"
                                        value="{{ $user['no_telp']}}">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label class="control-label">Jenis Kelamin</label>
                                    <select class="form-control" name="jenis_kelamin">
                                        <option disabled selected>{{ $user['jenis_kelamin']}}</option>
                                        <option value="L">Laki - Laki</option>
                                        <option value="P">Perempuan</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label">Nama Perusahaan</label>
                                    <input type="email" class="form-control" name="nama_perusahaan"
                                        value="{{ $user['nama_perusahaan']}}">
                                </div>

                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label class="control-label">Jabatan</label>
                                    <input type="email" class="form-control" name="jabatan"
                                        value="{{ $user['jabatan']}}">
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label">Alamat</label>
                                    <textarea class="form-control" rows="6"
                                        name="alamat">{{ $user['alamat']}}</textarea>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">Foto Profil</label>
                                        <input type="file" class="form-control" name="foto_profil">
                                    </div>
                                </div>
                            </div>
                            <br><br>
                            <div class="form-row">
                                <div class="col-sm-12">
                                    <div class="text-sm-right">
                                        <a href="#">
                                            <button type="submit" class="btn btn-info btn-full"
                                                style="width: 100%;">UPDATE
                                                PROFIL</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>

</div>
@endsection
