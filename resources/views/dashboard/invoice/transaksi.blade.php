<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Invoice</title>

    <!-- Favicon -->
    <link rel="apple-touch-icon" href="{{ asset('dashboard/images/logo/apple-touch-icon.png')}}">
    <link rel="shortcut icon" href="{{ asset('dashboard/images/logo/favicon.png')}}">

    <!-- core dependcies css -->
    <link rel="stylesheet" href="{{ asset('dashboard/vendor/bootstrap/dist/css/bootstrap.css')}}" />
    <link rel="stylesheet" href="{{ asset('dashboard/vendor/PACE/themes/blue/pace-theme-minimal.css')}}" />
    <link rel="stylesheet" href="{{ asset('dashboard/vendor/perfect-scrollbar/css/perfect-scrollbar.min.css')}}" />

    <!-- page css -->

    <!-- core css -->
    <link href="{{ asset('dashboard/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{ asset('dashboard/css/themify-icons.css')}}" rel="stylesheet">
    <link href="{{ asset('dashboard/css/materialdesignicons.min.css')}}" rel="stylesheet">
    <link href="{{ asset('dashboard/css/animate.min.css')}}" rel="stylesheet">
    <link href="{{ asset('dashboard/css/app.css')}}" rel="stylesheet">
</head>

<body>
    <div class="app header-success-gradient">
        <div class="layout m-t-80" style="width:70%;margin: auto;">




            <!-- Content Wrapper START -->
            <div class="main-content">
                <div class="container-fluid">
                    <div class="container">
                        <div class="card">
                            <div class="p-v-5 p-h-10 border bottom print-invisible">
                                <ul class="list-unstyle list-inline text-right">
                                    <li class="list-inline-item">
                                        <a href="" class="btn text-gray text-hover display-block p-10 m-b-0"
                                            onclick="window.print();">
                                            <i class="ti-printer text-info p-r-5"></i>
                                            <b>Print</b>
                                        </a>
                                    </li>
                                    <li class="list-inline-item">
                                        <a href="" class="text-gray text-hover display-block p-10 m-b-0">
                                            <i class="fa fa-file-pdf-o text-danger p-r-5"></i>
                                            <b>Export PDF</b>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="p-h-30">
                                    <div class="m-t-15">
                                        <div class="inline-block">
                                            <img class="img-fluid" src="{{ asset('dashboard/images/logo/logo.png')}}"
                                                alt="">
                                            <address class="p-l-10 m-t-20">
                                                <b class="text-dark">PT. DIFAPEDIA</b><br>
                                                <span>Giri Hill Terrace, Permata Raya No. 3</span><br>
                                                <span>Indonesia</span><br>
                                                <abbr class="text-dark" title="Phone">Phone:</abbr>
                                                <span>(+62) 8123 6133 886</span>
                                            </address>
                                        </div>
                                        <div class="pull-right">
                                            <h2>BUKTI PEMBAYARAN</h2>
                                        </div>
                                    </div>
                                    <div class="row m-t-20">
                                        <div class="col-sm-9">
                                            <h3 class="p-l-10 m-t-10">Diberikan Kepada:</h3>
                                            <address class="p-l-10 m-t-10">
                                                <b class="text-dark">{{$transaksi->getUser['name']}}</b><br>
                                                <span>{{$transaksi->getUser['alamat']}} </span>
                                            </address>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="m-t-80">
                                                <div class="text-dark text-uppercase d-inline-block"><b>ID :</b></div>
                                                <div class="pull-right"> #{{$transaksi['id']}}</div>
                                            </div>
                                            <div class="text-dark text-uppercase d-inline-block"><b>Tanggal :</b></div>
                                            <div class="pull-right">{{$transaksi['tanggal_pembelian']}}</div>
                                        </div>
                                    </div>
                                    <div class="row m-t-20">
                                        <div class="col-sm-12">
                                            <table class="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>Jenis Pembayaran</th>
                                                        <th>Jumlah</th>
                                                        <th>Harga</th>
                                                        <th class="text-right">Total</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($detail as $i => $item)
                                                    <?php
                                                    $total=0;
                                                    $total += $item->jumlah*$item->getProduk->harga;
                                                    ?>
                                                    <tr>
                                                        <td>{{$i+1}}</td>
                                                        <td>{{$item->getProduk->nama_produk}}</td>
                                                        <td>{{$item->jumlah}}</td>
                                                        <td>Rp. {{$item->getProduk->harga}}</td>
                                                        <td class="text-right">Rp. {{$item->jumlah*$item->getProduk->harga}}</td>
                                                    </tr>
                                                    @endforeach


                                                </tbody>
                                            </table>
                                            <div class="row m-t-30">
                                                <div class="col-sm-12">
                                                    <div class="pull-right text-right">
                                                        <h3><b>Total :</b> Rp. {{$total}}</h3>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row m-t-30">
                                                <div class="col-sm-12">
                                                    <div class="border top bottom p-v-20">
                                                        <p class="text-opacity"><small>Biaya yang sudah dibayarkan tidak
                                                                dapat dikembalikan & semua pembayaran akan diserahkan
                                                                <br> kepada yang bersangkutan, dalam hal ini adalah para
                                                                <strong>difabel</strong></small></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row m-v-20">
                                                <div class="col-sm-6">
                                                    <img class="img-fluid text-opacity m-t--5" width="100"
                                                        src="{{ asset('dashboard/images/logo/logo.png')}}" alt="">
                                                </div>
                                                <div class="col-sm-6 text-right">
                                                    <small><b>Phone:</b> (+62) 8123 6133 886</small>
                                                    <br>
                                                    <small>PT. DIFAPEDIA</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Content Wrapper END -->



        </div>
        <!-- Page Container END -->

    </div>
    </div>

    <!-- build:js assets/js/vendor.js -->
    <!-- core dependcies js -->
    <script src="{{ asset('dashboard/vendor/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{ asset('dashboard/vendor/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{ asset('dashboard/vendor/bootstrap/dist/js/bootstrap.js')}}"></script>
    <script src="{{ asset('dashboard/vendor/PACE/pace.min.js')}}"></script>
    <script src="{{ asset('dashboard/vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.js')}}"></script>
    <script src="{{ asset('dashboard/vendor/d3/d3.min.js')}}"></script>
    <!-- endbuild -->

    <!-- build:js assets/js/app.min.js -->
    <!-- core js -->
    <script src="{{ asset('dashboard/js/app.js')}}"></script>
    <!-- configurator js -->
    <script src="{{ asset('dashboard/js/configurator.js')}}"></script>
    <!-- endbuild -->

    <!-- page js -->

</body>

</html>
