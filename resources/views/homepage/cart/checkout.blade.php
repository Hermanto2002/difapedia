@extends('layouts.shop')

@section('content')
<div role="main" class="main shop py-4">

    <div class="container">
        <div class="row">
            <div class="col-lg-12">

                <div class="accordion accordion-modern" id="accordion">
                    <div class="card card-default">
                        <div class="card-header">
                            <h4 class="card-title m-0">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
                                    href="#collapseThree">
                                    Metode Pembayaran
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="collapse show">
                            <div class="card-body">
                                <table class="shop_table cart">
                                    <thead>
                                        <tr>
                                            <th class="product-remove">
                                                &nbsp;
                                            </th>
                                            <th class="product-thumbnail">
                                                &nbsp;
                                            </th>
                                            <th class="product-name">
                                                Produk/Barang
                                            </th>
                                            <th class="product-price">
                                                Harga
                                            </th>
                                            <th class="product-quantity">
                                                Jumlah
                                            </th>
                                            <th class="product-subtotal">
                                                Total
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($cart as $item)
                                        <tr class="cart_table_item">
                                            <td class="product-remove">
                                                <a title="Remove this item" class="remove" href="{{ url('cart/delete', $item['id'])}}">
                                                    <i class="fas fa-times"></i>
                                                </a>
                                            </td>
                                            <td class="product-thumbnail">
                                                <a href="#">
                                                    <img width="100" height="100" alt="" class="img-fluid"
                                                        src="{{ asset('produk/'.$item['gambar'])}}">
                                                </a>
                                            </td>
                                            <td class="product-name">
                                                <a href="{{ url('/product/detail/'.$item['id'])}}">{{$item['nama_produk']}}</a>
                                            </td>
                                            <td class="product-price">
                                                <span class="amount">Rp. {{$item['harga']}}</span>
                                            </td>
                                            <td class="product-quantity">
                                                {{$item['qty']}}
                                            </td>
                                            <td class="product-subtotal">
                                                <span class="amount">Rp. {{$item['total']}}</span>
                                            </td>
                                        </tr>
                                        @endforeach


                                    </tbody>
                                </table>

                                <hr class="solid my-5">

                                <h4 class="text-primary">Total Belanja</h4>
                                <table class="cart-totals">
                                    <tbody>
                                        <tr class="cart-subtotal">
                                            <th>
                                                <strong class="text-dark">Total Harga Barang :</strong>
                                            </th>
                                            <td>
                                                <strong class="text-dark"><span class="amount">Rp.
                                                    {{$total}}</span></strong>
                                            </td>
                                        </tr>
                                        <tr class="shipping">
                                            <th>
                                                Biaya Pengiriman
                                            </th>
                                            <td>
                                                GRATIS !<input type="hidden" value="free_shipping" id="shipping_method"
                                                    name="shipping_method">
                                            </td>
                                        </tr>
                                        <tr class="total">
                                            <th>
                                                <strong class="text-dark">Total Belanja</strong>
                                            </th>
                                            <td>
                                                <strong class="text-dark"><span class="amount">Rp.
                                                    {{$total}}</span></strong>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                                <hr class="solid my-5">

                                <h4 class="text-primary">Metode Pembayaran</h4>


                                    <div class="form-row">
                                        <div class="form-group col">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input"
                                                    id="paymentdirectbank">
                                                <label class="custom-control-label" for="paymentdirectbank">Direct Bank
                                                    Transfer</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="paymentcheque">
                                                <label class="custom-control-label" for="paymentcheque">Cheque
                                                    Payment</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="paymentpaypal">
                                                <label class="custom-control-label" for="paymentpaypal">Paypal</label>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="actions-continue">
                    <form action="{{ route('cart.checkoutAdd')}}" method="POST">
                        @csrf
                        <input type="submit" value="Bayar Sekarang" name="proceed"
                            class="btn btn-primary btn-modern text-uppercase mt-5 mb-5 mb-lg-0">
                    </form>
                </div>

            </div>

        </div>

    </div>

</div>

<section class="section bg-primary border-0 m-0">
    <div class="container">
        <div class="row justify-content-center text-center text-lg-left py-4">
            <div class="col-lg-auto appear-animation" data-appear-animation="fadeInRightShorter">
                <div class="feature-box feature-box-style-2 d-block d-lg-flex mb-4 mb-lg-0">
                    <div class="feature-box-icon">
                        <i class="icon-location-pin icons text-color-light"></i>
                    </div>
                    <div class="feature-box-info pl-1">
                        <h5 class="font-weight-light text-color-light opacity-7 mb-0">ALAMAT</h5>
                        <p class="text-color-light font-weight-semibold mb-0">ITB STIKOM Bali</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-auto appear-animation" data-appear-animation="fadeInRightShorter"
                data-appear-animation-delay="200">
                <div class="feature-box feature-box-style-2 d-block d-lg-flex mb-4 mb-lg-0 px-xl-4 mx-lg-5">
                    <div class="feature-box-icon">
                        <i class="icon-people icons text-color-light"></i>
                    </div>
                    <div class="feature-box-info pl-1">
                        <h5 class="font-weight-light text-color-light opacity-7 mb-0">TEAM</h5>
                        <a href="tel:+8001234567"
                            class="text-color-light font-weight-semibold text-decoration-none"><strong>STIKOM
                                DEV</strong></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-auto appear-animation" data-appear-animation="fadeInRightShorter"
                data-appear-animation-delay="400">
                <div class="feature-box feature-box-style-2 d-block d-lg-flex">
                    <div class="feature-box-icon">
                        <i class="icon-share icons text-color-light"></i>
                    </div>
                    <div class="feature-box-info pl-1">
                        <h5 class="font-weight-light text-color-light opacity-7 mb-0">FOLLOW KAMI</h5>
                        <p class="mb-0">

                            <span class="social-icons-instagram pl-3"><a
                                    href="http://www.instagram.com/https://www.instagram.com/progress.stikombali/"
                                    target="_blank" class="text-color-light font-weight-semibold" title="Linkedin"><i
                                        class="mr-1 fab fa-instagram"></i> UKM
                                    PROGRESS</a></span>
                            <span class="social-icons-instagram pl-3"><a href="http://www.instagram.com/stikombali"
                                    target="_blank" class="text-color-light font-weight-semibold" title="Linkedin"><i
                                        class="mr-1 fab fa-instagram"></i> ITB STIKOM Bali</a></span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
