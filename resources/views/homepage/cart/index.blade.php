@extends('layouts.shop')

@section('content')
<div role="main" class="main shop py-4">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="featured-boxes">
                    <div class="row">
                        <div class="col">
                            <div class="featured-box featured-box-primary text-left mt-2">
                                <div class="box-content">
                                    <form method="post" action="">
                                        <table class="shop_table cart">
                                            <thead>
                                                <tr>
                                                    <th class="product-remove">
                                                        &nbsp;
                                                    </th>
                                                    <th class="product-thumbnail">
                                                        &nbsp;
                                                    </th>
                                                    <th class="product-name">
                                                        Produk/Barang
                                                    </th>
                                                    <th class="product-price">
                                                        Harga
                                                    </th>
                                                    <th class="product-quantity">
                                                        Jumlah
                                                    </th>
                                                    <th class="product-subtotal">
                                                        Total
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($cart as $item)
                                                <tr class="cart_table_item">
                                                    <td class="product-remove">
                                                        <a title="Remove this item" class="remove" href="{{ url('cart/delete', $item['id'])}}">
                                                            <i class="fas fa-times"></i>
                                                        </a>
                                                    </td>
                                                    <td class="product-thumbnail">
                                                        <a href="#">
                                                            <img width="100" height="100" alt="" class="img-fluid"
                                                                src="{{ asset('produk/'.$item['gambar'])}}">
                                                        </a>
                                                    </td>
                                                    <td class="product-name">
                                                        <a href="{{ url('/product/detail/'.$item['id'])}}">{{$item['nama_produk']}}</a>
                                                    </td>
                                                    <td class="product-price">
                                                        <span class="amount">Rp. {{$item['harga']}}</span>
                                                    </td>
                                                    <td class="product-quantity">
                                                        {{$item['qty']}}
                                                    </td>
                                                    <td class="product-subtotal">
                                                        <span class="amount">Rp. {{$item['total']}}</span>
                                                    </td>
                                                </tr>
                                                @endforeach

                                                <tr>
                                                    <td class="actions" colspan="6">
                                                        <div class="actions-continue">
                                                            <input type="submit" value="Bayar Sekarang"
                                                                name="update_cart"
                                                                class="btn btn-xl btn-light pr-4 pl-4 text-2 font-weight-semibold text-uppercase">
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="featured-boxes">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="featured-box featured-box-primary text-left mt-3 mt-lg-4">
                                <div class="box-content">
                                    <h4 class="color-primary font-weight-semibold text-4 text-uppercase mb-3">Opsi
                                        Pengiriman</h4>
                                    <form action="/" id="frmCalculateShipping" method="post">
                                        <div class="form-row">
                                            <div class="form-group col">
                                                <label class="font-weight-bold text-dark">Kurir</label>
                                                <select class="form-control">
                                                    <option value="">Pilih Kurir</option>
                                                    <option value="">JNE</option>
                                                    <option value="">JNT</option>
                                                    <option value="">SICEPAT</option>
                                                    <option value="">GO-SEND</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-lg-6">
                                                <label class="font-weight-bold text-dark">Provinsi</label>
                                                <select class="form-control">
                                                    <option value="">Pilih Provinsi</option>
                                                    <option value="">Bali</option>
                                                    <option value="">Jawa Timur</option>
                                                    <option value="">Jawa Tengah</option>
                                                    <option value="">Jawa Barat</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label class="font-weight-bold text-dark">Kabupaten / Kota</label>
                                                <select class="form-control">
                                                    <option value="">Pilih Kota</option>
                                                    <option value="">Denpasar</option>
                                                    <option value="">Badung</option>
                                                    <option value="">Tabanan</option>
                                                    <option value="">Gianyar</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-lg-12">
                                                <label class="font-weight-bold text-dark">Alamat Lengkap</label>
                                                <textarea class="form-control"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col">
                                                <input type="submit" value="Update Total"
                                                    class="btn btn-xl btn-light pr-4 pl-4 text-2 font-weight-semibold text-uppercase"
                                                    data-loading-text="Loading...">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="featured-box featured-box-primary text-left mt-3 mt-lg-4">
                                <div class="box-content">
                                    <h4 class="color-primary font-weight-semibold text-4 text-uppercase mb-3">Total
                                        Belanja</h4>
                                    <table class="cart-totals">
                                        <tbody>
                                            <tr class="cart-subtotal">
                                                <th>
                                                    <strong class="text-dark">Total Harga Barang :</strong>
                                                </th>
                                                <td>
                                                    <strong class="text-dark"><span class="amount">Rp.
                                                            {{$total}}</span></strong>
                                                </td>
                                            </tr>
                                            <tr class="shipping">
                                                <th>
                                                    Biaya Pengiriman
                                                </th>
                                                <td>
                                                    GRATIS !<input type="hidden" value="free_shipping"
                                                        id="shipping_method" name="shipping_method">
                                                </td>
                                            </tr>
                                            <tr class="total">
                                                <th>
                                                    <strong class="text-dark">Total Belanja</strong>
                                                </th>
                                                <td>
                                                    <strong class="text-dark"><span class="amount">Rp.
                                                        {{$total}}</span></strong>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="actions-continue">
                            <a href="{{ route('cart.checkout')}}">
                                <button type="submit" class="btn btn-primary btn-modern text-uppercase">Lanjut ke
                                    Pembayaran <i class="fas fa-angle-right ml-1"></i></button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
