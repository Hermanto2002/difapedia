<!DOCTYPE html>
<html>

<head>
    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Checkout | Difa - Difabel Indonesia</title>
    <meta name="keywords" content="" />
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon -->
    <link rel="shortcut icon" href="img/favicon.png" type="image/x-icon" />
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">
    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800%7COpen+Sans:400,700,800"
        rel="stylesheet" type="text/css">
    <!-- Vendor CSS -->
    <link rel="stylesheet" href="{{ asset('homepage/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/vendor/fontawesome-free/css/all.min.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/vendor/animate/animate.min.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/vendor/simple-line-icons/css/simple-line-icons.min.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/vendor/owl.carousel/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/vendor/owl.carousel/assets/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/vendor/magnific-popup/magnific-popup.min.css')}}">
    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{ asset('homepage/css/theme.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/css/theme-elements.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/css/theme-blog.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/css/theme-shop.css')}}">
    <!-- Demo CSS -->
    <link rel="stylesheet" href="{{ asset('homepage/css/demos/demo-landing.css')}}">
    <!-- Skin CSS -->
    <link rel="stylesheet" href="{{ asset('homepage/css/skins/skin-landing.css')}}">
    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="{{ asset('homepage/css/custom.css')}}">
    <!-- Head Libs -->
    <script src="{{ asset('homepage/vendor/modernizr/modernizr.min.js')}}"></script>
</head>

<body class="alternative-font-4 loading-overlay-showing" data-plugin-page-transition data-loading-overlay
    data-plugin-options="{'hideDelay': 500}">
    <div class="loading-overlay">
        <div class="bounce-loader">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
    <div class="body">
        <div role="main" class="main">
            <div class="col-lg-8 offset-lg-2 text-center">
                <ul
                    class="list list-unstyled d-flex justify-content-center text-primary font-weight-semibold positive-ls-2 flex-column flex-md-row text-4 mb-4 mt-5">
                    <li class="px-3 bg-color-primary text-light "><strong>PEMBAYARAN</strong></li>
                </ul>
                <p class="d-flex align-items-center justify-content-center font-weight-bold text-color-dark text-8 negative-ls-1 pb-2 mb-5 appear-animation"
                    data-appear-animation="blurIn" data-appear-animation-delay="600">Selesaikan Pembayaran Anda</p>
                <div class="alert alert-primary">
                    Jumlah: <strong>Rp. {{$total}}</strong> bisa lakukan pembayaran ke <a href="user.html"
                        class="alert-link">BCA 001245625 a/n PT. DIFAPEDIA </a>.
                </div>
                <div class="text-center mt-5">
                    <a href="{{route('donatur.index')}}" class="btn btn-modern btn-primary">Kembali ke Dashboard</a>
                </div>
            </div>
        </div>
    </div>
    <!-- Vendor -->
    <script src="{{ asset('homepage/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/jquery.appear/jquery.appear.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/jquery.easing/jquery.easing.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/jquery.cookie/jquery.cookie.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/popper/umd/popper.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/common/common.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/jquery.validation/jquery.validate.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/jquery.gmap/jquery.gmap.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/jquery.lazyload/jquery.lazyload.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/isotope/jquery.isotope.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/owl.carousel/owl.carousel.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/vide/jquery.vide.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/vivus/vivus.min.js')}}"></script>
    <!-- Theme Base, Components and Settings -->
    <script src="{{ asset('homepage/js/theme.js')}}"></script>
    <!-- Current Page Vendor and Views -->
    <script src="{{ asset('homepage/vendor/rs-plugin/js/jquery.themepunch.tools.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>
    <!-- Theme Custom -->
    <script src="{{ asset('homepage/js/custom.js')}}"></script>
    <!-- Theme Initialization Files -->
    <script src="{{ asset('homepage/js/theme.init.js')}}"></script>
    <!-- Examples -->
    <!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
         <script>
         	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
         	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
         	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
         	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

         	ga('create', 'UA-12345678-1', 'auto');
         	ga('send', 'pageview');
         </script>
          -->
</body>

</html>
