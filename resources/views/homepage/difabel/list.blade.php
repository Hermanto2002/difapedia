@extends('layouts.difa')

@section('content')
<div role="main" class="main shop py-4">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <aside class="sidebar">
                    <form action="page-search-results.html" method="get">
                        <div class="input-group mb-3 pb-1">
                            <input class="form-control text-1" placeholder="Search..." name="s" id="s" type="text">
                            <span class="input-group-append">
                                <button type="submit" class="btn btn-dark text-1 p-2"><i
                                        class="fas fa-search m-2"></i></button>
                            </span>
                        </div>
                    </form>
                    <h5 class="font-weight-bold pt-3">Provinsi</h5>
                    <ul class="nav nav-list flex-column">
                        <li class="nav-item"><a class="nav-link" href="#">Aceh</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Bali</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Jawa Timur</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Jawa Barat</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Jawa Tengah</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Jakarta Timur</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Jakarta Barat</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Jakarta Selatan</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Jakarta Utara</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Jakarta Pusat</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Bandung</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">DIY Yogyakarta</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Sulawesi Tengah</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">NTB</a></li>
                    </ul>
                    <h5 class="font-weight-bold pt-3">Kategori Umur</h5>
                    <ul class="nav nav-list flex-column">
                        <li class="nav-item"><a class="nav-link" href="#">1 - 10 Tahun</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">11 - 20 Tahun</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">21 - 30 Tahun</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">31 - 40 Tahun</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">41 - 50 Tahun</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">51 - Lanjut Usia</a></li>
                    </ul>
                    <div class="row mb-5">
                </aside>
            </div>
            <div class="col-lg-9">
                <div class="masonry-loader masonry-loader-showing">
                    <div class="row products product-thumb-info-list" data-plugin-masonry
                        data-plugin-options="{'layoutMode': 'fitRows'}">
                        @foreach ($difabel as $item)
                        <div class="col-sm-5 product mb-0">
                            <span class="product-thumb-info border-0">
                                <a href="{{ url('/difa/profil/'.$item->id)}}" class="add-to-cart-product bg-color-primary">
                                    <span class="text-uppercase text-1">Lihat Profil</span>
                                </a>
                                <a href="{{ url('/difa/profil/'.$item->id)}}">
                                    <span class="product-thumb-info-image">
                                        <img alt="" class="img-fluid" src="{{ asset('difabel/'.$item->foto_profil)}}">
                                    </span>
                                </a>
                            </span>
                        </div>
                        <div class="col-sm-7">
                            <div class="summary entry-summary">
                                <a href="{{ url('/difa/profil/'.$item->id)}}">
                                    <h1 class="mb-0 font-weight-bold text-7">{{$item->name}}</h1>
                                </a>
                                <p>
                                    {{-- <span class="text-dark font-weight-semibold">{{hitung_umur($item->tanggal_lahir)}}</span> --}}
                                </p>
                                <p class="mb-4">
                                    {{$item->tentang_difabel}}
                                </p>
                                <div class="product-meta">
                                    <span class="posted-in"> <strong>Total Donasi : </strong><a rel="tag" href="#">Rp.
                                            {{$item->getDonasi->sum('jumlah')}}</a></span>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <hr class="my-5">
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
