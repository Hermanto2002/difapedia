<!DOCTYPE html>
<html>

<head>

    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Difabel Dashboard | Difa - Difabel Indonesia</title>

    <meta name="keywords" content="" />
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('homepage/img/favicon.png')}}" type="image/x-icon" />
    <link rel="apple-touch-icon" href="{{ asset('homepage/img/apple-touch-icon.png')}}">


    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light"
        rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="{{ asset('homepage/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/vendor/fontawesome-free/css/all.min.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/vendor/animate/animate.min.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/vendor/simple-line-icons/css/simple-line-icons.min.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/vendor/owl.carousel/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/vendor/owl.carousel/assets/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/vendor/magnific-popup/magnific-popup.min.css')}}">

    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{ asset('homepage/css/theme.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/css/theme-elements.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/css/theme-blog.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/css/theme-shop.css')}}">

    <!-- Current Page CSS -->
    <link rel="stylesheet" href="{{ asset('homepage/vendor/rs-plugin/css/settings.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/vendor/rs-plugin/css/layers.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/vendor/rs-plugin/css/navigation.css')}}">

    <!-- Demo CSS -->
    <link rel="stylesheet" href="{{ asset('homepage/css/demos/demo-resume.css')}}">

    <!-- Skin CSS -->
    <link rel="stylesheet" href="{{ asset('homepage/css/skins/skin-user.css')}}">

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="{{ asset('homepage/css/custom.css')}}">

    <!-- Head Libs -->
    <script src="{{ asset('homepage/vendor/modernizr/modernizr.min.js')}}"></script>

</head>

<body data-spy="scroll" data-target=".wrapper-spy">

    <div class="body">
        <header id="header" class="header-floating-icons"
            data-plugin-options="{'stickyEnabled': false, 'stickyEnableOnBoxed': false, 'stickyEnableOnMobile': false, 'stickyStartAt': 0, 'stickySetTop': '60px', 'stickyChangeLogo': false}">
            <div class="header-body">
                <div class="header-container container">
                    <div class="header-row">
                        <div class="header-column">
                            <div class="header-row">
                                <div class="header-nav pt-1">
                                    <div class="header-nav-main">
                                        <nav class="wrapper-spy collapse">
                                            <ul class="nav" id="mainNav">
                                                <li>
                                                    <a data-hash href="#about-me"
                                                        class="nav-link text-color-dark bg-color-primary">
                                                        <i class="icon-home icons"></i>
                                                        <span class="custom-tooltip">Tentang Saya</span>
                                                    </a>
                                                </li>

                                                <li>
                                                    <a data-hash href="#recommendations"
                                                        class="nav-link text-color-dark bg-color-primary">
                                                        <i class="icon-present icons"></i>
                                                        <span class="custom-tooltip">Produk</span>
                                                    </a>
                                                </li>

                                                <li>
                                                    <a data-hash href="#say-hello"
                                                        class="nav-link text-color-dark bg-color-primary">
                                                        <i class="icon-envelope-open icons"></i>
                                                        <span class="custom-tooltip">Kontak Saya</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                    <button class="btn header-btn-collapse-nav" data-toggle="collapse"
                                        data-target=".header-nav-main nav">
                                        <i class="fas fa-bars"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <div role="main" class="main">

            <section id="about-me"
                class="section section-no-border section-parallax custom-section-padding-1 custom-position-1 custom-xs-bg-size-cover parallax-no-overflow m-0"
                data-plugin-parallax data-plugin-options="{'speed': 1.5}" data-image-src="{{ asset('homepage/img/bg/02.jpg')}}">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 custom-sm-margin-bottom-4">
                            <img src="{{ asset('homepage/img/users/06.jpg')}}"
                                class="img-fluid custom-border custom-image-position-2 custom-box-shadow-4" alt />
                        </div>
                        <div class="col-lg-8 col-xl-8">
                            <h1 class="text-color-light custom-font-size-1 mt-2">{{$difabel['name']}}</h1>
                            <a href="#">
                                <button type="button" class="mb-1 mt-1 mr-1 btn btn-primary btn-rounded"
                                    data-toggle="modal" data-target="#modalDonasi"
                                    class="btn btn-modern btn-primary">Donasi Sekarang <span><i
                                            class="fas fa-donate"></i></span></button>
                            </a>
                        </div>

                    </div>
                </div>
                <ul class="social-icons custom-social-icons">
                    <li class="social-icons-facebook">
                        <a href="http://www.facebook.com/" target="_blank" title="Facebook">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                    </li>
                    <li class="social-icons-twitter">
                        <a href="http://www.twitter.com/" target="_blank" title="Twitter">
                            <i class="fab fa-twitter"></i>
                        </a>
                    </li>
                    <li class="social-icons-facebook bg-color-primary">
                        <a href="http://www.instagram.com/" target="_blank" title="instagram">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </li>
                </ul>
            </section>

            <div class="custom-about-me-links bg-color-light">
                <div class="container">
                    <div class="row justify-content-end">
                        <div class="col-lg-4 text-center custom-xs-border-bottom p-0">
                            <a data-hash href="#say-hello" class="text-decoration-none">
                                <span class="custom-nav-button text-color-dark">
                                    <i class="icon-earphones-alt icons text-color-primary"></i>
                                    Kontak Saya
                                </span>
                            </a>
                        </div>
                        <div class="col-lg-5 text-center custom-xs-border-bottom p-0">
                            <a data-hash href="#say-hello" class="text-decoration-none">
                                <span class="custom-nav-button custom-divisors text-color-dark">
                                    <i class="icon-envelope-open icons text-color-primary"></i>
                                    Kirim Pesan
                                </span>
                            </a>
                        </div>

                    </div>
                </div>
            </div>

            <section class="section section-no-border bg-color-light m-0">
                <div class="container">
                    <div class="row">
                        <div class="col">

                            <div
                                class="custom-box-details bg-color-light custom-box-shadow-1 col-lg-6 ml-5 mb-5 mb-lg-4 float-right clearfix">
                                <h4>Data Diri Singkat</h4>
                                <div class="row">
                                    <div class="col-md-6">
                                        <ul class="custom-list-style-1 p-0 mb-0">
                                            <li>
                                                <span class="text-color-dark">Kat. Disabilitas :</span>
                                                <span
                                                    class="custom-text-color-2">{{$difabel['kategori_disabilitas']}}</span>
                                            </li>
                                            {{-- <li>
                                                <span class="text-color-dark">Umur :</span>
                                                <span
                                                    class="custom-text-color-2">{{hitung_umur($difabel['tanggal_lahir'])}}
                                                    Tahun </span>
                                            </li> --}}
                                            <li>
                                                <span class="text-color-dark">T/L :</span>
                                                <span class="custom-text-color-2">{{$difabel['tanggal_lahir']}}</span>
                                            </li>
                                            <li>
                                                <span class="text-color-dark">Total Donasi:</span>
                                                <span class="text-light bg-color-primary p-1"><strong>Rp.
                                                        {{$difabel->getDonasi->sum('jumlah')}}</strong></span>
                                            </li>

                                        </ul>
                                    </div>
                                    <div class="col-md-6">
                                        <ul class="custom-list-style-1 p-0 mb-0">
                                            <li>
                                                <span class="text-color-dark">Kota Asal :</span>
                                                <span class="custom-text-color-2">{{$difabel['kota_asal']}}</span>
                                            </li>
                                            <li>
                                                <span class="text-color-dark">No. Telp :</span>
                                                <span class="custom-text-color-2"><a class="custom-text-color-2"
                                                        href="tel:{{$difabel['no_telp']}}">{{$difabel['no_telp']}}</a></span>
                                            </li>
                                            <li>
                                                <span class="text-color-dark">EMAIL:</span>
                                                <span class="custom-text-color-2"><a class="custom-text-color-2"
                                                        href="mailto:{{$difabel['email']}}">{{$difabel['email']}}</a></span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <h2 class="text-color-quaternary font-weight-extra-bold text-uppercase mt-1">Tentang Saya
                            </h2>

                            <p class="custom-text-color-2">
                                {{$difabel['tentang_difabel']}}
                            </p>

                        </div>
                    </div>
                </div>
            </section>

            <section id="recommendations" class="section section-no-border bg-color-primary m-0">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <h2 class="text-color-quaternary font-weight-extra-bold text-uppercase">Produk Saya</h2>
                        </div>
                    </div>
                    <div class="row">
                        @foreach ($produk as $item)
                        <div class="col-lg-4 mb-5 mb-lg-0">
                            <article class="thumb-info custom-thumb-info-2 appear-animation text-center"
                                data-appear-animation="fadeIn" data-appear-animation-delay="0"
                                data-appear-animation-duration="1s">
                                <div class="thumb-info-wrapper">
                                    <a href="#">
                                        <img src="{{ asset('produk/'.$item->gambar_produk)}}" alt class="img-fluid" />
                                    </a>
                                </div>
                                <div class="thumb-info-caption">
                                    <div class="thumb-info-caption-text">
                                        <h4>
                                            <a href="#"
                                                class="text-decoration-none text-color-dark font-weight-semibold">
                                                {{$item->nama_produk}}
                                            </a>
                                        </h4>

                                        <p class="custom-text-color-2">
                                            {{$item->deskripsi_produk}}
                                        </p>
                                    </div>
                                    <hr class="solid m-0 mt-4 mb-2">
                                    <div class="row justify-content-between">
                                        <div
                                            class="col-auto text-uppercase font-weight-semibold text-color-dark text-2">
                                            <span class="text-warning">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star-half"></i>
                                            </span>45 Reviews | Rp. {{$item->harga}}
                                            <a href="#">
                                                <button type="button"
                                                    class="mb-1 mt-1 mr-1 btn btn-primary btn-rounded text-3"
                                                    href="{{ url('/product/detail/'.$item->id)}}">Beli Sekarang <span><i
                                                            class="fas fa-cart-plus"></i></span></button>
                                            </a>
                                        </div>

                                    </div>
                                </div>
                            </article>
                        </div>
                        @endforeach

                    </div>

                </div>
            </section>


            <div id="say-hello" class="container-fluid">
                <div class="row">
                    <div class="col-lg-6 p-0">
                        <section class="section section-no-border bg-color-default h-100 m-0">
                            <div class="row justify-content-end m-0">
                                <div class="col-half-section col-half-section-right mr-3">

                                    <h2 class="text-color-quaternary text-uppercase font-weight-extra-bold">Say Hello
                                    </h2>
                                    <form id="contactForm" class="contact-form custom-form-style"
                                        action="php/contact-form.php" method="post">
                                        <div class="contact-form-success alert alert-success d-none mt-4"
                                            id="contactSuccess">
                                            <strong>Success!</strong> Your message has been sent to us.
                                        </div>
                                        <div class="contact-form-error alert alert-danger d-none mt-4"
                                            id="contactError">
                                            <strong>Error!</strong> There was an error sending your message.
                                            <span class="mail-error-message text-1 d-block"
                                                id="mailErrorMessage"></span>
                                        </div>

                                        <div class="form-content">
                                            <div class="form-control-custom">
                                                <input type="text" class="form-control" name="name"
                                                    placeholder="Your Name *"
                                                    data-msg-required="This field is required." id="name" required="" />
                                            </div>
                                            <div class="form-control-custom">
                                                <input type="text" class="form-control" name="subject"
                                                    placeholder="Subject *" data-msg-required="This field is required."
                                                    id="subject" required="" />
                                            </div>
                                            <div class="form-control-custom">
                                                <textarea maxlength="5000"
                                                    data-msg-required="Please enter your message." rows="10"
                                                    class="form-control" name="message" placeholder="Message*"
                                                    id="message" required="" aria-required="true"></textarea>
                                            </div>
                                            <input type="submit"
                                                class="btn btn-quaternary text-color-light text-uppercase font-weight-semibold outline-none custom-btn-style-2 custom-border-radius-1"
                                                value="Submit" />
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="col-lg-6 p-0">
                        <section class="section section-no-border h-100 m-0"
                            style="background: url({{ asset('homepage/img/portfolio/02.jpg')}}); background-size: cover;">
                            <div class="row m-0">
                                <div class="col-half-section col-half-section-left ml-3">
                                    <a href="mailto:{{$difabel['email']}}" class="text-decoration-none">
                                        <span class="feature-box custom-feature-box align-items-center mb-4">
                                            <span class="custom-feature-box-icon">
                                                <i class="icon-envelope icons text-color-light"></i>
                                            </span>
                                            <span class="feature-box-info">
                                                <span
                                                    class="custom-label font-weight-semibold text-uppercase custom-text-color-1">Email</span>
                                                <strong
                                                    class="font-weight-light text-color-light custom-opacity-effect-1">{{$difabel['email']}}</strong>
                                            </span>
                                        </span>
                                    </a>
                                    <a href="tel:{{$difabel['no_telp']}}" class="text-decoration-none">
                                        <span class="feature-box custom-feature-box align-items-center mb-4">
                                            <span class="custom-feature-box-icon">
                                                <i class="icon-phone icons text-color-light"></i>
                                            </span>
                                            <span class="feature-box-info">
                                                <span
                                                    class="custom-label font-weight-semibold text-uppercase custom-text-color-1">Phone</span>
                                                <strong
                                                    class="font-weight-light text-color-light custom-opacity-effect-1">{{$difabel['no_telp']}}</strong>
                                            </span>
                                        </span>
                                    </a>

                                    <span class="feature-box custom-feature-box align-items-center">
                                        <span class="custom-feature-box-icon">
                                            <i class="icon-share icons text-color-light"></i>
                                        </span>
                                        <a href="http://www.facebook.com" class="d-flex text-decoration-none">
                                            <span class="feature-box-info">
                                                <span
                                                    class="custom-label font-weight-semibold text-uppercase custom-text-color-1">Follow
                                                    me</span>
                                                <strong
                                                    class="font-weight-light text-color-light custom-opacity-effect-1">Facebook</strong>
                                            </span>
                                        </a>
                                        <a href="http://www.twitter.com" class="d-flex text-decoration-none">
                                            <span class="feature-box-info custom-both-side-border mt-2">
                                                <strong
                                                    class="font-weight-light text-color-light custom-opacity-effect-1">Twitter</strong>
                                            </span>
                                        </a>
                                        <a href="http://www.instagram.com" class="d-flex text-decoration-none">
                                            <span class="feature-box-info p-0 mt-2">
                                                <strong
                                                    class="font-weight-light text-color-light custom-opacity-effect-1">Instagram</strong>
                                            </span>
                                        </a>
                                    </span>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>

            <footer id="footer" class="m-0 p-0">
                <div class="footer-copyright bg-color-light m-0">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 m-0">
                                <p class="text-center"><strong>DIFAPEDIA - DIFABEL INDONESIA</strong> | ©Copyright 2021.
                                    All Rights Reserved.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
        <div class="modal fade" id="modalDonasi" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <form id="demo-form" action="{{ route('difa.donasi')}}" method="POST" class="mb-4" novalidate="novalidate">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="largeModalLabel">Form Donasi</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                            @csrf
                            <input type="hidden" name="id_difabel" value="{{$difabel['id']}}">
                            <div class="form-group row align-items-center">
                                <label class="col-sm-3 text-left text-sm-right mb-0">Jumlah</label>
                                <div class="col-sm-9">
                                    <input type="number" name="jumlah" class="form-control"
                                        placeholder="minimal Rp. 10.000" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 text-left text-sm-right mb-0">Pesan Semangat</label>
                                <div class="col-sm-9">
                                    <textarea rows="5" name="pesan_semangat" class="form-control"
                                        placeholder="Berikan pesan semangat untuk dia" required></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary px-5">Checkout</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- Vendor -->
        <script src="{{ asset('homepage/vendor/jquery/jquery.min.js')}}"></script>
        <script src="{{ asset('homepage/vendor/jquery.appear/jquery.appear.min.js')}}"></script>
        <script src="{{ asset('homepage/vendor/jquery.easing/jquery.easing.min.js')}}"></script>
        <script src="{{ asset('homepage/vendor/jquery.cookie/jquery.cookie.min.js')}}"></script>
        <script src="{{ asset('homepage/vendor/popper/umd/popper.min.js')}}"></script>
        <script src="{{ asset('homepage/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
        <script src="{{ asset('homepage/vendor/common/common.min.js')}}"></script>
        <script src="{{ asset('homepage/vendor/jquery.validation/jquery.validate.min.js')}}"></script>
        <script src="{{ asset('homepage/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js')}}"></script>
        <script src="{{ asset('homepage/vendor/jquery.gmap/jquery.gmap.min.js')}}"></script>
        <script src="{{ asset('homepage/vendor/jquery.lazyload/jquery.lazyload.min.js')}}"></script>
        <script src="{{ asset('homepage/vendor/isotope/jquery.isotope.min.js')}}"></script>
        <script src="{{ asset('homepage/vendor/owl.carousel/owl.carousel.min.js')}}"></script>
        <script src="{{ asset('homepage/vendor/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{ asset('homepage/vendor/vide/jquery.vide.min.js')}}"></script>
        <script src="{{ asset('homepage/vendor/vivus/vivus.min.js')}}"></script>

        <!-- Theme Base, Components and Settings -->
        <script src="{{ asset('homepage/js/theme.js')}}"></script>

        <!-- Current Page Vendor and Views -->
        <script src="{{ asset('homepage/vendor/rs-plugin/js/jquery.themepunch.tools.min.js')}}"></script>
        <script src="{{ asset('homepage/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>

        <!-- Current Page Vendor and Views -->
        <script src="{{ asset('homepage/js/views/view.contact.js')}}"></script>

        <!-- Demo -->
        <script src="{{ asset('homepage/js/demos/demo-resume.js')}}"></script>

        <!-- Theme Custom -->
        <script src="{{ asset('homepage/js/custom.js')}}"></script>

        <!-- Theme Initialization Files -->
        <script src="{{ asset('homepage/js/theme.init.js')}}"></script>


        <!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-12345678-1', 'auto');
			ga('send', 'pageview');
		</script>
		 -->


</body>

</html>
