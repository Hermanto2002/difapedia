@extends('layouts.homepage')

@section('content')
<div role="main" class="main" id="home">
    <div class="slider-container rev_slider_wrapper" style="height: 100vh;">
        <div id="revolutionSlider" class="slider rev_slider" data-version="5.4.8" data-plugin-revolution-slider
            data-plugin-options="{'sliderLayout': 'fullscreen', 'delay': 9000, 'gridwidth': 1140, 'gridheight': 800, 'responsiveLevels': [4096,1200,992,500]}">
            <ul>

                <li class="slide-overlay" data-transition="fade">
                    <img src="{{ asset('homepage/img/slides/slide-one-page-1-1.jpg') }}" alt=""
                        data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat"
                        class="rev-slidebg">

                    <div class="tp-caption text-color-light font-weight-normal" data-x="center" data-y="center"
                        data-voffset="['-80','-80','-80','-105']" data-start="700" data-fontsize="['16','16','16','40']"
                        data-lineheight="['25','25','25','45']" data-transform_in="y:[-50%];opacity:0;s:500;">DIFAPEDIA
                        - DIFABEL INDONESIA</div>

                    <h1 class="tp-caption font-weight-extra-bold text-color-light negative-ls-1"
                        data-frames='[{"delay":1000,"speed":2000,"frame":"0","from":"sX:1.5;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
                        data-x="center" data-y="center" data-voffset="['-30','-30','-30','-30']"
                        data-fontsize="['50','50','50','90']" data-lineheight="['55','55','55','95']">" SATU HARAPAN
                        UNTUK SEMUA "</h1>

                    <div class="tp-caption font-weight-light ws-normal text-center"
                        data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":2000,"split":"chars","splitdelay":0.03,"ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]'
                        data-x="center" data-y="center" data-voffset="['53','53','53','105']"
                        data-width="['530','530','530','1100']" data-fontsize="['18','18','18','40']"
                        data-lineheight="['26','26','26','45']" style="color: #b5b5b5;"><strong
                            class="text-color-light">8,56%</strong> atau sekitar <strong class="text-color-light">21,84
                            juta</strong> penduduk Indonesia merupakan penyandang disabilitas</div>

                    <a class="tp-caption btn btn-primary btn-rounded font-weight-semibold"
                        data-frames='[{"delay":2500,"speed":2000,"frame":"0","from":"opacity:0;y:50%;","to":"o:1;y:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]'
                        data-hash data-hash-offset="85" href="#projects" data-x="center" data-hoffset="0"
                        data-y="center" data-voffset="['133','133','133','255']" data-whitespace="nowrap"
                        data-fontsize="['14','14','14','33']" data-paddingtop="['15','15','15','40']"
                        data-paddingright="['45','45','45','110']" data-paddingbottom="['15','15','15','40']"
                        data-paddingleft="['45','45','45','110']">Bantu Mereka!</a>

                </li>

            </ul>
        </div>
    </div>
    <div class="container">
        <div class="row py-5 my-5">
            <div class="col-md-6 order-2 order-md-1 text-center text-md-left appear-animation mt-2 pt-1"
                data-appear-animation="fadeInRightShorter">
                <div class="owl-carousel owl-theme nav-style-1 mb-0"
                    data-plugin-options="{'responsive': {'576': {'items': 1}, '768': {'items': 1}, '992': {'items': 2}, '1200': {'items': 2}}, 'margin': 25, 'loop': true, 'nav': false, 'dots': false, 'autoplay': true, 'autoplayTimeout': 3000}">
                    @foreach ($difabel as $item)
                    <div>
                        <a href="{{ route('difa.profil',['id'=>$item->id]) }}">
                            <img class="img-fluid rounded-0 mb-4" src="{{ asset('difabel/'.$item->foto_profil) }}"
                                alt="" />
                            <h3 class="font-weight-bold text-color-dark text-4 mb-0">{{$item->name}}</h3>
                            {{-- <p class="text-2 mb-0">{{hitung_umur($item->tanggal_lahir)}}</p> --}}
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-6 order-1 order-md-2 text-center text-md-left mb-5 mb-md-0 appear-animation"
                data-appear-animation="fadeInRightShorter" data-appear-animation-delay="200">
                <h1 class="font-weight-normal text-8 mb-1 mt-3">BANTU <strong class="font-weight-bold">MEREKA </strong>
                </h1>
                <p class="lead">Mereka juga mempunyai harapan dan mimpi yang sama seperti kita</p>
                <p class="mb-4">Anda bisa ber-donasi serta membeli produk - produk hasil karya mereka dengan cara
                    melihat produk mereka di halaman profile mereka</p>
                <a href="{{ route('difa.index')}}">
                    <button type="button" class="btn btn-outline btn-rounded btn-dark  btn-with-arrow mb-2"
                        href="#">Lihat Selengkapnya <span><i class="fas fa-chevron-right"></i></span></button>
                </a>
            </div>
        </div>
    </div>

    <section id="" class="section section-background section-height-4 overlay overlay-show overlay-op-9 border-0 m-0"
        style="background-image: url(img/bg/01.jpg); background-size: cover; background-position: center;">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <h2 class="font-weight-bold text-color-light mb-2">DIFAPEDIA - DIFABEL INDONESIA</h2>
                    <p class="text-color-light opacity-7">" 21,84 juta penyandang disabilitas membutuhkan bantuan anda,
                        Beberapa vendor yang sudah bekerja sama dengan kami "</p>
                </div>
            </div>
            <div class="row text-center py-3 my-4">
                <div class="owl-carousel owl-theme carousel-center-active-item carousel-center-active-item-style-2 mb-0"
                    data-plugin-options="{'responsive': {'0': {'items': 1}, '476': {'items': 1}, '768': {'items': 5}, '992': {'items': 7}, '1200': {'items': 7}}, 'autoplay': true, 'autoplayTimeout': 3000, 'dots': false}">
                    <div>
                        <img class="img-fluid" src="{{ asset('homepage/img/vendor/01.png')}}" alt="">
                    </div>
                    <div>
                        <img class="img-fluid" src="{{ asset('homepage/img/vendor/02.png')}}" alt="">
                    </div>
                    <div>
                        <img class="img-fluid" src="{{ asset('homepage/img/vendor/01.png')}}" alt="">
                    </div>
                    <div>
                        <img class="img-fluid" src="{{ asset('homepage/img/vendor/02.png')}}" alt="">
                    </div>
                    <div>
                        <img class="img-fluid" src="{{ asset('homepage/img/vendor/01.png')}}" alt="">
                    </div>
                    <div>
                        <img class="img-fluid" src="{{ asset('homepage/img/vendor/02.png')}}" alt="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">

                    <div class="owl-carousel owl-theme nav-bottom rounded-nav mb-0"
                        data-plugin-options="{'items': 1, 'loop': true, 'autoHeight': true}">
                        <div>
                            <div
                                class="testimonial testimonial-style-2 testimonial-light testimonial-with-quotes testimonial-quotes-primary mb-0">
                                <blockquote>
                                    <p class="text-5 line-height-5 mb-0">Kami ingin dorong semua provinsi, kota, dan
                                        kabupaten juga ramah terhadap disabilitas. Ini ke depan terus kami dorong</p>
                                </blockquote>
                                <div class="testimonial-author">
                                    <p><strong class="font-weight-extra-bold text-2">- Joko Widodo. Presiden RI</strong>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <section class="section border-0 my-0 pb-4">
        <div class="container">
            <div class="row pt-5 mt-1 mb-4">
                <div class="col text-center appear-animation" data-appear-animation="fadeInUpShorter">
                    <h1 class="font-weight-normal text-8 mb-1">DIF <strong class="font-weight-bold"> PRODUCT</strong>
                    </h1>
                    <p>Beberapa produk - produk yang mereka hasilkan</p>
                </div>
            </div>

            <div class="row justify-content-center mb-3 pb-2">
                @foreach ($produk as $item)
                <div class="col-md-6 col-lg-4 col-xl-3 mb-4 pb-3 appear-animation"
                    data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="400">
                    <div class="thumb-info thumb-info-hide-wrapper-bg custom-thumb-info-style-1">
                        <div class="thumb-info-wrapper">
                            <a href="#"><img src="{{ asset('produk/'.$item->gambar_produk) }}" class="img-fluid"
                                    alt=""></a>
                            <div class="thumb-info-price bg-color-primary font-weight-semibold">
                                <span>Terlaris</span>
                            </div>
                        </div>
                        <div class="thumb-info-caption">
                            <h3 class="font-weight-semibold text-transform-none"><a href="#"
                                    class="custom-link-color-dark">{{$item->nama_produk}}</a></h3>
                            <p class="text-muted ">
                                <small>
                                    <span class="text-warning">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </span> 156 Ulasan | <a href="#"> Rahmad A.</a>

                                </small>
                            </p>
                            <div class="divider divider-small">
                                <hr>
                            </div>
                            <p class="text-1 text-muted">
                                {{$item->deskripsi_produk}}
                            </p>
                        </div>
                        <div class="thumb-info-footer">
                            <ul class="nav text-2">
                                <li><a href="#"><strong><i class="fas fa-tag text-color-primary"></i>
                                            {{$item->harga}}</strong></a></li>
                                <li class="ml-3"><a href="#"><strong><i
                                                class="fas fa-shipping-fast text-color-primary"></i> Free Ongkir
                                        </strong></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                @endforeach


            </div>
            <div class="col-lg-12 mt-4 mb-5 text-center">
                <a href="{{route('produk.index')}}">
                    <button type="button" class="btn btn-outline btn-rounded btn-primary  btn-with-arrow mb-2"
                        href="#">Lihat Lebih Banyak<span><i class="fas fa-chevron-right"></i></span></button>
                </a>
            </div>
        </div>
    </section>
    <div id="team" class="container pb-4">
        <div class="row pt-5 mt-5 mb-4">
            <div class="col text-center appear-animation" data-appear-animation="fadeInUpShorter">
                <h1 class="font-weight-normal text-8 mb-1">DIF <strong class="font-weight-bold">CAREER </strong> </h1>
                <p>Beberapa lowongan pekerjaan untuk penyandang difabel</p>
            </div>
        </div>
        <ul class="nav nav-pills sort-source sort-source-style-3 justify-content-center" data-sort-id="portfolio"
            data-option-key="filter" data-plugin-options="{'layoutMode': 'fitRows', 'filter': '*'}">
            <li class="nav-item active" data-option-value="*"><a class="nav-link text-1 text-uppercase active"
                    href="#">Semua</a></li>
            <li class="nav-item" data-option-value=".terbaru"><a class="nav-link text-1 text-uppercase"
                    href="#">Terbaru</a></li>
            <li class="nav-item" data-option-value=".rekomendasi"><a class="nav-link text-1 text-uppercase"
                    href="#">Rekomendasi</a></li>
            <li class="nav-item" data-option-value=".populer"><a class="nav-link text-1 text-uppercase"
                    href="#">Populer</a></li>
        </ul>
        <div class="sort-destination-loader sort-destination-loader-showing mt-3">
            <div class="row portfolio-list sort-destination" data-sort-id="portfolio">
                @foreach ($loker as $item)
                <div class="col-lg-12 isotope-item mt-4 terbaru">
                    <div class="row">

                        <div class="col-lg-6">
                            <div class="portfolio-item">
                                <a href="{{ route('loker.detail', ['id'=>$item->id])}}">
                                    <span
                                        class="thumb-info thumb-info-no-zoom thumb-info-lighten border-radius-0 appear-animation"
                                        data-appear-animation="fadeIn" data-appear-animation-delay="100">
                                        <span class="thumb-info-wrapper border-radius-0">
                                            <img src="{{ asset('loker/'.$item->gambar) }}"
                                                class="img-fluid border-radius-0" alt="">

                                            <span class="thumb-info-action">
                                                <span class="thumb-info-action-icon bg-dark opacity-8"><i
                                                        class="fas fa-plus"></i></span>
                                            </span>
                                        </span>
                                    </span>
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-6">

                            <div class="overflow-hidden">
                                <h2 class="text-color-dark font-weight-bold text-5 mb-2 appear-animation"
                                    data-appear-animation="maskUp" data-appear-animation-delay="600">{{$item->nama_loker}}</h2>
                            </div>

                            <p class="appear-animation" data-appear-animation="fadeInUpShorter"
                                data-appear-animation-delay="800">{{ $item->deskripsi}}</p>

                            <ul class="list list-icons list-primary list-borders text-2 appear-animation"
                                data-appear-animation="fadeInUpShorter" data-appear-animation-delay="1200">
                                <li><i class="fas fa-caret-right left-10"></i> <strong
                                        class="text-color-primary">Perusahaan:</strong> {{ $item->getHrd->nama_perusahaan}}</li>
                                <li><i class="fas fa-caret-right left-10"></i> <strong
                                        class="text-color-primary">Deadline :</strong> {{$item->deadline}}</li>
                                <li><i class="fas fa-caret-right left-10"></i> <strong
                                        class="text-color-primary">Skill:</strong> <a href="#"
                                        class="badge badge-dark badge-sm badge-pill px-2 py-1 ml-1">Tidak ada skill
                                        khusus</a>
                            </ul>

                        </div>

                    </div>
                </div>
                @endforeach


            </div>
            <div class="col-lg-12 mt-4 mb-5 text-center">
                <a href="{{route('loker.index')}}">
                    <button type="button" class="btn btn-outline btn-rounded btn-primary  btn-with-arrow mb-2"
                        href="#">Lihat Lebih Banyak<span><i class="fas fa-chevron-right"></i></span></button>
                </a>
            </div>
        </div>
    </div>


    <section id="contact" class="section bg-color-grey-scale-1 border-0 py-0 m-0">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">

                    <!-- Google Maps - Settings on footer -->
                    <div id="googlemaps" class="google-ma h-100 mb-0" style="min-height: 400px;"></div>

                </div>
                <div class="col-md-6 p-5 my-5">

                    <div class="px-4">
                        <h1 class="font-weight-normal text-8 mb-1">KONTAK <strong class="font-weight-bold">KAMI
                            </strong> </h1>

                        <p class="text-3 mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua.</p>
                        <form id="contactForm" class="contact-form form-style-2 pr-xl-5" action="" method="POST">
                            <div class="contact-form-success alert alert-success d-none mt-4" id="contactSuccess">
                                <strong>Success!</strong> Your message has been sent to us.
                            </div>

                            <div class="contact-form-error alert alert-danger d-none mt-4" id="contactError">
                                <strong>Error!</strong> There was an error sending your message.
                                <span class="mail-error-message text-1 d-block" id="mailErrorMessage"></span>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-xl-4">
                                    <input type="text" value="" data-msg-required="Please enter your name."
                                        maxlength="100" class="form-control" name="name" id="name"
                                        placeholder="Nama Lengkap" required>
                                </div>
                                <div class="form-group col-xl-8">
                                    <input type="email" value="" data-msg-required="Please enter your email address."
                                        data-msg-email="Please enter a valid email address." maxlength="100"
                                        class="form-control" name="email" id="email" placeholder="Email" required>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col">
                                    <input type="text" value="" data-msg-required="Please enter the subject."
                                        maxlength="100" class="form-control" name="subject" id="subject"
                                        placeholder="Subject" required>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col">
                                    <textarea maxlength="5000" data-msg-required="Please enter your message." rows="4"
                                        class="form-control" name="message" id="message"
                                        placeholder="Pesan / Pertanyaraan / Kritik / Saran" required></textarea>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col">
                                    <button type="button" class="mb-1 mt-1 mr-1 btn btn-primary btn-block">Kirim Pesan
                                        <i class="fas fa-paper-plane ml-1"></i> </button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </section>


</div>
@endsection
