@extends('layouts.difa')

@section('content')
<div role="main" class="main">
    <section class="page-header page-header-modern page-header-md bg-transparent m-0">
        <div class="container">
            <div class="row">
                <div class="col-md-12 align-self-center p-static order-2 text-center mt-2">
                    <h1 class="text-dark text-10"><strong>Lowongan Pekerjaan</strong></h1>
                </div>
            </div>
        </div>
    </section>
    <hr class="m-0">
    <div class="container py-5 mt-3">
        <div class="row">
            <div class="col-md-12">
                <div class="appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="400">
                    <div>
                        <div class="pb-5">
                            <img alt="" class="img-fluid rounded box-shadow-3"
                                src="{{ asset('loker/'.$loker['gambar'])}}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="overflow-hidden mb-2">
                    <h2 class="font-weight-normal text-7 mb-2 appear-animation" data-appear-animation="maskUp"
                        data-appear-animation-delay="200">{{$loker['nama_loker']}}</strong>
                    </h2>
                </div>
                <div class="overflow-hidden mb-4">
                    <p class="lead mb-0 appear-animation" data-appear-animation="maskUp"
                        data-appear-animation-delay="400">
                        {{$loker['deskripsi']}}
                    </p>
                </div>
                <br><br>
                <div class="col-lg-12 text-center">
                    <a href="#" data-toggle="modal" data-target="#modalSubmitCV"
                        class="btn btn-modern btn-primary">Daftar Sekarang !</a>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modalSubmitCV" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="largeModalLabel">Upload/Unggah CV</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <form id="demo-form" class="mb-4" novalidate="novalidate" action="{{ route('loker.add')}}" method="POST">
                    @csrf
                    <input type="hidden" name="id_loker" value="{{$loker['id']}}">
                    <div class="form-group row">
                        <label class="col-sm-3 text-left text-sm-right mb-0">Deskripsi singkat</label>
                        <div class="col-sm-9">
                            <textarea rows="5" class="form-control"
                                placeholder="Tuliskan deskripsi singkat tentang anda..." required></textarea>
                        </div>
                    </div>
                    <div class="form-group row align-items-center">
                        <label class="col-sm-3 text-left text-sm-right mb-0">Upload CV</label>
                        <div class="col-sm-9">
                            <input type="file" name="" class="form-control" placeholder="uNGGAH cv" />
                        </div>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary px-5">Kirim CV</button>
            </div></form>
        </div>
    </div>
</div>
@endsection
