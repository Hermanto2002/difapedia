@extends('layouts.difa')

@section('content')
<div role="main" class="main">
    <section class="page-header page-header-modern bg-color-light-scale-1 page-header-md ">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col">
                    <div class="row">
                        <div class="col-md-12 align-self-center p-static order-2 text-center">
                            <div class="overflow-hidden pb-2">
                                <h1 class="text-dark font-weight-bold text-9 appear-animation"
                                    data-appear-animation="maskUp" data-appear-animation-delay="100">
                                    Lowongan Pekerjaan</h2>
                            </div>
                        </div>
                        <div class="col-md-12 align-self-center order-1">
                            <ul class="breadcrumb d-block text-center appear-animation" data-appear-animation="fadeIn"
                                data-appear-animation-delay="300">
                                <li><a href="#">Beranda</a></li>
                                <li class="active">DIF <strong>CAREER</strong></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container py-2">
        <ul class="nav nav-pills sort-source sort-source-style-3 justify-content-center" data-sort-id="portfolio"
            data-option-key="filter" data-plugin-options="{'layoutMode': 'fitRows', 'filter': '*'}">
            <li class="nav-item active" data-option-value="*"><a class="nav-link text-1 text-uppercase active"
                    href="#">Semua</a></li>
            <li class="nav-item" data-option-value=".terbaru"><a class="nav-link text-1 text-uppercase"
                    href="#">Terbaru</a></li>
            <li class="nav-item" data-option-value=".rekomendasi"><a class="nav-link text-1 text-uppercase"
                    href="#">Rekomendasi</a></li>
            <li class="nav-item" data-option-value=".populer"><a class="nav-link text-1 text-uppercase"
                    href="#">Populer</a></li>
        </ul>
        <div class="sort-destination-loader sort-destination-loader-showing mt-3">
            <div class="row portfolio-list sort-destination" data-sort-id="portfolio">
                @foreach ($loker as $item)
                <div class="col-lg-12 isotope-item mt-4 terbaru">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="portfolio-item">
                                <a href="{{ url('lowongan/detail/'.$item->id)}}">
                                    <span
                                        class="thumb-info thumb-info-no-zoom thumb-info-lighten border-radius-0 appear-animation"
                                        data-appear-animation="fadeIn" data-appear-animation-delay="100">
                                        <span class="thumb-info-wrapper border-radius-0">
                                            <img src="{{ asset('loker/'.$item->gambar)}}" class="img-fluid border-radius-0" alt="">
                                            <span class="thumb-info-action">
                                                <span class="thumb-info-action-icon bg-dark opacity-8"><i
                                                        class="fas fa-plus"></i></span>
                                            </span>
                                        </span>
                                    </span>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="overflow-hidden">
                                <a href="{{ url('lowongan/detail/'.$item->id)}}">
                                    <h2 class="text-color-dark font-weight-bold text-5 mb-2 appear-animation"
                                        data-appear-animation="maskUp" data-appear-animation-delay="600">{{$item->nama_loker}}
                                    </h2>
                                </a>
                            </div>
                            <p class="appear-animation" data-appear-animation="fadeInUpShorter"
                                data-appear-animation-delay="800">{{ $item->deskripsi }}</p>
                            <ul class="list list-icons list-primary list-borders text-2 appear-animation"
                                data-appear-animation="fadeInUpShorter" data-appear-animation-delay="1200">
                                <li><i class="fas fa-caret-right left-10"></i> <strong
                                        class="text-color-primary">Perusahaan:</strong> {{$item->getHrd->nama_perusahaan}}</li>
                                <li><i class="fas fa-caret-right left-10"></i> <strong
                                        class="text-color-primary">Tanggal Posting :</strong> {{ $item->created_at->diffForHumans()}}</li>
                                <li><i class="fas fa-caret-right left-10"></i> <strong
                                        class="text-color-primary">Status:</strong> <a href="#"
                                        class="badge badge-primary badge-sm badge-pill px-2 py-1 ml-1">Terbuka</a>
                            </ul>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </div>
</div>
@endsection
