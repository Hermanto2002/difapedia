@extends('layouts.shop')
@section('content')
<div role="main" class="main shop py-4">

    <div class="container">

        <div class="row">
            <div class="col-lg-3">
                <aside class="sidebar">
                    <form action="page-search-results.html" method="get">
                        <div class="input-group mb-3 pb-1">
                            <input class="form-control text-1" placeholder="Search..." name="s" id="s" type="text">
                            <span class="input-group-append">
                                <button type="submit" class="btn btn-dark text-1 p-2"><i
                                        class="fas fa-search m-2"></i></button>
                            </span>
                        </div>
                    </form>
                    <h5 class="font-weight-bold pt-3">Categories</h5>
                    <ul class="nav nav-list flex-column">
                        <li class="nav-item"><a class="nav-link" href="#">Miniatur</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Lukisan</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Tempat ATK</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Celengan</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Gantungan</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Pot Bunga</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Bucket Bunga</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Aksesoris</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Produk Lainnya</a></li>
                    </ul>
                    <h5 class="font-weight-bold pt-5">Tag </h5>
                    <div class="mb-3 pb-1">
                        <a href="#"><span
                                class="badge badge-dark badge-sm badge-pill text-uppercase px-2 py-1 mr-1">Kerajinan
                                Tangan</span></a>
                        <a href="#"><span
                                class="badge badge-dark badge-sm badge-pill text-uppercase px-2 py-1 mr-1">Lukisan</span></a>
                        <a href="#"><span
                                class="badge badge-dark badge-sm badge-pill text-uppercase px-2 py-1 mr-1">Aksesoris</span></a>
                        <a href="#"><span
                                class="badge badge-dark badge-sm badge-pill text-uppercase px-2 py-1 mr-1">Terbaru</span></a>
                        <a href="#"><span
                                class="badge badge-dark badge-sm badge-pill text-uppercase px-2 py-1 mr-1">Promo</span></a>
                    </div>
                    <div class="row mb-5">
                        <div class="col">
                            <h5 class="font-weight-bold pt-5">Produk Terlaris</h5>
                            <ul class="simple-post-list">
                                <li>
                                    <div class="post-image">
                                        <div class="d-block">
                                            <a href="detail-product.html">
                                                <img alt="" width="60" height="60" class="img-fluid"
                                                    src="{{ asset('homepage/img/shop/03.jpg')}}">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="post-info">
                                        <a href="detail-product.html">Lukisan Pemandangan</a>
                                        <div class="post-meta text-dark font-weight-semibold">
                                            Rp. 150.000
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="post-image">
                                        <div class="d-block">
                                            <a href="#">
                                                <img alt="" width="60" height="60" class="img-fluid"
                                                    src="{{ asset('homepage/img/shop/01.jpg')}}">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="post-info">
                                        <a href="#">Miniatur</a>
                                        <div class="post-meta text-dark font-weight-semibold">
                                            Rp. 175.000
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="post-image">
                                        <div class="d-block">
                                            <a href="#">
                                                <img alt="" width="60" height="60" class="img-fluid"
                                                    src="{{ asset('homepage/img/shop/02.jpg')}}">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="post-info">
                                        <a href="shop-product-sidebar-left.html">Jopa Japu Sepedaan</a>
                                        <div class="post-meta text-dark font-weight-semibold">
                                            Rp. 100.000
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </aside>
            </div>
            <div class="col-lg-9">

                <div class="row">
                    <div class="col-lg-6">

                        <div class="owl-carousel owl-theme" data-plugin-options="{'items': 1, 'margin': 10}">
                            <div>
                                <img alt="" height="300" class="img-fluid" src="{{ asset('produk/'.$produkDetail['gambar_produk'])}}">
                            </div>
                        </div>

                    </div>

                    <div class="col-lg-6">

                        <div class="summary entry-summary">

                            <h1 class="mb-0 font-weight-bold text-7">{{$produkDetail['nama_produk']}}</h1>

                            <div class="pb-0 clearfix">
                                <div title="Rated 5 out of 5" class="float-left">
                                    <input type="text" class="d-none text-1" value="5" title="" data-plugin-star-rating
                                        data-plugin-options="{'displayOnly': true, 'color': 'warning', 'size':'s'}">
                                </div>

                                <div class="review-num">
                                    <span class="count" itemprop="ratingCount">114</span> reviews
                                </div>
                            </div>


                            <p class="price">
                                <span class="amount">Rp. {{$produkDetail['harga']}}</span>
                            </p>

                            <p class="mb-5">
                                {{$produkDetail['deskripsi_produk']}}
                            </p>

                            <form action="{{ route('cart.add')}}" enctype="multipart/form-data" method="post" class="cart">
                                @csrf
                                <div class="quantity quantity-lg">
                                    <input type="hidden" name="id" value="{{$produkDetail['id']}}">
                                    <input type="button" class="minus" value="-">
                                    <input type="text" class="input-text qty text" title="Qty" value="1" name="qty"
                                        min="1" step="1">
                                    <input type="button" class="plus" value="+">
                                </div>
                                <a>
                                    <button type="submit" class="btn btn-primary btn-modern text-uppercase">Tambah ke
                                        Keranjang</button>
                                </a>

                            </form>

                            <div class="product-meta">
                                <span class="posted-in">Kategori: <a rel="tag" href="#">Aksesoris</a>, <a rel="tag"
                                        href="#">Miniatur</a>.</span>
                            </div>

                        </div>


                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="tabs tabs-product mb-2">
                            <ul class="nav nav-tabs">
                                <li class="nav-item active"><a class="nav-link py-3 px-4" href="#productDescription"
                                        data-toggle="tab">Deskripsi Barang</a></li>
                                <li class="nav-item"><a class="nav-link py-3 px-4" href="#productInfo"
                                        data-toggle="tab">Spesifikasi</a></li>
                                <li class="nav-item"><a class="nav-link py-3 px-4" href="#productReviews"
                                        data-toggle="tab">Ulasan(114)</a></li>
                            </ul>
                            <div class="tab-content p-0">
                                <div class="tab-pane p-4 active" id="productDescription">
                                    {{$produkDetail['deskripsi_produk']}}
                                </div>
                                <div class="tab-pane p-4" id="productInfo">
                                    <table class="table m-0">
                                        <tbody>
                                            <tr>
                                                <th class="border-top-0">
                                                    Warna :
                                                </th>
                                                <td class="border-top-0">
                                                    -
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    Usia
                                                </th>
                                                <td>
                                                    10 Tahun keatas
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    Bahan Dasar
                                                </th>
                                                <td>
                                                    Kayu
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane p-4" id="productReviews">
                                    <ul class="comments">
                                        <li>
                                            <div class="comment">
                                                <div class="img-thumbnail border-0 p-0 d-none d-md-block">
                                                    <img class="avatar" alt="" src="{{ asset('homepage/img/avatars/avatar-2.jpg')}}">
                                                </div>
                                                <div class="comment-block">
                                                    <div class="comment-arrow"></div>
                                                    <span class="comment-by">
                                                        <strong>Jack Doe</strong>
                                                        <span class="float-right">
                                                            <div class="pb-0 clearfix">
                                                                <div title="Rated 3 out of 5" class="float-left">
                                                                    <input type="text" class="d-none" value="3" title=""
                                                                        data-plugin-star-rating
                                                                        data-plugin-options="{'displayOnly': true, 'color': 'primary', 'size':'xs'}">
                                                                </div>

                                                                <div class="review-num">
                                                                    <span class="count" itemprop="ratingCount">2</span>
                                                                    reviews
                                                                </div>
                                                            </div>
                                                        </span>
                                                    </span>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                                        Nam viverra euismod odio, gravida pellentesque urna
                                                        varius vitae, gravida pellentesque urna varius vitae.
                                                    </p>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="comment">
                                                <div class="img-thumbnail border-0 p-0 d-none d-md-block">
                                                    <img class="avatar" alt="" src="{{ asset('homepage/img/avatars/avatar.jpg')}}">
                                                </div>
                                                <div class="comment-block">
                                                    <div class="comment-arrow"></div>
                                                    <span class="comment-by">
                                                        <strong>John Doe</strong>
                                                        <span class="float-right">
                                                            <div class="pb-0 clearfix">
                                                                <div title="Rated 3 out of 5" class="float-left">
                                                                    <input type="text" class="d-none" value="3" title=""
                                                                        data-plugin-star-rating
                                                                        data-plugin-options="{'displayOnly': true, 'color': 'primary', 'size':'xs'}">
                                                                </div>

                                                                <div class="review-num">
                                                                    <span class="count" itemprop="ratingCount">2</span>
                                                                    reviews
                                                                </div>
                                                            </div>
                                                        </span>
                                                    </span>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                                        Nam viverra odio, gravida urna varius vitae, gravida
                                                        pellentesque urna varius vitae.</p>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <hr class="solid my-5">
                                    <h4>Add a review</h4>
                                    <div class="row">
                                        <div class="col">

                                            <form action="" id="submitReview" method="post">
                                                <div class="form-row">
                                                    <div class="form-group col pb-2">
                                                        <label
                                                            class="required font-weight-bold text-dark">Rating</label>
                                                        <input type="text" class="rating-loading" value="0" title=""
                                                            data-plugin-star-rating
                                                            data-plugin-options="{'color': 'primary', 'size':'xs'}">
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-lg-6">
                                                        <label class="required font-weight-bold text-dark">Name</label>
                                                        <input type="text" value=""
                                                            data-msg-required="Please enter your name." maxlength="100"
                                                            class="form-control" name="name" id="name" required>
                                                    </div>
                                                    <div class="form-group col-lg-6">
                                                        <label class="required font-weight-bold text-dark">Email
                                                            Address</label>
                                                        <input type="email" value=""
                                                            data-msg-required="Please enter your email address."
                                                            data-msg-email="Please enter a valid email address."
                                                            maxlength="100" class="form-control" name="email" id="email"
                                                            required>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col">
                                                        <label
                                                            class="required font-weight-bold text-dark">Review</label>
                                                        <textarea maxlength="5000"
                                                            data-msg-required="Please enter your review." rows="8"
                                                            class="form-control" name="review" id="review"
                                                            required></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col mb-0">
                                                        <input type="submit" value="Post Review"
                                                            class="btn btn-primary btn-modern"
                                                            data-loading-text="Loading...">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <hr class="solid my-5">

                <h4 class="mb-3"> <strong>Produk</strong> Lainnya </h4>
                <div class="masonry-loader masonry-loader-showing">
                    <div class="row products product-thumb-info-list mt-3" data-plugin-masonry
                        data-plugin-options="{'layoutMode': 'fitRows'}">
                        <div class="col-sm-6 col-lg-3 product">
                            <span class="product-thumb-info border-0">
                                <a href="shop-cart.html" class="add-to-cart-product bg-color-primary">
                                    <span class="text-uppercase text-1">Tambah ke Keranjang</span>
                                </a>
                                <a href="shop-product-sidebar-left.html">
                                    <span class="product-thumb-info-image">
                                        <img alt="" class="img-fluid" src="{{ asset('homepage/img/shop/04.jpg')}}">
                                    </span>
                                </a>
                                <span class="product-thumb-info-content product-thumb-info-content pl-0 bg-color-light">
                                    <a href="#">
                                        <h4 class="text-4 text-primary">Lampu Belajar</h4>
                                        <div class="pb-0 clearfix">
                                            <div title="Rated 5 out of 5" class="float-left">
                                                <input type="text" class="d-none text-1" value="5" title=""
                                                    data-plugin-star-rating
                                                    data-plugin-options="{'displayOnly': true, 'color': 'warning', 'size':'s'}">
                                            </div>

                                            <div class="review-num">
                                                <span class="count" itemprop="ratingCount">85</span> reviews
                                            </div>
                                        </div>

                                        <p class="price">

                                            <ins class="text-decoration-none"><span
                                                    class="amount text-dark font-weight-semibold">Rp.
                                                    150.000</span></ins>
                                        </p>
                                    </a>
                                </span>
                            </span>
                        </div>

                        <div class="col-sm-6 col-lg-3 product">
                            <span class="product-thumb-info border-0">
                                <a href="shop-cart.html" class="add-to-cart-product bg-color-primary">
                                    <span class="text-uppercase text-1">Tambah Ke Keranjang</span>
                                </a>
                                <a href="#">
                                    <span class="product-thumb-info-image">
                                        <img alt="" class="img-fluid" src="{{ asset('homepage/img/shop/05.jpg')}}">
                                    </span>
                                </a>
                                <span class="product-thumb-info-content product-thumb-info-content pl-0 bg-color-light">
                                    <a href="shop-product-sidebar-left.html">
                                        <h4 class="text-4 text-primary">Tempat Alat Tulis</h4>
                                        <div class="pb-0 clearfix">
                                            <div title="Rated 5 out of 5" class="float-left">
                                                <input type="text" class="d-none text-1" value="5" title=""
                                                    data-plugin-star-rating
                                                    data-plugin-options="{'displayOnly': true, 'color': 'warning', 'size':'s'}">
                                            </div>

                                            <div class="review-num">
                                                <span class="count" itemprop="ratingCount">79</span> reviews
                                            </div>
                                        </div>

                                        <p class="price">

                                            <ins class="text-decoration-none"><span
                                                    class="amount text-dark font-weight-semibold">Rp.
                                                    50.000</span></ins>
                                        </p>
                                    </a>
                                </span>
                            </span>
                        </div>
                        <div class="col-sm-6 col-lg-3 product">
                            <span class="product-thumb-info border-0">
                                <a href="shop-cart.html" class="add-to-cart-product bg-color-primary">
                                    <span class="text-uppercase text-1">Tambah ke Keranjang</span>
                                </a>
                                <a href="shop-product-sidebar-left.html">
                                    <span class="product-thumb-info-image">
                                        <img alt="" class="img-fluid" src="{{ asset('homepage/img/shop/06.jpg')}}">
                                    </span>
                                </a>
                                <span class="product-thumb-info-content product-thumb-info-content pl-0 bg-color-light">
                                    <a href="#">
                                        <h4 class="text-4 text-primary">Bingkai Foto</h4>
                                        <div class="pb-0 clearfix">
                                            <div title="Rated 4 out of 5" class="float-left">
                                                <input type="text" class="d-none text-1" value="4" title=""
                                                    data-plugin-star-rating
                                                    data-plugin-options="{'displayOnly': true, 'color': 'warning', 'size':'s'}">
                                            </div>

                                            <div class="review-num">
                                                <span class="count" itemprop="ratingCount">88</span> reviews
                                            </div>
                                        </div>

                                        <p class="price">

                                            <ins class="text-decoration-none"><span
                                                    class="amount text-dark font-weight-semibold">Rp.
                                                    45.000</span></ins>
                                        </p>
                                    </a>
                                </span>
                            </span>
                        </div>
                        <div class="col-sm-6 col-lg-3 product">
                            <span class="product-thumb-info border-0">
                                <a href="shop-cart.html" class="add-to-cart-product bg-color-primary">
                                    <span class="text-uppercase text-1">Tambah ke Keranjang</span>
                                </a>
                                <a href="shop-product-sidebar-left.html">
                                    <span class="product-thumb-info-image">
                                        <img alt="" class="img-fluid" src="{{ asset('homepage/img/shop/07.jpg')}}">
                                    </span>
                                </a>
                                <span class="product-thumb-info-content product-thumb-info-content pl-0 bg-color-light">
                                    <a href="shop-product-sidebar-left.html">
                                        <h4 class="text-4 text-primary">Miniatur Rumah</h4>
                                        <div class="pb-0 clearfix">
                                            <div title="Rated 4 out of 5" class="float-left">
                                                <input type="text" class="d-none text-1" value="4" title=""
                                                    data-plugin-star-rating
                                                    data-plugin-options="{'displayOnly': true, 'color': 'warning', 'size':'s'}">
                                            </div>

                                            <div class="review-num">
                                                <span class="count" itemprop="ratingCount">95</span> reviews
                                            </div>
                                        </div>

                                        <p class="price">
                                            <del class="text-1"><span class="amount text-1">Rp.
                                                    250.000</span></del>
                                            <ins class="text-decoration-none"><span
                                                    class="amount text-dark font-weight-semibold">Rp.
                                                    175.000</span></ins>
                                        </p>
                                    </a>
                                </span>
                            </span>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>
@endsection
