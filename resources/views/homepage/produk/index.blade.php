@extends('layouts.shop')

@section('content')
<div role="main" class="main shop py-4">

    <div class="container">

        <div class="row">
            <div class="col-lg-3">
                <aside class="sidebar">
                    <form action="page-search-results.html" method="get">
                        <div class="input-group mb-3 pb-1">
                            <input class="form-control text-1" placeholder="Cari Produk" name="s" id="s" type="text">
                            <span class="input-group-append">
                                <button type="submit" class="btn btn-dark text-1 p-2"><i
                                        class="fas fa-search m-2"></i></button>
                            </span>
                        </div>
                    </form>
                    <h5 class="font-weight-bold pt-3">Categories</h5>
                    <ul class="nav nav-list flex-column">
                        <li class="nav-item"><a class="nav-link" href="#">Miniatur</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Lukisan</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Tempat ATK</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Celengan</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Gantungan</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Pot Bunga</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Bucket Bunga</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Aksesoris</a></li>
                        <li class="nav-item"><a class="nav-link" href="#">Produk Lainnya</a></li>
                    </ul>
                    <h5 class="font-weight-bold pt-5">Tag </h5>
                    <div class="mb-3 pb-1">
                        <a href="#"><span
                                class="badge badge-dark badge-sm badge-pill text-uppercase px-2 py-1 mr-1">Kerajinan
                                Tangan</span></a>
                        <a href="#"><span
                                class="badge badge-dark badge-sm badge-pill text-uppercase px-2 py-1 mr-1">Lukisan</span></a>
                        <a href="#"><span
                                class="badge badge-dark badge-sm badge-pill text-uppercase px-2 py-1 mr-1">Aksesoris</span></a>
                        <a href="#"><span
                                class="badge badge-dark badge-sm badge-pill text-uppercase px-2 py-1 mr-1">Terbaru</span></a>
                        <a href="#"><span
                                class="badge badge-dark badge-sm badge-pill text-uppercase px-2 py-1 mr-1">Promo</span></a>
                    </div>
                    <div class="row mb-5">
                        <div class="col">
                            <h5 class="font-weight-bold pt-5">Produk Terlaris</h5>
                            <ul class="simple-post-list">
                                <li>
                                    <div class="post-image">
                                        <div class="d-block">
                                            <a href="detail-product.html">
                                                <img alt="" width="60" height="60" class="img-fluid"
                                                    src="{{ asset('homepage/img/shop/03.jpg')}}">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="post-info">
                                        <a href="detail-product.html">Lukisan Pemandangan</a>
                                        <div class="post-meta text-dark font-weight-semibold">
                                            Rp. 150.000
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="post-image">
                                        <div class="d-block">
                                            <a href="#">
                                                <img alt="" width="60" height="60" class="img-fluid"
                                                    src="{{ asset('homepage/img/shop/01.jpg')}}">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="post-info">
                                        <a href="#">Miniatur</a>
                                        <div class="post-meta text-dark font-weight-semibold">
                                            Rp. 175.000
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="post-image">
                                        <div class="d-block">
                                            <a href="#">
                                                <img alt="" width="60" height="60" class="img-fluid"
                                                    src="{{ asset('homepage/img/shop/02.jpg')}}">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="post-info">
                                        <a href="shop-product-sidebar-left.html">Jopa Japu Sepedaan</a>
                                        <div class="post-meta text-dark font-weight-semibold">
                                            Rp. 100.000
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </aside>
            </div>
            <div class="col-lg-9">

                <div class="masonry-loader masonry-loader-showing">
                    <div class="row products product-thumb-info-list" data-plugin-masonry
                        data-plugin-options="{'layoutMode': 'fitRows'}">
                        @foreach ($produk as $item)
                        <div class="col-sm-6 col-lg-4 product">
                            <a href="">
                                <span class="onsale">Promo</span>
                            </a>
                            <span class="product-thumb-info border-0">
                                <a href="shop-cart.html" class="add-to-cart-product bg-color-primary">
                                    <span class="text-uppercase text-1">Tambah ke Keranjang </span>
                                </a>
                                <a href="{{ url('/product/detail/'.$item->id)}}">
                                    <span class="product-thumb-info-image">
                                        <img alt="" class="img-fluid" src="{{ asset('produk/'.$item->gambar_produk)}}">
                                    </span>
                                </a>
                                <span class="product-thumb-info-content product-thumb-info-content pl-0 bg-color-light">
                                    <a href="{{ route('produk.detail', ['id', $item->id])}}">
                                        <h4 class="text-4 text-primary">{{$item->nama_produk}}</h4>

                                        <div class="pb-0 clearfix">
                                            <div title="Rated 5 out of 5" class="float-left">
                                                <input type="text" class="d-none text-1" value="5" title=""
                                                    data-plugin-star-rating
                                                    data-plugin-options="{'displayOnly': true, 'color': 'warning', 'size':'s'}">
                                            </div>

                                            <div class="review-num">
                                                <span class="count" itemprop="ratingCount">114</span> reviews
                                            </div>
                                        </div>

                                        <p class="price">
                                            <ins class="text-decoration-none"><span
                                                    class="amount text-dark font-weight-semibold">Rp.
                                                    {{$item->harga}}</span></ins>
                                        </p>
                                    </a>
                                </span>
                            </span>
                        </div>
                        @endforeach

                    </div>
                    <div class="row">
                        {{-- <div class="col">
                            <ul class="pagination float-right">
                                <li class="page-item"><a class="page-link" href="#"><i
                                            class="fas fa-angle-left"></i></a></li>
                                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <a class="page-link" href="#"><i class="fas fa-angle-right"></i></a>
                            </ul>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
