<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Admin Dashboard - DIFAPEDIA</title>
    <!-- Favicon -->
    <link rel="apple-touch-icon" href="{{ asset('dashboard/images/logo/apple-touch-icon.png')}}">
    <link rel="shortcut icon" href="{{ asset('dashboard/images/logo/favicon.png')}}">
    <!-- core dependcies css -->
    <link rel="stylesheet" href="{{ asset('dashboard/vendor/bootstrap/dist/css/bootstrap.css')}}" />
    <link rel="stylesheet" href="{{ asset('dashboard/vendor/PACE/themes/blue/pace-theme-minimal.css')}}" />
    <link rel="stylesheet" href="{{ asset('dashboard/vendor/perfect-scrollbar/css/perfect-scrollbar.min.css')}}" />
    <link href="{{ asset('dashboard/vendor/summernote/dist/summernote-bs4.css')}}" rel="stylesheet">
    <!-- page css -->
    <!-- core css -->
    <link href="{{ asset('dashboard/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{ asset('dashboard/css/themify-icons.css')}}" rel="stylesheet">
    <link href="{{ asset('dashboard/css/materialdesignicons.min.css')}}" rel="stylesheet">
    <link href="{{ asset('dashboard/css/animate.min.css')}}" rel="stylesheet">
    <link href="{{ asset('dashboard/css/app.css')}}" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    @yield('css')
</head>

<body>
    <div class="app header-default side-nav-dark">
        <div class="layout">
            <!-- Header START -->
            <div class="header navbar">
                <div class="header-container">
                    <div class="nav-logo">
                        <a href="index.html">
                            <div class="logo logo-dark"
                                style="background-image: url('{{ asset('dashboard/images/logo/logo.png')}}')">
                            </div>
                        </a>
                    </div>
                    <ul class="nav-left">
                        <li>
                            <a class="sidenav-fold-toggler" href="javascript:void(0);">
                                <i class="mdi mdi-menu"></i>
                            </a>
                            <a class="sidenav-expand-toggler" href="javascript:void(0);">
                                <i class="mdi mdi-menu"></i>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav-right">
                        <li class="user-profile dropdown dropdown-animated scale-left">
                            <a href="" class="dropdown-toggle" data-toggle="dropdown">
                                <img class="profile-img img-fluid"
                                    src="{{ asset('dashboard/images/avatars/thumb-13.jpg')}}" alt="">
                            </a>
                            <ul class="dropdown-menu dropdown-md p-v-0">
                                <li>
                                    <ul class="list-media">
                                        <li class="list-item p-15">
                                            <div class="media-img">
                                                <img src="{{ asset('dashboard/images/avatars/thumb-13.jpg')}}" alt="">
                                            </div>
                                            <div class="info">
                                                <span class="title text-semibold">{{ Auth()->user()->name}}</span>
                                                {{-- <span class="sub-t`itle">{{ Auth()->user()->getRoleNames()}}</span>
                                                --}}
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                                @role('admin')
                                    @php($role = 'admin')
                                @endrole

                                @role('difabel')
                                    @php($role = 'difabel')
                                @endrole

                                @role('donatur')
                                    @php($role = 'donatur')
                                @endrole

                                @role('hrd')
                                    @php($role = 'hrd')
                                @endrole
                                <li role="separator" class="divider"></li>
                                <li>
                                    <a href="/dashboard/{{$role}}/profile">
                                        <i class="ti-settings p-r-10"></i>
                                        <span>Pengaturan</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="/dashboard/{{$role}}/profile">
                                        <i class="ti-user p-r-10"></i>
                                        <span>Profil</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        <i class="ti-power-off p-r-10"></i>
                                        <span>Logout</span>
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Header END -->
            <!-- Side Nav START -->
            <div class="side-nav expand-lg">
                <div class="side-nav-inner">
                    <ul class="side-nav-menu scrollable">
                        <li class="side-nav-header">
                            <span>Navigation</span>
                        </li>
                        @role('admin')
                        <li class="nav-item dropdown @if(\Request::route()->getName()=='admin.index') open @endif">
                            <a class="dropdown-toggle" href="javascript:void(0);">
                                <span class="icon-holder">
                                    <i class="mdi mdi-gauge"></i>
                                </span>
                                <span class="title">Dashboard</span>
                                <span class="arrow">
                                    <i class="mdi mdi-chevron-right"></i>
                                </span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="active">
                                    <a href="{{ route('admin.index')}}">Overview</a>
                                </li>
                            </ul>
                        </li>
                        <li
                            class="nav-item dropdown @if(\Request::route()->getName()=='admin.difabel.index') open @endif">
                            <a class="dropdown-toggle" href="javascript:void(0);">
                                <span class="icon-holder">
                                    <i class="mdi mdi-image-filter-tilt-shift"></i>
                                </span>
                                <span class="title">Difabel</span>
                                <span class="arrow">
                                    <i class="mdi mdi-chevron-right"></i>
                                </span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="active">
                                    <a href="{{ route('admin.difabel.index')}}">List Difabel</a>
                                </li>
                                <li>
                                    <a href="{{ route('admin.difabel.create')}}">Register Difabel</a>
                                </li>
                                <li>
                                    <a href="{{ route('admin.difabel.produk.index')}}">Produk</a>
                                </li>
                            </ul>
                        </li>
                        <li
                            class="nav-item dropdown @if(\Request::route()->getName()=='admin.donatur.index') open @endif">
                            <a class="dropdown-toggle" href="javascript:void(0);">
                                <span class="icon-holder">
                                    <i class="mdi mdi-image-filter-tilt-shift"></i>
                                </span>
                                <span class="title">Donatur</span>
                                <span class="arrow">
                                    <i class="mdi mdi-chevron-right"></i>
                                </span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="active">
                                    <a href="{{ route('admin.donatur.index')}}">List Donatur</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown @if(\Request::route()->getName()=='admin.hrd.*') open @endif">
                            <a class="dropdown-toggle" href="javascript:void(0);">
                                <span class="icon-holder">
                                    <i class="mdi mdi-image-filter-tilt-shift"></i>
                                </span>
                                <span class="title">HRD</span>
                                <span class="arrow">
                                    <i class="mdi mdi-chevron-right"></i>
                                </span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="active">
                                    <a href="{{ route('admin.hrd.index')}}">List HRD</a>
                                </li>
                                <li>
                                    <a href="{{ route('admin.hrdLoker.index')}}">List Lowongan Kerja</a>
                                </li>
                            </ul>
                        </li>
                        <li
                            class="nav-item dropdown @if(\Request::route()->getName()=='admin.donasi.*' || \Request::route()->getName()=='admin.produk.*' ) open @endif">
                            <a class="dropdown-toggle" href="javascript:void(0);">
                                <span class="icon-holder">
                                    <i class="mdi mdi-image-filter-tilt-shift"></i>
                                </span>
                                <span class="title">Pembayaran</span>
                                <span class="arrow">
                                    <i class="mdi mdi-chevron-right"></i>
                                </span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="active">
                                    <a href="{{ route('admin.donasi.index')}}">Donasi</a>
                                </li>
                                <li>
                                    <a href="{{ route('admin.produk.index')}}">Produk</a>
                                </li>
                            </ul>
                        </li>
                        @endrole

                        @role('hrd')
                        <li class="nav-item dropdown">
                            <a class="dropdown-toggle" href="javascript:void(0);">
                                <span class="icon-holder">
                                    <i class="mdi mdi-image-filter-tilt-shift"></i>
                                </span>
                                <span class="title">Lowongan Kerja</span>
                                <span class="arrow">
                                    <i class="mdi mdi-chevron-right"></i>
                                </span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="active">
                                    <a href="{{ route('hrd.loker.index')}}">List Lowongan Kerja</a>
                                </li>
                            </ul>
                        </li>
                        @endrole

                        @role('difabel')
                        </li>
                        <li class="nav-item dropdown">
                            <a class="dropdown-toggle" href="javascript:void(0);">
                                <span class="icon-holder">
                                    <i class="mdi mdi-gauge"></i>
                                </span>
                                <span class="title">Dashboard</span>
                                <span class="arrow">
                                    <i class="mdi mdi-chevron-right"></i>
                                </span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{{ route('difabel.index')}}">Overview</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown ">
                            <a class="dropdown-toggle" href="javascript:void(0);">
                                <span class="icon-holder">
                                    <i class="mdi mdi-image-filter-tilt-shift"></i>
                                </span>
                                <span class="title">Donasi</span>
                                <span class="arrow">
                                    <i class="mdi mdi-chevron-right"></i>
                                </span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="active">
                                    <a href="{{ route('difabel.donasi.index')}}">List Donasi</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="dropdown-toggle" href="javascript:void(0);">
                                <span class="icon-holder">
                                    <i class="mdi mdi-image-filter-tilt-shift"></i>
                                </span>
                                <span class="title">Produk</span>
                                <span class="arrow">
                                    <i class="mdi mdi-chevron-right"></i>
                                </span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="active">
                                    <a href="{{ route('difabel.penjualan.index')}}">List Penjualan</a>
                                </li>
                                <li>
                                    <a href="{{ route('difabel.produk.index')}}">List Produk</a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="dropdown-toggle" href="javascript:void(0);">
                                <span class="icon-holder">
                                    <i class="mdi mdi-image-filter-tilt-shift"></i>
                                </span>
                                <span class="title">Lowongan Kerja</span>
                                <span class="arrow">
                                    <i class="mdi mdi-chevron-right"></i>
                                </span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="active">
                                    <a href="{{ route('difabel.loker.index')}}">List Lowongan Kerja</a>
                                </li>
                            </ul>
                        </li>
                        @endrole

                        @role('donatur')
                        <li class="nav-item dropdown open">
                            <a class="dropdown-toggle" href="javascript:void(0);">
                                <span class="icon-holder">
                                    <i class="mdi mdi-gauge"></i>
                                </span>
                                <span class="title">Dashboard</span>
                                <span class="arrow">
                                    <i class="mdi mdi-chevron-right"></i>
                                </span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="active">
                                    <a href="{{ route('donatur.index')}}">Overview</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="dropdown-toggle" href="javascript:void(0);">
                                <span class="icon-holder">
                                    <i class="mdi mdi-image-filter-tilt-shift"></i>
                                </span>
                                <span class="title">Pembayaran</span>
                                <span class="arrow">
                                    <i class="mdi mdi-chevron-right"></i>
                                </span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="active">
                                    <a href="{{ route('donatur.donasi.index')}}">Donasi</a>
                                </li>
                                <li>
                                    <a href="{{ route('donatur.produk.index')}}">Produk</a>
                                </li>
                            </ul>
                        </li>
                        @endrole
                    </ul>
                </div>
            </div>
            <!-- Side Nav END -->
            <!-- Page Container START -->
            <div class="page-container">
                <!-- Content Wrapper START -->
                <div class="main-content">
                    <div class="container-fluid">
                        @yield('content')
                    </div>
                </div>
                <!-- Content Wrapper END -->
                <!-- Footer START -->
                <footer class="content-footer">
                    <div class="footer">
                        <div class="copyright">
                            <span>Copyright © 2021 <b class="text-dark">DIFAPEDIA</b>. All rights reserved.</span>
                        </div>
                    </div>
                </footer>
                <!-- Footer END -->
            </div>
            <!-- Page Container END -->
        </div>
    </div>

    <!-- build:js dashboard/js/vendor.js -->
    <!-- core dependcies js -->
    <script src="{{ asset('dashboard/vendor/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{ asset('dashboard/vendor/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{ asset('dashboard/vendor/bootstrap/dist/js/bootstrap.js')}}"></script>
    <script src="{{ asset('dashboard/vendor/PACE/pace.min.js')}}"></script>
    <script src="{{ asset('dashboard/vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.js')}}"></script>
    <script src="{{ asset('dashboard/vendor/d3/d3.min.js')}}"></script>
    <!-- endbuild -->
    <!-- build:js {{ asset('dashboard/js/app.min.js')}} -->
    <!-- core js -->
    <script src="{{ asset('dashboard/js/app.js')}}"></script>
    <!-- configurator js -->
    <script src="{{ asset('dashboard/js/configurator.js')}}"></script>
    <!-- endbuild -->
    <!-- page js -->
    <script src="{{ asset('dashboard/vendor/chart.js/dist/Chart.min.js')}}"></script>
    <script src="{{ asset('dashboard/vendor/jquery.sparkline/jquery.sparkline.min.js')}}"></script>
    <script src="{{ asset('dashboard/js/dashboard/default.js')}}"></script>
    <script src="{{ asset('dashboard/vendor/summernote/dist/summernote-bs4.min.js')}}"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('dashboard/js/datatables/main.js')}}"></script>
</body>

</html>
