<!DOCTYPE html>
<html>

<head>
    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Daftar Difabel | DIFA.ID - Difabel Indonesia</title>
    <meta name="keywords" content="" />
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon -->
    <link rel="shortcut icon" href="img/favicon.png" type="image/x-icon" />
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">
    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light"
        rel="stylesheet" type="text/css">
    <!-- Vendor CSS -->
    <link rel="stylesheet" href="{{ asset('homepage/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/vendor/fontawesome-free/css/all.min.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/vendor/animate/animate.min.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/vendor/simple-line-icons/css/simple-line-icons.min.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/vendor/owl.carousel/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/vendor/owl.carousel/assets/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/vendor/magnific-popup/magnific-popup.min.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/vendor/bootstrap-star-rating/css/star-rating.min.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/vendor/bootstrap-star-rating/themes/krajee-fas/theme.min.css')}}">
    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{ asset('homepage/css/theme.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/css/theme-elements.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/css/theme-blog.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/css/theme-shop.css')}}">
    <!-- Demo CSS -->
    <!-- Skin CSS -->
    <link rel="stylesheet" href="{{ asset('homepage/css/skins/default.css')}}">
    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="{{ asset('homepage/css/custom.css')}}">
    <!-- Head Libs -->
    <script src="{{ asset('homepage/vendor/modernizr/modernizr.min.js')}}"></script>
</head>

<body>
    <div class="body">
        <header id="header" class="header-effect-shrink"
            data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyChangeLogo': true, 'stickyStartAt': 30, 'stickyHeaderContainerHeight': 70}">
            <div class="header-body border-color-primary border-bottom-0 box-shadow-none"
                data-sticky-header-style="{'minResolution': 0}"
                data-sticky-header-style-active="{'background-color': '#f7f7f7'}"
                data-sticky-header-style-deactive="{'background-color': '#FFF'}">
                <div class="header-container container">
                    <div class="header-row">
                        <div class="header-column">
                            <div class="header-row">
                                <div class="header-logo">
                                    <a href="{{ route('index')}}">
                                        <img alt="" width="160" height="52" data-sticky-width="125"
                                            data-sticky-height="40" src="{{ asset('homepage/img/logo-new.png')}}">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="header-column justify-content-end">
                            <div class="header-row">
                                <div
                                    class="header-nav header-nav-line header-nav-top-line header-nav-top-line-with-border order-2 order-lg-1">
                                    <div
                                        class="header-nav-main header-nav-main-square header-nav-main-effect-2 header-nav-main-sub-effect-1">
                                        <nav class="collapse">
                                            <ul class="nav nav-pills" id="mainNav">
                                                <li>
                                                    <a class="dropdown-item dropdown-toggle" href="/">
                                                        Beranda
                                                    </a>
                                                </li>
                                                <li class="dropdown">
                                                    <a class="dropdown-item dropdown-toggle active" href="#">
                                                        Donasi
                                                    </a>
                                                </li>
                                                <li class="dropdown">
                                                    <a class="dropdown-item dropdown-toggle" href="#">
                                                        Produk
                                                    </a>
                                                </li>
                                                <li class="dropdown">
                                                    <a class="dropdown-item dropdown-toggle" href="#">
                                                        Lowongan Pekerjaan
                                                    </a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                    <button class="btn header-btn-collapse-nav" data-toggle="collapse"
                                        data-target=".header-nav-main nav">
                                        <i class="fas fa-bars"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        @yield('content')

    </div>
    <footer id="footer" class="mt-0">
        <div class="footer-copyright">
            <div class="container py-2">
                <div class="row py-4">
                    <div class="col d-flex align-items-center justify-content-center">
                        <p class="text-center"><strong>DIFAPEDIA.ID - DIFABEL INDONESIA</strong> <br> ©Copyright 2021.
                            All Rights Reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    </div>
    <!-- Vendor -->
    <script src="{{ asset('homepage/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/jquery.appear/jquery.appear.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/jquery.easing/jquery.easing.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/jquery.cookie/jquery.cookie.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/popper/umd/popper.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/common/common.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/jquery.validation/jquery.validate.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/jquery.gmap/jquery.gmap.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/jquery.lazyload/jquery.lazyload.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/isotope/jquery.isotope.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/owl.carousel/owl.carousel.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/vide/jquery.vide.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/vivus/vivus.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/bootstrap-star-rating/js/star-rating.min.js')}}"></script>
    <script src="{{ asset('homepage/vendor/bootstrap-star-rating/themes/krajee-fas/theme.min.js')}}"></script>
    <!-- Theme Base, Components and Settings -->
    <script src="{{ asset('homepage/js/theme.js')}}"></script>
    <!-- Current Page Vendor and Views -->
    <script src="{{ asset('homepage/js/views/view.shop.js')}}"></script>
    <!-- Theme Custom -->
    <script src="{{ asset('homepage/js/custom.js')}}"></script>
    <!-- Theme Initialization Files -->
    <script src="{{ asset('homepage/js/theme.init.js')}}"></script>
    <!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
         <script>
         	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
         	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
         	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
         	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

         	ga('create', 'UA-12345678-1', 'auto');
         	ga('send', 'pageview');
         </script>
          -->
</body>

</html>
