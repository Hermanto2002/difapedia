<!DOCTYPE html>
<html>

<head>

    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>DifaPedia - Difabel Indonesia</title>

    <meta name="keywords" content="" />
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('homepage/img/favicon.png')}}" type="image/x-icon" />
    <link rel="apple-touch-icon" href="{{ asset('homepage/img/apple-touch-icon.png')}}">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light"
        rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="{{ asset('homepage/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/vendor/fontawesome-free/css/all.min.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/vendor/animate/animate.min.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/vendor/simple-line-icons/css/simple-line-icons.min.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/vendor/owl.carousel/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/vendor/owl.carousel/assets/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/vendor/magnific-popup/magnific-popup.min.css')}}">

    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{ asset('homepage/css/theme.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/css/theme-elements.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/css/theme-blog.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/css/theme-shop.css')}}">

    <!-- Current Page CSS -->
    <link rel="stylesheet" href="{{ asset('homepage/vendor/rs-plugin/css/settings.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/vendor/rs-plugin/css/layers.css')}}">
    <link rel="stylesheet" href="{{ asset('homepage/vendor/rs-plugin/css/navigation.css')}}">

    <!-- Demo CSS -->
    <link rel="stylesheet" href="{{ asset('homepage/css/demos/demo-education.css')}}">

    <!-- Skin CSS -->
    <link rel="stylesheet" href="{{ asset('homepage/css/skins/skin-education.css')}}">

    <!-- Skin CSS -->
    <link rel="stylesheet" href="{{ asset('homepage/css/skins/default.css')}}">

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="{{ asset('homepage/css/custom.css')}}">

    <!-- Head Libs -->
    <script src="{{ asset('homepage/vendor/modernizr/modernizr.min.js')}}"></script>

</head>

<body class="loading-overlay-showing" data-plugin-page-transition data-loading-overlay
    data-plugin-options="{'hideDelay': 500}">
    <div class="loading-overlay">
        <div class="bounce-loader">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <body class="one-page" data-target="#header" data-spy="scroll" data-offset="100">

        <div class="body">

            <header id="header" class="header-transparent header-effect-shrink"
                data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyChangeLogo': true, 'stickyStartAt': 30, 'stickyHeaderContainerHeight': 70}">
                <div class="header-body border-top-0 header-body-bottom-border">
                    <div class="header-top header-top-borders">
                        <div class="container h-100">
                            <div class="header-row h-100">
                                <div class="header-column justify-content-start">
                                    <div class="header-row">
                                        <nav class="header-nav-top">
                                            <ul class="nav nav-pills">
                                                <li class="nav-item nav-item-borders py-2">
                                                    <span class="pl-0">
                                                        <i class="far fa-dot-circle text-4 text-color-primary"
                                                            style="top: 1px;"></i> Live Donasi <i
                                                            class="fas fa-angle-right"></i> <strong> Ahmad Abdul : Rp.
                                                            50.000</strong>
                                                    </span>
                                                </li>

                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                                <div class="header-column justify-content-end">
                                    <div class="header-row">
                                        <nav class="header-nav-top">
                                            <ul class="nav nav-pills">

                                                <li class="nav-item dropdown nav-item-left-border d-none d-sm-block">
                                                    @auth
                                                        @role('admin')
                                                        <a class="nav-link" href="{{ route('admin.index')}}">
                                                            <i class="fas fa-sign-in-alt"></i> Dashboard Admin
                                                        </a>
                                                        @endrole

                                                        @role('difabel')
                                                        <a class="nav-link" href="{{ route('difabel.index')}}">
                                                            <i class="fas fa-sign-in-alt"></i> Dashboard Difabel
                                                        </a>
                                                        @endrole

                                                        @role('donatur')
                                                        <a class="nav-link" href="{{ route('donatur.index')}}">
                                                            <i class="fas fa-sign-in-alt"></i> Dashboard Donatur
                                                        </a>
                                                        @endrole

                                                        @role('hrd')
                                                        <a class="nav-link" href="{{ route('hrd.loker.index')}}">
                                                            <i class="fas fa-sign-in-alt"></i> Dashboard HRD
                                                        </a>
                                                        @endrole
                                                    @endauth


                                                    @guest
                                                    <a class="nav-link" href="{{ route('login')}}">
                                                        <i class="fas fa-sign-in-alt"></i> Masuk / Daftar
                                                    </a>
                                                    @endguest

                                                    <div class="dropdown-menu dropdown-menu-right"
                                                        aria-labelledby="dropdownLanguage">
                                                        <a class="dropdown-item" href="#" data-toggle="modal"
                                                            data-target="#ModalDonatur"> Donatur </a>
                                                        <a class="dropdown-item" href="#" data-toggle="modal"
                                                            data-target="#ModalDifabel">Difabel </a>
                                                        <a class="dropdown-item" href="#" data-toggle="modal"
                                                            data-target="#ModalPerusahaan">Perusahaan </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="header-container container">
                        <div class="header-row">
                            <div class="header-column">
                                <div class="header-row">
                                    <div class="header-logo">
                                        <a href="{{ route('index')}}">
                                            <img alt="" width="160" height="52" data-sticky-width="125"
                                                data-sticky-height="40" src="{{ asset('homepage/img/logo-new.png')}}">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="header-column justify-content-end">
                                <div class="header-row">
                                    <div class="header-nav header-nav-links order-2 order-lg-1">
                                        <div
                                            class="header-nav-main header-nav-main-square header-nav-main-dropdown-no-borders header-nav-main-effect-2 header-nav-main-sub-effect-1">
                                            <nav class="collapse">
                                                <ul class="nav nav-pills" id="mainNav">
                                                    <li class="dropdown">
                                                        <a class="dropdown-item dropdown-toggle active"
                                                            href="/">
                                                            Beranda
                                                        </a>
                                                    </li>
                                                    <li class="dropdown">
                                                        <a class="dropdown-item dropdown-toggle" href="#">
                                                            Donasi
                                                        </a>
                                                        <ul class="dropdown-menu">
                                                            <li class="dropdown-submenu">
                                                                <a class="dropdown-item" href="#">Sub 1</a>
                                                            </li>
                                                            <li class="dropdown-submenu">
                                                                <a class="dropdown-item" href="#">Sub 2</a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li class="dropdown">
                                                        <a class="dropdown-item dropdown-toggle" href="#">
                                                            Tentang Kami
                                                        </a>

                                                    </li>
                                                    <li class="dropdown">
                                                        <a class="dropdown-item dropdown-toggle" href="#">
                                                            Kontak Kami
                                                        </a>
                                                    </li>
                                                </ul>
                                            </nav>
                                        </div>

                                        <button class="btn header-btn-collapse-nav" data-toggle="collapse"
                                            data-target=".header-nav-main nav">
                                            <i class="fas fa-bars"></i>
                                        </button>
                                    </div>
                                    <div
                                        class="header-nav-features header-nav-features-no-border header-nav-features-lg-show-border order-1 order-lg-2">
                                        <div class="header-nav-feature header-nav-features-search d-inline-flex">
                                            <a href="#" class="header-nav-features-toggle" data-focus="headerSearch"><i
                                                    class="fas fa-search header-nav-top-icon"></i></a>
                                            <div class="header-nav-features-dropdown header-nav-features-dropdown-mobile-fixed"
                                                id="headerTopSearchDropdown">
                                                <form role="search" action="page-search-results.html" method="get">
                                                    <div class="simple-search input-group">
                                                        <input class="form-control text-1" id="headerSearch" name="q"
                                                            type="search" value="" placeholder="Search...">
                                                        <span class="input-group-append">
                                                            <button class="btn" type="submit">
                                                                <i class="fa fa-search header-nav-top-icon"></i>
                                                            </button>
                                                        </span>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

            @yield('content')

            <footer id="footer" class="mt-0">
                <div class="footer-copyright">
                    <div class="container py-2">
                        <div class="row py-4">
                            <div class="col d-flex align-items-center justify-content-center">
                                <p class="text-center"><strong>DIFAPEDIA - DIFABEL INDONESIA</strong> <br> ©Copyright
                                    2021. All Rights Reserved.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>

            <!-- Modal Sign-in Donatur -->
            <div class="modal fade" id="ModalDonatur" tabindex="-1" role="dialog" aria-labelledby="defaultModalLabel"
                aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Donatur Form (Masuk/Daftar)</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="tabs">
                                        <ul class="nav nav-tabs nav-justified flex-column flex-md-row">
                                            <li class="nav-item active">
                                                <a class="nav-link" href="#inDonatur" data-toggle="tab"
                                                    class="text-center">Masuk</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#regDonatur" data-toggle="tab"
                                                    class="text-center">Daftar</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div id="inDonatur" class="tab-pane active">
                                                <div class="row justify-content-md-center">
                                                    <div class="col-md-12">
                                                        <div class="text-left mt-0">
                                                            <div class="box-content">
                                                                <form action="/" id="frmSignIn" method="post"
                                                                    class="needs-validation">
                                                                    <div class="form-row">
                                                                        <div class="form-group col">
                                                                            <label
                                                                                class="font-weight-bold text-dark text-2">Username
                                                                                / E-mail </label>
                                                                            <input type="text" value=""
                                                                                class="form-control form-control-lg"
                                                                                required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-row">
                                                                        <div class="form-group col">
                                                                            <a class="float-right" href="#">(Lupa
                                                                                Password?)</a>
                                                                            <label
                                                                                class="font-weight-bold text-dark text-2">Password</label>
                                                                            <input type="password" value=""
                                                                                class="form-control form-control-lg"
                                                                                required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-row">
                                                                        <div class="form-group col-lg-6">
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox"
                                                                                    class="custom-control-input"
                                                                                    id="rememberme">
                                                                                <label
                                                                                    class="custom-control-label text-2"
                                                                                    for="rememberme">Ingat Saya</label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group col-lg-6">
                                                                            <input type="submit" value="Login"
                                                                                class="btn btn-primary btn-modern float-right"
                                                                                data-loading-text="Loading...">
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div id="regDonatur" class="tab-pane">
                                                <div class="row justify-content-md-center">
                                                    <div class="col-md-12">
                                                        <div class="text-left mt-0">
                                                            <div class="box-content">
                                                                <form action="/" id="frmSignUp" method="post"
                                                                    class="needs-validation">
                                                                    <div class="form-row">
                                                                        <div class="form-group col">
                                                                            <label
                                                                                class="font-weight-bold text-dark text-2">E-mail</label>
                                                                            <input type="text" value=""
                                                                                class="form-control form-control-lg"
                                                                                required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-row">
                                                                        <div class="form-group col-lg-6">
                                                                            <label
                                                                                class="font-weight-bold text-dark text-2">Password</label>
                                                                            <input type="password" value=""
                                                                                class="form-control form-control-lg"
                                                                                required>
                                                                        </div>
                                                                        <div class="form-group col-lg-6">
                                                                            <label
                                                                                class="font-weight-bold text-dark text-2">Ulangi
                                                                                Password</label>
                                                                            <input type="password" value=""
                                                                                class="form-control form-control-lg"
                                                                                required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-row">
                                                                        <div class="form-group col-lg-9">
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox"
                                                                                    class="custom-control-input"
                                                                                    id="terms">
                                                                                <label
                                                                                    class="custom-control-label text-2"
                                                                                    for="terms">Saya telah menyetujui <a
                                                                                        href="#">Syarat & Ketentuan
                                                                                    </a></label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group col-lg-3">
                                                                            <input type="submit" value="Daftar"
                                                                                class="btn btn-primary btn-modern float-right"
                                                                                data-loading-text="Loading...">
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Modal Sign-in Donatur -->
            <!-- Modal Sign-in Difabel -->
            <div class="modal fade" id="ModalDifabel" tabindex="-1" role="dialog" aria-labelledby="defaultModalLabel"
                aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Difabel Form (Masuk/Daftar)</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="tabs">
                                        <ul class="nav nav-tabs nav-justified flex-column flex-md-row">
                                            <li class="nav-item active">
                                                <a class="nav-link" href="#inDifabel" data-toggle="tab"
                                                    class="text-center">Masuk</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#regDifabel" data-toggle="tab"
                                                    class="text-center">Daftar</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div id="inDifabel" class="tab-pane active">
                                                <div class="row justify-content-md-center">
                                                    <div class="col-md-12">
                                                        <div class="text-left mt-0">
                                                            <div class="box-content">
                                                                <form action="/" id="frmSignIn" method="post"
                                                                    class="needs-validation">
                                                                    <div class="form-row">
                                                                        <div class="form-group col">
                                                                            <label
                                                                                class="font-weight-bold text-dark text-2">Username
                                                                                / E-mail </label>
                                                                            <input type="text" value=""
                                                                                class="form-control form-control-lg"
                                                                                required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-row">
                                                                        <div class="form-group col">
                                                                            <a class="float-right" href="#">(Lupa
                                                                                Password?)</a>
                                                                            <label
                                                                                class="font-weight-bold text-dark text-2">Password</label>
                                                                            <input type="password" value=""
                                                                                class="form-control form-control-lg"
                                                                                required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-row">
                                                                        <div class="form-group col-lg-6">
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox"
                                                                                    class="custom-control-input"
                                                                                    id="rememberme">
                                                                                <label
                                                                                    class="custom-control-label text-2"
                                                                                    for="rememberme">Ingat Saya</label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group col-lg-6">
                                                                            <input type="submit" value="Login"
                                                                                class="btn btn-primary btn-modern float-right"
                                                                                data-loading-text="Loading...">
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div id="regDifabel" class="tab-pane" align="center">
                                                <p>Silahkan Daftar Disini</p>
                                                <a href="template-choose.html">
                                                    <button type="button" class="mb-1 btn btn-outline btn-info"> Daftar
                                                        Sekarang <i class="fas fa-sign-in-alt"></i> </button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Modal Sign-in Difabel -->
            <!-- Modal Sign-in Perusahaan -->
            <div class="modal fade" id="ModalPerusahaan" tabindex="-1" role="dialog" aria-labelledby="defaultModalLabel"
                aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="defaultModalLabel">Employer / Perusahaan Form (Masuk/Daftar)
                            </h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="tabs">
                                        <ul class="nav nav-tabs nav-justified flex-column flex-md-row">
                                            <li class="nav-item active">
                                                <a class="nav-link" href="#inPerusahaan" data-toggle="tab"
                                                    class="text-center">Masuk</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#regPerusahaan" data-toggle="tab"
                                                    class="text-center">Daftar</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div id="inPerusahaan" class="tab-pane active">
                                                <div class="row justify-content-md-center">
                                                    <div class="col-md-12">
                                                        <div class="text-left mt-0">
                                                            <div class="box-content">
                                                                <form action="/" id="frmSignIn" method="post"
                                                                    class="needs-validation">
                                                                    <div class="form-row">
                                                                        <div class="form-group col">
                                                                            <label
                                                                                class="font-weight-bold text-dark text-2">Username
                                                                                / E-mail </label>
                                                                            <input type="text" value=""
                                                                                class="form-control form-control-lg"
                                                                                required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-row">
                                                                        <div class="form-group col">
                                                                            <a class="float-right" href="#">(Lupa
                                                                                Password?)</a>
                                                                            <label
                                                                                class="font-weight-bold text-dark text-2">Password</label>
                                                                            <input type="password" value=""
                                                                                class="form-control form-control-lg"
                                                                                required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-row">
                                                                        <div class="form-group col-lg-6">
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox"
                                                                                    class="custom-control-input"
                                                                                    id="rememberme">
                                                                                <label
                                                                                    class="custom-control-label text-2"
                                                                                    for="rememberme">Ingat Saya</label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group col-lg-6">
                                                                            <input type="submit" value="Login"
                                                                                class="btn btn-primary btn-modern float-right"
                                                                                data-loading-text="Loading...">
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div id="regPerusahaan" class="tab-pane">
                                                <div class="row justify-content-md-center">
                                                    <div class="col-md-12">
                                                        <div class="text-left mt-0">
                                                            <div class="box-content">
                                                                <form action="/" id="frmSignUp" method="post"
                                                                    class="needs-validation">
                                                                    <div class="form-row">
                                                                        <div class="form-group col">
                                                                            <label
                                                                                class="font-weight-bold text-dark text-2">E-mail</label>
                                                                            <input type="text" value=""
                                                                                class="form-control form-control-lg"
                                                                                required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-row">
                                                                        <div class="form-group col-lg-6">
                                                                            <label
                                                                                class="font-weight-bold text-dark text-2">Password</label>
                                                                            <input type="password" value=""
                                                                                class="form-control form-control-lg"
                                                                                required>
                                                                        </div>
                                                                        <div class="form-group col-lg-6">
                                                                            <label
                                                                                class="font-weight-bold text-dark text-2">Ulangi
                                                                                Password</label>
                                                                            <input type="password" value=""
                                                                                class="form-control form-control-lg"
                                                                                required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-row">
                                                                        <div class="form-group col-lg-9">
                                                                            <div class="custom-control custom-checkbox">
                                                                                <input type="checkbox"
                                                                                    class="custom-control-input"
                                                                                    id="terms">
                                                                                <label
                                                                                    class="custom-control-label text-2"
                                                                                    for="terms">Saya telah menyetujui <a
                                                                                        href="#">Syarat & Ketentuan
                                                                                    </a></label>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group col-lg-3">
                                                                            <input type="submit" value="Daftar"
                                                                                class="btn btn-primary btn-modern float-right"
                                                                                data-loading-text="Loading...">
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Modal Sign-in Perusahaan -->
        </div>

        <!-- Vendor -->
        <script src="{{ asset('homepage/vendor/jquery/jquery.min.js')}}"></script>
        <script src="{{ asset('homepage/vendor/jquery.appear/jquery.appear.min.js')}}"></script>
        <script src="{{ asset('homepage/vendor/jquery.easing/jquery.easing.min.js')}}"></script>
        <script src="{{ asset('homepage/vendor/jquery.cookie/jquery.cookie.min.js')}}"></script>
        <script src="{{ asset('homepage/vendor/popper/umd/popper.min.js')}}"></script>
        <script src="{{ asset('homepage/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
        <script src="{{ asset('homepage/vendor/common/common.min.js')}}"></script>
        <script src="{{ asset('homepage/vendor/jquery.validation/jquery.validate.min.js')}}"></script>
        <script src="{{ asset('homepage/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js')}}"></script>
        <script src="{{ asset('homepage/vendor/jquery.gmap/jquery.gmap.min.js')}}"></script>
        <script src="{{ asset('homepage/vendor/jquery.lazyload/jquery.lazyload.min.js')}}"></script>
        <script src="{{ asset('homepage/vendor/isotope/jquery.isotope.min.js')}}"></script>
        <script src="{{ asset('homepage/vendor/owl.carousel/owl.carousel.min.js')}}"></script>
        <script src="{{ asset('homepage/vendor/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{ asset('homepage/vendor/vide/jquery.vide.min.js')}}"></script>
        <script src="{{ asset('homepage/vendor/vivus/vivus.min.js')}}"></script>

        <!-- Theme Base, Components and Settings -->
        <script src="{{ asset('homepage/js/theme.js')}}"></script>

        <!-- Current Page Vendor and Views -->
        <script src="{{ asset('homepage/vendor/rs-plugin/js/jquery.themepunch.tools.min.js')}}"></script>
        <script src="{{ asset('homepage/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>
        <script src="{{ asset('homepage/js/views/view.contact.js')}}"></script>

        <!-- Theme Custom -->
        <script src="{{ asset('homepage/js/custom.js')}}"></script>

        <!-- Theme Initialization Files -->
        <script src="{{ asset('homepage/js/theme.init.js')}}"></script>

        <!-- Examples -->
        <script src="{{ asset('homepage/js/examples/examples.portfolio.js')}}"></script>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBSX05mK1Dt8MoDAgcsaVrXfn_Rl11rQYM"></script>
        <script>
            // Map Markers
            var mapMarkers = [{
                address: "Jl. Raya Puputan No.86, Dangin Puri Klod, Kec. Denpasar Selatan",
                html: "<strong>Kota Denpasar</strong><br>Bali 80234, Indonesia<br><br><a href='#' onclick='mapCenterAt({latitude: -8.673099, longitude: 115.226640, zoom: 25}, event)'>[+] zoom here</a>",
                icon: {
                    image: "{{ asset('homepage/img/pin.png')}}",
                    iconsize: [26, 46],
                    iconanchor: [12, 46]
                }
            }];

            // Map Initial Location
            var initLatitude = -8.673099;
            var initLongitude = 115.226640;

            // Map Extended Settings
            var mapSettings = {
                controls: {
                    draggable: (($.browser.mobile) ? false : true),
                    panControl: true,
                    zoomControl: true,
                    mapTypeControl: true,
                    scaleControl: true,
                    streetViewControl: true,
                    overviewMapControl: true
                },
                scrollwheel: false,
                markers: mapMarkers,
                latitude: initLatitude,
                longitude: initLongitude,
                zoom: 16
            };

            var map = $('#googlemaps').gMap(mapSettings),
                mapRef = $('#googlemaps').data('gMap.reference');

            // Map text-center At
            var mapCenterAt = function (options, e) {
                e.preventDefault();
                $('#googlemaps').gMap("centerAt", options);
            }

            // Styles from https://snazzymaps.com/
            var styles = [{
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#e9e9e9"
                }, {
                    "lightness": 17
                }]
            }, {
                "featureType": "landscape",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#f5f5f5"
                }, {
                    "lightness": 20
                }]
            }, {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [{
                    "color": "#ffffff"
                }, {
                    "lightness": 17
                }]
            }, {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [{
                    "color": "#ffffff"
                }, {
                    "lightness": 29
                }, {
                    "weight": 0.2
                }]
            }, {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#ffffff"
                }, {
                    "lightness": 18
                }]
            }, {
                "featureType": "road.local",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#ffffff"
                }, {
                    "lightness": 16
                }]
            }, {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#f5f5f5"
                }, {
                    "lightness": 21
                }]
            }, {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#dedede"
                }, {
                    "lightness": 21
                }]
            }, {
                "elementType": "labels.text.stroke",
                "stylers": [{
                    "visibility": "on"
                }, {
                    "color": "#ffffff"
                }, {
                    "lightness": 16
                }]
            }, {
                "elementType": "labels.text.fill",
                "stylers": [{
                    "saturation": 36
                }, {
                    "color": "#333333"
                }, {
                    "lightness": 40
                }]
            }, {
                "elementType": "labels.icon",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "transit",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#f2f2f2"
                }, {
                    "lightness": 19
                }]
            }, {
                "featureType": "administrative",
                "elementType": "geometry.fill",
                "stylers": [{
                    "color": "#fefefe"
                }, {
                    "lightness": 20
                }]
            }, {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [{
                    "color": "#fefefe"
                }, {
                    "lightness": 17
                }, {
                    "weight": 1.2
                }]
            }];

            var styledMap = new google.maps.StyledMapType(styles, {
                name: 'Styled Map'
            });

            mapRef.mapTypes.set('map_style', styledMap);
            mapRef.setMapTypeId('map_style');

        </script>

    </body>

</html>
