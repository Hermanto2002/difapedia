<!DOCTYPE html>
<html>

<head>

	<!-- Basic -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title>Detail Product | Difa - Difabel Indonesia</title>

	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Favicon -->
	<link rel="shortcut icon" href="img/favicon.png" type="image/x-icon" />
	<link rel="apple-touch-icon" href="img/apple-touch-icon.png">

	<!-- Mobile Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

	<!-- Web Fonts  -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light"
		rel="stylesheet" type="text/css">

	<!-- Vendor CSS -->
	<link rel="stylesheet" href="{{ asset('homepage/vendor/bootstrap/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{ asset('homepage/vendor/fontawesome-free/css/all.min.css')}}">
	<link rel="stylesheet" href="{{ asset('homepage/vendor/animate/animate.min.css')}}">
	<link rel="stylesheet" href="{{ asset('homepage/vendor/simple-line-icons/css/simple-line-icons.min.css')}}">
	<link rel="stylesheet" href="{{ asset('homepage/vendor/owl.carousel/assets/owl.carousel.min.css')}}">
	<link rel="stylesheet" href="{{ asset('homepage/vendor/owl.carousel/assets/owl.theme.default.min.css')}}">
	<link rel="stylesheet" href="{{ asset('homepage/vendor/magnific-popup/magnific-popup.min.css')}}">
	<link rel="stylesheet" href="{{ asset('homepage/vendor/bootstrap-star-rating/css/star-rating.min.css')}}">
	<link rel="stylesheet" href="{{ asset('homepage/vendor/bootstrap-star-rating/themes/krajee-fas/theme.min.css')}}">

	<!-- Theme CSS -->
	<link rel="stylesheet" href="{{ asset('homepage/css/theme.css')}}">
	<link rel="stylesheet" href="{{ asset('homepage/css/theme-elements.css')}}">
	<link rel="stylesheet" href="{{ asset('homepage/css/theme-blog.css')}}">
	<link rel="stylesheet" href="{{ asset('homepage/css/theme-shop.css')}}">

	<!-- Demo CSS -->


	<!-- Skin CSS -->
	<link rel="stylesheet" href="{{ asset('homepage/css/skins/default.css')}}">

	<!-- Theme Custom CSS -->
	<link rel="stylesheet" href="{{ asset('homepage/css/custom.css')}}">

	<!-- Head Libs -->
	<script src="{{ asset('homepage/vendor/modernizr/modernizr.min.js')}}"></script>

</head>

<body>

	<div class="body">
		<header id="header" class="header-effect-shrink"
			data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyChangeLogo': true, 'stickyStartAt': 30, 'stickyHeaderContainerHeight': 70}">
			<div class="header-body border-color-primary border-bottom-0 box-shadow-none"
				data-sticky-header-style="{'minResolution': 0}"
				data-sticky-header-style-active="{'background-color': '#f7f7f7'}"
				data-sticky-header-style-deactive="{'background-color': '#FFF'}">
				<div class="header-top header-top-borders">
					<div class="container h-100">
						<div class="header-row h-100">
							<div class="header-column justify-content-start">
								<div class="header-row">
									<nav class="header-nav-top">
										<ul class="nav nav-pills">
											<li class="nav-item nav-item-borders py-2">
												<span class="pl-0">
													<i class="far fa-dot-circle text-4 text-color-primary"
														style="top: 1px;"></i> Live Donasi <i
														class="fas fa-angle-right"></i> <strong> Ahmad Abdul : Rp.
														50.000</strong>
												</span>
											</li>

										</ul>
									</nav>
								</div>
							</div>
							<div class="header-column justify-content-end">
								<div class="header-row">
									<nav class="header-nav-top">
										<ul class="nav nav-pills">

											<li class="nav-item dropdown nav-item-left-border d-none d-sm-block">
												<a class="nav-link" href="#" role="button" id="dropdownLanguage"
													data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													<i class="fas fa-sign-in-alt"></i> Masuk / Daftar
												</a>
												<div class="dropdown-menu dropdown-menu-right"
													aria-labelledby="dropdownLanguage">
													<a class="dropdown-item" href="#" data-toggle="modal"
														data-target="#ModalDonatur"> Donatur </a>
													<a class="dropdown-item" href="#" data-toggle="modal"
														data-target="#ModalDifabel">Difabel </a>
													<a class="dropdown-item" href="#" data-toggle="modal"
														data-target="#ModalPerusahaan">Perusahaan </a>
												</div>
											</li>
										</ul>
									</nav>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="header-container container">
					<div class="header-row">
						<div class="header-column">
							<div class="header-row">
								<div class="header-logo">
									<a href="{{ route('index')}}">
										<img alt="" width="160" height="52" data-sticky-width="125"
											data-sticky-height="40" src="{{ asset('homepage/img/logo-new.png')}}">
									</a>
								</div>
							</div>
						</div>
						<div class="header-column justify-content-end">
							<div class="header-row">
								<div
									class="header-nav header-nav-line header-nav-top-line header-nav-top-line-with-border order-2 order-lg-1">
									<div
										class="header-nav-main header-nav-main-square header-nav-main-effect-2 header-nav-main-sub-effect-1">
										<nav class="collapse">
											<ul class="nav nav-pills" id="mainNav">
												<li>
													<a class="dropdown-item dropdown-toggle" href="/">
														Beranda
													</a>
												</li>
												<li class="dropdown">
													<a class="dropdown-item dropdown-toggle" href="#">
														Promo
													</a>
												</li>
												<li class="dropdown">
													<a class="dropdown-item dropdown-toggle" href="#">
														Terbaru
													</a>
												</li>
												<li class="dropdown">
													<a class="dropdown-item dropdown-toggle" href="#">
														Rekomendasi
													</a>
												</li>
												<li class="dropdown">
													<a class="dropdown-item dropdown-toggle" href="#">
														Kategori
													</a>
													<ul class="dropdown-menu">

														<li><a class="dropdown-item" href="#">Miniatur</a></li>
														<li class="dropdown-submenu">
															<a class="dropdown-item" href="#">Lukisan</a>
															<ul class="dropdown-menu">
																<li><a class="dropdown-item"
																		href="blog-medium-image-sidebar-left.html">Pemandangan</a>
																</li>
																<li><a class="dropdown-item"
																		href="blog-medium-image-sidebar-right.html">Seni
																		& Budaya</a></li>
															</ul>
														</li>
														<li><a class="dropdown-item" href="#">Tempat ATK</a></li>
														<li><a class="dropdown-item" href="#">Pot Bunga</a></li>
														<li><a class="dropdown-item" href="#">Bucket Bunga</a></li>
														<li><a class="dropdown-item" href="#">Celengan</a></li>
														<li><a class="dropdown-item" href="#">Gantungan</a></li>
														<li><a class="dropdown-item" href="#">Produk Lainnya</a></li>
													</ul>
												</li>
											</ul>
										</nav>
									</div>
									<button class="btn header-btn-collapse-nav" data-toggle="collapse"
										data-target=".header-nav-main nav">
										<i class="fas fa-bars"></i>
									</button>
								</div>
								<div
									class="header-nav-features header-nav-features-no-border header-nav-features-lg-show-border order-1 order-lg-2 mr-2 mr-lg-0">
									<div class="header-nav-feature header-nav-features-cart d-inline-flex ml-2">
										<a href="{{route('cart.index')}}" >
											<img src="{{ asset('homepage/img/icons/icon-cart.svg')}}" width="14" alt=""
												class="header-nav-top-icon-img">
											<span class="cart-info">
												<span class="cart-qty">{{ countCart() }}</span>
											</span>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>

		@yield('content')


	</div>

	<footer id="footer" class="mt-0">
		<div class="footer-copyright">
			<div class="container py-2">
				<div class="row py-4">
					<div class="col d-flex align-items-center justify-content-center">
						<p class="text-center"><strong>DIFA.ID - DIFABEL INDONESIA</strong> <br> ©Copyright 2021. All
							Rights Reserved.</p>
					</div>
				</div>
			</div>
		</div>
	</footer>
	</div>

	<!-- Vendor -->
	<script src="{{ asset('homepage/vendor/jquery/jquery.min.js')}}"></script>
	<script src="{{ asset('homepage/vendor/jquery.appear/jquery.appear.min.js')}}"></script>
	<script src="{{ asset('homepage/vendor/jquery.easing/jquery.easing.min.js')}}"></script>
	<script src="{{ asset('homepage/vendor/jquery.cookie/jquery.cookie.min.js')}}"></script>
	<script src="{{ asset('homepage/vendor/popper/umd/popper.min.js')}}"></script>
	<script src="{{ asset('homepage/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
	<script src="{{ asset('homepage/vendor/common/common.min.js')}}"></script>
	<script src="{{ asset('homepage/vendor/jquery.validation/jquery.validate.min.js')}}"></script>
	<script src="{{ asset('homepage/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js')}}"></script>
	<script src="{{ asset('homepage/vendor/jquery.gmap/jquery.gmap.min.js')}}"></script>
	<script src="{{ asset('homepage/vendor/jquery.lazyload/jquery.lazyload.min.js')}}"></script>
	<script src="{{ asset('homepage/vendor/isotope/jquery.isotope.min.js')}}"></script>
	<script src="{{ asset('homepage/vendor/owl.carousel/owl.carousel.min.js')}}"></script>
	<script src="{{ asset('homepage/vendor/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
	<script src="{{ asset('homepage/vendor/vide/jquery.vide.min.js')}}"></script>
	<script src="{{ asset('homepage/vendor/vivus/vivus.min.js')}}"></script>
	<script src="{{ asset('homepage/vendor/bootstrap-star-rating/js/star-rating.min.js')}}"></script>
	<script src="{{ asset('homepage/vendor/bootstrap-star-rating/themes/krajee-fas/theme.min.js')}}"></script>

	<!-- Theme Base, Components and Settings -->
	<script src="{{ asset('homepage/js/theme.js')}}"></script>

	<!-- Current Page Vendor and Views -->
	<script src="{{ asset('homepage/js/views/view.shop.js')}}"></script>

	<!-- Theme Custom -->
	<script src="{{ asset('homepage/js/custom.js')}}"></script>

	<!-- Theme Initialization Files -->
	<script src="{{ asset('homepage/js/theme.init.js')}}"></script>

</body>

</html>
