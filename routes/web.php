<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', function () {
    return redirect('/');
});

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/', [App\Http\Controllers\Homepage\IndexController::class, 'index'])->name('index');
Route::get('/product', [App\Http\Controllers\Homepage\ProdukController::class, 'index'])->name('produk.index');
Route::get('/product/detail/{id}', [App\Http\Controllers\Homepage\ProdukController::class, 'detail'])->name('produk.detail');

Route::get('/cart', [App\Http\Controllers\Homepage\CartController::class, 'index'])->name('cart.index');
Route::post('/addToCart', [App\Http\Controllers\Homepage\CartController::class, 'add'])->name('cart.add');
Route::get('/cart/delete/{id}', [App\Http\Controllers\Homepage\CartController::class, 'delete'])->name('cart.delete');
Route::get('/checkout', [App\Http\Controllers\Homepage\CartController::class, 'checkout'])->name('cart.checkout');
Route::post('/checkout/add', [App\Http\Controllers\Homepage\CartController::class, 'checkout_add'])->name('cart.checkoutAdd');
Route::get('/payment', [App\Http\Controllers\Homepage\CartController::class, 'payment'])->name('cart.payment');

Route::get('/difa', [App\Http\Controllers\Homepage\DifabelController::class, 'index'])->name('difa.index');
Route::get('/difa/profil/{id}', [App\Http\Controllers\Homepage\DifabelController::class, 'profil'])->name('difa.profil');
Route::post('/difa/donasi', [App\Http\Controllers\Homepage\DifabelController::class, 'donasi'])->name('difa.donasi');

Route::get('/lowongan', [App\Http\Controllers\Homepage\LokerController::class, 'index'])->name('loker.index');
Route::get('/lowongan/detail/{id}', [App\Http\Controllers\Homepage\LokerController::class, 'detail'])->name('loker.detail');
Route::post('/lowongan/add', [App\Http\Controllers\Homepage\LokerController::class, 'add'])->name('loker.add');


//dashboard
Route::group(['namespace' => 'Dashboard', 'prefix' => 'dashboard'], function () {

    Route::post('/profile/update', "ProfileController@update")->name('profile.update');
    Route::get('/invoice/transaksi/{id}', "InvoiceController@transaksi")->name('invoice.transaksi');
    Route::get('/invoice/donasi/{id}', "InvoiceController@donasi")->name('invoice.donasi');

    //admin
    Route::group(['middleware' => ['role:admin'],'namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin.'], function () {
        //admin-dashboard
        Route::get('/', "DashboardController@index")->name('index');

        //admin-difabel
        Route::get('/difabel', "Difabel\DifabelController@index")->name('difabel.index');
        Route::get('/difabel/detail/{id}', "Difabel\DifabelController@detail")->name('difabel.detail');
        Route::get('/difabel/create', "Difabel\DifabelController@create")->name('difabel.create');
        Route::post('/difabel/store', "Difabel\DifabelController@store")->name('difabel.store');
        Route::post('/difabel/update', "Difabel\DifabelController@update")->name('difabel.update');
        Route::get('/difabel/produk', "Difabel\DifabelProdukController@index")->name('difabel.produk.index');
        Route::post('/difabel/produk/update/{id}', "Difabel\DifabelProdukController@update")->name('difabel.produk.update');
        Route::post('/difabel/produk/delete/{id}', "Difabel\DifabelProdukController@delete")->name('difabel.produk.delete');

        //admin-donatur
        Route::get('/donatur', "Donatur\DonaturController@index")->name('donatur.index');

        //admin-hrd
        Route::get('/hrd', "Hrd\HrdController@index")->name('hrd.index');
        //admin-hrd-loker
        Route::get('/hrd/loker', "Hrd\HrdLokerController@index")->name('hrdLoker.index');

        //admin-pembayaran
        Route::get('/pembayaran/donasi', "Pembayaran\DonasiController@index")->name('donasi.index');
        Route::post('/pembayaran/donasi/update', "Pembayaran\DonasiController@update")->name('donasi.update');
        Route::get('/pembayaran/produk', "Pembayaran\ProdukController@index")->name('produk.index');
        Route::post('/pembayaran/produk/update', "Pembayaran\ProdukController@update")->name('produk.update');

        //profile
        Route::get('/profile', "ProfileController@index")->name('profile.index');

    });


    //Hrd
    Route::group(['middleware' => ['role:hrd'],'namespace' => 'Hrd', 'prefix' => 'hrd', 'as' => 'hrd.'], function () {
        Route::get('/', "LokerController@index")->name('loker.index');
        Route::post('/loker/store', "LokerController@store")->name('loker.store');
        Route::post('/loker/update', "LokerController@update")->name('loker.update');

        Route::get('/profile', "ProfileController@index")->name('profile.index');
    });

    //difabel
    Route::group(['middleware' => ['role:difabel'],'namespace' => 'Difabel', 'prefix' => 'difabel', 'as' => 'difabel.'], function () {
        Route::get('/', "DashboardController@index")->name('index');

        //donasi
        Route::get('/donasi', "DonasiController@index")->name('donasi.index');

        //penjualan
        Route::get('/penjualan', "PenjualanController@index")->name('penjualan.index');

        //produk
        Route::get('/produk', "ProdukController@index")->name('produk.index');
        Route::post('/produk/store', "ProdukController@store")->name('produk.store');
        Route::post('/produk/update', "ProdukController@update")->name('produk.update');
        Route::get('/produk/delete/{id}', "ProdukController@delete")->name('produk.delete');

        //loker
        Route::get('/loker', "LokerController@index")->name('loker.index');

        Route::get('/profile', "ProfileController@index")->name('profile.index');
    });

    //donatur
    Route::group(['middleware' => ['role:donatur'],'namespace' => 'Donatur', 'prefix' => 'donatur', 'as' => 'donatur.'], function () {

        Route::get('/', "DashboardController@index")->name('index');
        //donasi
        Route::get('/donasi', "DonasiController@index")->name('donasi.index');
        Route::post('/donasi/update', "DonasiController@update")->name('donasi.update');
        Route::get('/produk', "ProdukController@index")->name('produk.index');
        Route::post('/produk/update', "ProdukController@update")->name('produk.update');

        Route::get('/profile', "ProfileController@index")->name('profile.index');
    });

});
